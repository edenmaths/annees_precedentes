---
hide:
  # - navigation # Hide navigation
  # - toc # Hide table of contents
---

1.  [Memento  Python](Memento_Oral_Banque-PT_2023.pdf)   et  un  [catalogue
   encore plus complet](http://mathsinfo-bcpst2-eden.fr/wp-content/uploads/2022/09/aidePython.pdf)

1. [Tout ce que vous auriez dû savoir sur Python](./1_Premiers_Pas.md) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/edenmaths%2Fbcpst1/main)

1. [Algorithmes élémentaires à maîtriser](2_Premiers_exos)

1. [Récursion](3_Recursion)

1. [Dénombrement et dictionnaires](30_denomb)

1. [Algo aux concours - épisode 1](31_TB_algo)

1. [Tracer des courbes](6_Courbes)

1. [Calcul matriciel](4_Calcul_matriciel)

1. [Matrices suite : les images](./TP_image/TP_image) 

1. [Dichotomie](7_Dichotomie)

1. [Tris : épisode 1](8_tris_1)

1. [Suites](./B2/a_suites)

3. [Méthode d'Euler](./B2/c_euler)

1. [Tables et dictionnaires](10_MIB)


1. [SQL](12_sql)

1. [Cartes et big data](9_CarteElections2022)




1. [Approximations de calcul d'intégrales](11_approxint)



<!---

1. [Statistiques](5_Statistiques)

1. [Images](./TP_image/TP_image) ett [corrigé](./TP_image_corr/TP_image_corr)

1. [Dichotomie](7_Dichotomie)

1. [Tris : épisode 1](8_tris_1)

1. [Cartes et big data](9_CarteElections2022)

1. [Tables et dictionnaires](10_MIB)


1. [Approximations de calcul d'intégrales](11_approxint)


1. [SQL](12_sql)

-->
