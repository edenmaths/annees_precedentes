{{ chapitre(6, "Courbes", "TP ", 0)}}

# Courbe représentative d'une fonction

Il faudra importer les bibliothèques utiles :

```python
import matplotlib.pyplot as plt # Module pour tracer les graphiques
import numpy as np # pour donner à manger aux fonctions du module précédent
```

puis créer un tableau d'abscisses, souvent avec `linspace` :

```python
X = np.linspace(-3, 3, 1000) # linspace(début, fin, en option le nombre de points - 50 par défaut)
```

créer le tableau de leurs images. On utilise de préférence les fonctions de `numpy` qui s'appliquent sur chaque élément d'un tableau.

Ainsi `f([x1, x2, x3])` renvoie le tableau `[f(x1), f(x2), f(x3)]` ce qui est fort commode. Attention ! cela ne marche pas avec de simples listes Python 
ou avec des fonctions du module `math`

```python
Y = np.exp(X) * np.cos(1000*X)
```

Il ne reste plus qu'à lancer `plot` sur ces deux tableaux et de mander l'affichage avec `show`:

```python
plt.plot(X, Y)
plt.show()
```

![plot1](./IMG/plot1.png)

