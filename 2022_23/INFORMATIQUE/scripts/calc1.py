import numpy as np

r = np.sqrt(2)

print(f"r = {r}")

print(f"Le carré de r vaut {r*r}")
