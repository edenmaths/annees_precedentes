import pandas 
import numpy as np
import matplotlib.pyplot as plt


### Récupération et formatage des données
url = "http://www.cybervulcans.net/modules/bd/stats_taille-poids.php?poste=4&Submit=Envoyer"

tab = pandas.read_html(url)[1]
tab.columns = tab.iloc[1] # 2e ligne comme en-tête de colonnes
tab = tab[2:] # on prend comme valeurs celles à partir de la 3e ligne
tab = tab.astype({"Taille": int, "Poids": int}) # pour convertir les éléments des colonnes Taille et Poids en entiers


def df2list(df):
    """
    transforme un dataframe d'une colonne en liste
    """
    return [el for el in df]


#### On sait traiter des listes donc on transforme nos dataframes en listes

tailles = df2list(tab['Taille'])
poids = df2list(tab['Poids'])
noms = df2list(tab['Nom Prénom'])
dates = df2list(tab['Né le'])

t_n = [(noms[i], tailles[i], dates[i][-4:]) for i in range(len(tailles))]

#### On trie les joueurs selon leur taille

def tri_joueurs(js: list) -> list:
    njs = js[:]
    for i_stop in range(len(njs) - 1, -1, -1): # de droite à gauche je place les maxis
        for i in range(i_stop): # le numéro du caillou actif qui se compare à son suivant
            if njs[i][1] > njs[i+1][1]: # s'il est plus grand que son suivant
                njs[i], njs[i+1] = njs[i+1], njs[i] # on les échange
    return njs
                  

### Étude de la corrélation taille/poids

def moyenne(xs: list) -> float:
    s = 0
    for x in xs:
        s += x
    return s/len(xs)

def covariance(xs: list, ys: list) -> float:
    """Avec KH"""
    nx, ny = len(xs), len(ys)
    assert nx == ny, "séries de longueurs différentes"
    xbar, ybar = moyenne(xs), moyenne(ys)
    xybar = moyenne([xs[k]*ys[k] for k in range(nx)])
    return xybar - xbar*ybar

def variance(xs: list) -> float:
    return covariance(xs, xs)

def coeff_corr(xs: list, ys: list) -> float:
    return covariance(xs, ys) / np.sqrt(variance(xs) * variance(ys))

def reg_lin(xs: np.ndarray, ys: np.ndarray) -> tuple:
    xbar, ybar = moyenne(xs), moyenne(ys)
    a = covariance(xs, ys) / variance(xs)
    b = ybar - a*xbar # la droite passe par le point moyen
    D = [a*xs[k] + b for k in range(len(xs))] # ou a*xs + b si xs est un np.array
    print(f'a = {a}, b = {b}, r = {coeff_corr(xs, ys)}')
    plt.scatter(xs, ys, marker='P', color='blue')
    plt.plot(xs, D, color='red')
    plt.show()
    return a, b

# reg_lin(np.array(tailles), np.array(poids))


#####
#####   Tailles des humains de 1896 à 1996
#####

url = "https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/average-height-by-year-of-birth.csv"

urlretrieve(url, filename="./tailles_terriens.csv")
terre = pandas.read_csv("tailles_terriens.csv")

terre = terre.rename(columns = {'Mean male height (cm)': "Hommes", "Mean female height (cm)": "Femmes"})

def pays(code):
    return  terre.loc[lambda df: df["Code"] == code]


def trace_h_f(code):
    p = pays(code) 
    p.plot(x = 'Year', y = ['Hommes', 'Femmes'])
    plt.show()

def taux(code, sexe):
    p = pays(code)
    t0 = p.iloc[0][sexe]
    t1 = p.iloc[-1][sexe]
    return (t1 - t0) / t0
    
def reg_h_f(code):
    reg_lin(np.array(pays(code)['Hommes']), np.array(pays(code)['Femmes']))



                     


#reg_lin(np.array(hommes_france), np.array(femmes_france))

