{{ chapitre(6, "Info aux concours - les 30 premières minutes ", "TP ", 0)}}

Lors  des 30  premières  minutes préparées,  vous  aurez obligatoirement  à
illustrer le problème mathématique avec Python. Les thèmes habituels sont : 

- simulation de lois de probabilités
- méthode d'Euler,
- représentation graphique de fonctions / suites
- calculs des termes d'une suite
- méthode des rectangles
- recherches d'éléments propres d'une matrice
- produit scalaire
- ...

Vous aurez accès à une machine  pendant les 40 minutes de préparation. Vous
enregistrerez vos travaux sur une clé qui vous sera fournie et sera ouverte
à votre arrivée dans la salle d'interrogation.

Vous utiliserez donc  aujourd'hui vos machines comme le jour  J avec Spyder
ou Thonny.


Voici quelques exemples.

### Exercice 1 : Éléments propres d'une matrice


On munit $E=\mathbb{R}^{3}$ du produit scalaire usuel que l'on notera $<,>$.

On considère l'endomorphisme  $f$ de $\mathbb{R}^{3}$ dont  la matrice dans
la base canonique $B=\left(e_{1},  e_{2}, e_{3}\right)$ de $\mathbb{R}^{3}$
est : 

$$
A=\left(\begin{array}{ccc}
\frac{5}{2} & 1 & \frac{1}{2} \\
1 & 2 & 1 \\
\frac{1}{2} & 1 & \frac{5}{2}
\end{array}\right)
$$

Que permet le programme suivant?

```python
import numpy as np
import numpy.linalg as al

A = np.array([[5/2, 1, 1/2], [1, 2, 1], [1/2, 1, 5/2]])
Ι= np.eye(3)

r = al.matrix_rank(A - I)
s = al.matrix_rank(A - 2*I)
t = al.matrix_rank(A - 4*I)
```
À l'aide de Python, déterminer une base de vecteurs propres de $A$.



### Exercice 2 : Produit scalaire


Soit   $n   \in   \mathbb{N}^{*}$.    On   considère   l'espace   vectoriel
$\mathbb{R}^{n}$  muni  de   sa  base  canonique  $\mathcal{B}=\left(e_{1},
\ldots, e_{n}\right)$,  de son  produit scalaire usuel  noté $\langle\cdot,
\cdot\rangle$ et de sa norme usuelle notée $\|\cdot\|$. 

Soit $p \in  [\![ 1, n ]\!]$. Pour  toute famille $\left(u_{1},
\ldots, u_{p}\right)$  de vecteurs de  $\mathbb{R}^{n}$, on définit  $G$ la
matrice de Gram de $\left(u_{1}, \ldots, u_{p}\right)$ par 

$$
G=\left(\begin{array}{cccc}
\left\langle u_{1}, u_{1}\right\rangle & \left\langle u_{1}, u_{2}\right\rangle & \cdots & \left\langle u_{1}, u_{p}\right\rangle \\
\left\langle u_{2}, u_{1}\right\rangle & \left\langle u_{2}, u_{2}\right\rangle & \cdots & \left\langle u_{2}, u_{p}\right\rangle \\
\vdots & \vdots & \ddots & \vdots \\
\left\langle u_{p}, u_{1}\right\rangle & \left\langle u_{p}, u_{2}\right\rangle & \cdots & \left\langle u_{p}, u_{p}\right\rangle
\end{array}\right) \in \mathcal{M}_{p}(\mathbb{R})
$$

1. Écrire une  fonction Python ps prenant en argument  deux vecteurs u
    et $\mathrm{v}$  sous formes de  liste de  même taille et  renvoyant le
    produit scalaire $\langle u, v\rangle$. 
 
2.  Écrire une  fonction Python  Gram prenant  en argument  une famille  de
   vecteurs $\left(u_{1},  \ldots, u_{p}\right)$ sous forme  d'une liste de
   listes  et renvoyant  la matrice  de  Gram de  la famille  $\left(u_{1},
   \ldots, u_{p}\right)$. 
   
3. Tester votre fonction avec les vecteurs $u_{1}=(1,-1,0), u_{2}=(1,0,-1)$
   et $u_{3}=(1,1,1)$.  
   
   
### Exercice 3 : construction de matrice et utilisation de `linalg`


Pour   tout  $n   \geqslant  1$.   On   considère  la   matrice  $K_n   \in
\mathcal{M}_{n+1}(\mathbb{R})$ telle que  pour tout $i \in  [\![ 1, n
]\!],\left(K_n\right)_{i, i+1}=i$,  pour tout $j \in  [\![ 1, n
]\!],\left(K_n\right)_{j+1,  j}=-n-1+j$  et   dont  tous  les  autres
coefficients sont nuls. On a donc : 

$$
K_1=\left(\begin{array}{cc}
0 & 1 \\
-1 & 0
\end{array}\right) \text { et } K_2=\left(\begin{array}{ccc}
0 & 1 & 0 \\
-2 & 0 & 2 \\
0 & -1 & 0
\end{array}\right)
$$

1. Écrire une fonction `K` en Python qui prend en entrée un entier
   $n$ et qui renvoie la matrice $K_n$. 
   
2.   Utiliser la  fonction précédente  et la  fonction `eigvals`  du module
    `numpy.linalg` pour déterminer les valeurs propres de $K_n$ pour $n \in
    [\![ 1,10 ]\!]$. Que peut-on conjecturer ? 




### Exercice 4 : Probas à densité


Soient   $\lambda  \in   \mathbb{R}_{+}^*$   et  $\left(T_n\right)_{n   \in
\mathbb{N}}$  une  suite  de  variables  aléatoires  réelles  indépendantes
suivant toute la loi exponentielle de paramètre $\lambda$.

On définit la  variable aléatoire réelle $N$ égale au  plus petit entier $k
\in \mathbb{N}^*$ tel  que $T_k \leqslant T_0$ si un  tel entier existe, et
égale à 0 sinon.

1.  Soit $U$ une variable aléatoire suivant la loi uniforme sur $] 0,1[$.
    Démontrer que $-\frac{1}{\lambda} \ln  (1-U)$ suit la loi exponentielle
    de paramètre $\lambda$. 

2. Écrire  une fonction Python $N(\ell)$  qui prend en entrée  un paramètre
   $\ell>0$ et qui simule la variable aléatoire $N$ avec $\lambda=\ell$. 

3.   Écrire une  fonction Python  $\operatorname{espN}(\ell)$ qui  prend en
    entrée un  paramètre $\ell>0$  et qui renvoie  une valeur  approchée de
    l'espérance   de   $N$  si   elle   existe   (encore  une   fois   avec
    $\lambda=\ell$).
	Que peut-on conjecturer?
	



### Exercice 5 : Somme de VAR

On s'intéresse  à la vitre  d'un abris bus  dans un lieu  sensible. Lorsque
celle-ci est cassée, on la remplace immédiatement. On s'intéresse au nombre
de vitres cassées au bout d'un temps $t$ (en heures).

Pour $n \geqslant 1$, on note $X_n$ la durée de vie en heures de la $n$-ième vitre de cet abris bus. On suppose que $\left(X_n\right)_{n \in \mathbb{N}^*}$ est une suite de variables mutuellement indépendantes qui suivent toutes la loi exponentielle de paramètre $\lambda>0$.
On commence  à observer au  temps 0 et pour  $n \in \mathbb{N}^*$,  on note
$S_n$ le  temps au  bout duquel,  en heures,  la vitre  est cassée  pour la
$n$-ième fois de cet abris bus. 


1.  Écrire une fonction `nb\_cassees (t, lamb )` qui prend en
      arguments deux flottants strictement positifs `t` et `lamb`, qui simule
      l'expérience décrite  ci-dessus et qui  retourne le nombre  de vitres
      cassées dans  l'intervalle $[0, \mathrm{t}]$ où  `lamb` est
      le paramètre de la loi  exponentielle. On pourra simuler une variable
      suivant une loi exponentielle à l'aide de `random()`

2.  Écrire une fonction `moyenne(t, lamb)` qui estime le nombre moyen
  de vitres cassées dans l'intervalle $[0,t]$. 
  
  
### Exercice 6 : histogramme

Soit V une VAR qui suit une loi  de Bernoulli de paramètre 1/2 et U une VAR
indépendante de  V qui suit la  loi uniforme sur  $]0,1[$ et soit T  la VAR
définie par $T=2V-1$.


1. Écrire  une fonction `SimulT(n)` qui  prend en entrée un  entier naturel
   $n$  non  nul  et  renvoie  une liste  de  longueur  $n$  contenant  $n$
   réalisations de T.
   
2. On  pose $W=2-\ln(1-U)$.  Écrire une fonction  `SimulW(n)` qui  prend en
   entrée une entier  naturel non nul $n$ et renvoie  une liste de longueur
   $n$ contenant $n$ réalisations de W.
   
3.  On pose  $Z=T×W$.  Écrire  un script  Python  permettant  de tracer  la
   répartirion de $n$ réalisations de $Z$ sur une partition de $[\min(Z), \max(Z)]$ en 20
   intervalles de longueur égale.
   
   
### Exercice 7 : produit matriciel et VAR


On         considère          $\left(X_{i         j}\right)_{(i,         j)
\in\left(\mathbb{N}^{*}\right)^{2}}$  une famille  de variables  aléatoires
discrètes réelles définies sur un espace probabilisé $(\Omega, \mathcal{T},
P)$ telles que 

- pour tout $(i, j) \in\left(\mathbb{N}^{*}\right)^{2}, X_{i j}=X_{j i}$
- les variables aléatoires $X_{i j}$ sont de même loi, d'espérance nulle et de variance 1 et les $X_{i j}^{2}$ admettent une variance $v$;
- pour $n \geqslant 1$, les variables aléatoires $X_{i j}$, pour $1 \leqslant i \leqslant j \leqslant n$, sont mutuellement indépendantes.

Pour tout $n \in \mathbb{N}^{*}$, on considère la matrice aléatoire $M_{n}$
formée  par  les   $\left(X_{i  j}\right)_{(i,  j)  \in   [\![  1,  n
]\!]^{2}}$. 

Par  exemple, pour  $n=2, M_{2}=\left(\begin{array}{cc}X_{11}  & X_{12}  \\
X_{21} & X_{22}\end{array}\right)$. 


 Soit $Y$ une  variable aléatoire suivant la loi binomiale  de paramètres 4
 et $\frac{1}{2}$. On définit la variable aléatoire $Z=Y-2$.
 
On définit la trace d'une matrice $A \in \mathcal{M}_{n}(\mathbb{R})$ comme la somme de ses coefficients diagonaux $:$ trace $(\mathrm{A})=\sum_{i=1}^{n} a_{i i}$.

On suppose que les $X_{i j}$ suivent la loi de $Z$.

1.  Écrire une  fonction Python d'argument $n  \in \mathbb{N}^{*}$ simulant
    une   réalisation    de   la   matrice   aléatoire    $M_{n}$   définie
    précédemment. On pourra  utiliser la fonction `binomial`  de la bibliothèque
    `random`. 

2.  Écrire une fonction Python  d'argument $n \in \mathbb{N}^{*}$ calculant
    une         valeur         approchée        de         $\frac{1}{n^{2}}
    E\left(\operatorname{trace}\left(M_{n}^{2}\right)\right)$            et
    $\frac{1}{n^{3}}
    E\left(\operatorname{trace}\left(M_{n}^{4}\right)\right)$.   On  pourra
    utiliser  la  fonction  `trace`  de   la  bibliothèque  `numpy`.  Produire  une
    application numérique pour $n=10$. 


### Exercice 8 : polynômes

Pour tout entier naturel non nul $n$ et tout entier naturel $k$ inférieur à
$n$ on note $B_{n,k}$ le polynôme de $\mathbb{R}_n[X]$ défini par :

$$
B_{n,k}=\binom{n}{k}X^k(1-X)^{n-k}
$$

1.                               Démontrer                              que
   $B_{n,k}=\displaystyle\sum_{i=k}^n\binom{n}{k}\binom{n-i}{n-k}(-1)^{i-k}X^i$.
   
2. En  Python, on décide de  coder un polynôme en  listant ses coefficients
   par ordre de  degré croissant. Déterminer une fonction  Python qui prend
   en argument deux  entiers $n$ et $k$ et qui  renvoie la liste modélisant
   $B_{n,k}$.
   

