import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import matplotlib.colors as colors

def mat_pow(M: np.ndarray, n: int) -> np.ndarray:
    P = np.eye(np.shape(M)[0])
    for k in range(n):
        P = np.dot(P, M)
    return P


taille = 6 # taille de la matrice

Z = np.array([[1 if i < j else 0 for j in range(taille)] for i in range(taille)])

nf = 4
mats = [mat_pow(Z,i + 1) for i in range(nf)]
n = int(np.max(mats)) + 2

# Fabrication d'une barre de couleurs adaptée
cols = []
for i in range(n):
    cols.append(tuple([np.random.rand() for _ in range(4)]))
mes_couleurs = colors.ListedColormap(cols) 
ma_norme = colors.BoundaryNorm(range(len(cols)), mes_couleurs.N, clip=True)
# Fin de la fabrication de la colormap

for i in range(nf):
    plt.clf()
    im = plt.imshow(mat_pow(Z,i + 1), cmap=mes_couleurs, norm=ma_norme)
    cb = plt.colorbar()
    plt.show()
