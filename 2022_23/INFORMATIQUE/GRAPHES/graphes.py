Graphe = dict


g = {'1': {'2', '4', '3'}, '2': {'3'}, '3': {'4'}}

def sommets(g: Graphe) -> list:
    sx = []
    for s in g:
        if s not in sx:
            sx.append(s)
        for v in g[s]:
            if v not in sx:
                sx.append(v)
    return sorted(sx)


