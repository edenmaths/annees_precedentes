import pandas 

url = "http://www.cybervulcans.net/modules/bd/stats_taille-poids.php?poste=4&Submit=Envoyer"
tab = pandas.read_html(url, header=1)[0] # données sur les 2nde ligne de l'ASM
tailles = tab["Taille"]
ts = [int(taille) for taille in tailles]


def tri_compte(entiers: list) -> list:
    m = max(entiers)
    compteurs = [0 for _ in range(0, m + 1)] # Au départ, on met les compteurs à zéro
    # compte est l'histogramme de xs
    for entier in entiers:
        compteurs[entier] += 1
    entiers_tries = [] # on va ajouter chaque entier autant de fois qu'il est présent
    for i in range(len(compteurs)): # ou range(m + 1)
        for occ in range(compteurs[i]):
            entiers_tries.append(i)
    return entiers_tries

def mediane(entiers: list) -> float:
    entiers_tries = tri_compte(entiers)
    nb = len(entiers)
    if nb % 2 == 0:
        return 0.5*(entiers_tries[nb//2] + entiers_tries[nb//2 - 1])
    else:
        return entiers_tries[nb // 2]
    
print(mediane(ts))
    