import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0.1,10,200)
f = lambda t: t - np.log(t) - 1/t

plt.plot(x, x , label = "partie affine")
plt.plot(x, f(x), label = "Cf")
plt.grid(True, which='both')
plt.legend()
plt.show()