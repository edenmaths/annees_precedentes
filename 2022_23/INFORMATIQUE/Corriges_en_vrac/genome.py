from urllib.request import urlretrieve

url_genome = "https://edenmaths.gitlab.io/bcpst1/2021_22/INFORMATIQUE/SARSCOV2.txt"
urlretrieve(url_genome, filename="./genome.txt")
gen = open("./genome.txt", "r")

# chaine de caractères contenant le genome du sars covid 2
covid = gen.read().rstrip()

def compte_bases(genome: str) -> dict:
    # compteurs à 0
    cpt = {base: 0 for base in 'ACGT'}
    # on parcourt le génôme et on incrémente le compteur des bases vues
    for base in genome:
        cpt[base] += 1
    return cpt

#variante
def compte_bases2(genome: str) -> dict:
    cpt = {}
    for base in genome:
        if base in cpt:
            cpt[base] += 1
        else:
            cpt[base] = 1
    return cpt

def compte_nuplets(genome: str, long: int) -> dict:
    cpt = {}
    for i in range(len(genome) - long):
        nuplet = genome[i:i + long]
        if nuplet in cpt:
            cpt[nuplet] += 1
        else:
            cpt[nuplet] = 1
    return cpt
    
    
def compte_repets(genome: str, long: int) -> dict:
    cpt = {base*long: 0 for base in 'ACGT'}
    for i in range(len(genome) - long):
        nuplet = genome[i:i + long]
        if nuplet in cpt:
            cpt[nuplet] += 1
    return cpt
















