from urllib.request import urlretrieve

url_genome = "https://edenmaths.gitlab.io/bcpst1/2021_22/INFORMATIQUE/SARSCOV2.txt"
urlretrieve(url_genome, filename="./genome.txt")
with open("./genome.txt", "r") as gen:
    covid = gen.read().rstrip()

def compte_bases(genome: str) -> dict:
    compteurs = {base: 0 for base in 'ACGT'}
    for base in genome:
        compteurs[base] += 1
    return compteurs

def compte_nuplets(genome: str, long: int) -> dict:
    compteurs = {} # je ne connais pas les clés a priori
    nb_car = len(genome)
    for debut in range(nb_car - long + 1):
        nuplet = genome[debut: debut + long]
        if nuplet in compteurs:
            compteurs[nuplet] += 1
        else:
            compteurs[nuplet] = 1
    return compteurs


def compte_repets(genome: str, long: int) -> dict:
    compteurs = {...}
    for ... in ...:
        ....
        ....
            compteurs[...] += 1
    return compteurs