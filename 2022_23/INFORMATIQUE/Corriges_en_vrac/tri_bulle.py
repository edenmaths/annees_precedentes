from numpy.random import choice
import matplotlib.pyplot as plt
import numpy as np
from time import perf_counter


def une_liste(n):
    """ liste de n entiers aléatoirement choisis entre 0 et 10n - 1"""
    return list(choice(range(10*n), n))



def tri_bulle(ma_liste: list):
    xs = ma_liste[:] # je copie pour ne pas modifier ma liste initiale
    for i_stop in range(len(xs) - 1, -1, -1): # jusqu'où je regarde : de droite à gauche
        for i in range(i_stop): # le caillou actif qui se compare à son suivant
            if xs[i] > xs[i + 1]: # s'il est plus grand que son suivant
                xs[i], xs[i + 1] = xs[i + 1], xs[i] # on les échange
    return xs


def insere(elmt, liste_triee: list) -> list:
    cs = liste_triee[:] + [elmt] # on insère elmt par la doite dans liste_triee
    n = len(liste_triee)
    i = n # indice de départ  (il y a n + 1 éléments !)
    while cs[i] < cs[i - 1] and i > 0: # tant que le ieme est mal placé
        cs[i - 1], cs[i] = cs[i], cs[i - 1] # on échange 
        i -= 1 # on recule d'un cran
    return cs

def tri_insere(Pioche: list) -> list:
    Main = []
    for carte in Pioche:
        Main = insere(carte, Main) # on insère les cartes une par une
    return Main

def test(tri):
    """ teste 20 fois le tri donné en argument sur des listes de taille 1000"""
    for _ in range(20):
        t = une_liste(1000)
        assert tri(t) == sorted(t)
    print(f"{tri} est OK après 20 tests")
    
    
def liste(n):
    return list(choice(range(10*n), n))

def temps(tri, n):
    p = liste(n)
    debut = perf_counter()
    tri(p)
    return perf_counter() - debut

def liste_temps(tri):
    n = 10
    xs, ts = [], []
    while n < 6000:
        t = temps(tri, n)
        ts.append(t)
        xs.append(n)
        n = int(n*1.2)
    return xs, ts

x, y = liste_temps(tri_bulle)
plt.plot(x, y)
x, y = liste_temps(tri_insere)
plt.plot(x, y)

plt.show()