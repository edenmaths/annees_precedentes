import numpy as np
import matplotlib.pyplot as plt
from urllib.request import urlretrieve

image = np.ndarray
pixel = np.ndarray

url = "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/98/9843d072473afaffb2c910416993d7cb0845395c_full.jpg"
urlretrieve(url, filename="./kermit.jpg")
kermit = plt.imread("./kermit.jpg")

def un_sur_deux(im: image, ratio: float) -> image:
    h, w, _ = im.shape
    return np.array([[im[i, int(j/ratio)] for j in range(int(w*ratio))] for i in range(h)])

def gris1_1(im: image) -> image:
    h, w, _ = im.shape
    new_im = np.zeros(im.shape, dtype = 'uint8')
    for i in range(h):
        for j in range(w):
            g = np.sum(im[i,j]) // 3
            new_im[i, j] = [g, g, g]
    return new_im
    

def gris1_2(im: image) -> image:
    new_im = []
    for ligne in im:
        new_ligne = []
        for pix in ligne:
            g = np.sum(pix)//3
            new_pix = [g, g, g]
            new_ligne.append(new_pix)
        new_im.append(new_ligne)
    return np.array(new_im, dtype='uint8')














plt.imshow(gris1_2(kermit))
plt.show()

