import pandas 
url = "http://www.cybervulcans.net/modules/bd/stats_taille-poids.php?poste=4&Submit=Envoyer"

tab = pandas.read_html(url, header=1)[0]
tailles = tab["Taille"]
ts = [int(taille) for taille in tailles]

def tri_compte(entiers: list) -> list:
    m = max(entiers)
    compteurs = [0 for _ in range(0, m + 1)] # Au départ, on met les compteurs à zéro
    # compte est l'histogramme de xs
    for entier in entiers:
        compteurs[entier] += 1
    entiers_tries = [] # on va ajouter chaque entier autant de fois qu'il est présent
    for i in range(len(compteurs)): # ou range(m + 1)
        for occ in range(compteurs[i]):
            entiers_tries.append(i)
    return entiers_tries

def mediane(entiers: list) -> float:
    liste_triee = tri_compte(entiers)
    