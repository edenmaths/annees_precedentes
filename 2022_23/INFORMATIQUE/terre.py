import numpy as np
import matplotlib.pyplot as plt
from urllib.request import urlretrieve
import pandas

url = "https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/average-height-by-year-of-birth.csv"

urlretrieve(url, filename="./tailles_terriens.csv")
terre = pandas.read_csv("tailles_terriens.csv")

terre = terre.rename(columns = {'Mean male height (cm)': "Hommes", "Mean female height (cm)": "Femmes"})

France = terre.loc[lambda df: df["Code"] == 'FRA']

France.plot(x = 'Year', y = ['Hommes', 'Femmes'])
plt.show()
