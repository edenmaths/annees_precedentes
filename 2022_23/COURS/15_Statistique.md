{{ chapitre(15, "Statistique descriptive")}}


Cours au format 

- [PDF](../LATEX/PolyStats21.pdf) 

- et ses sources [TEX](../LATEX/stats_21.tex)

- [Fonctions Python utiles](../../INFORMATIQUE/5_stats_scripts/)
