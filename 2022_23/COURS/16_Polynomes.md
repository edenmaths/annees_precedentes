{{ chapitre(16, "Polynomes")}}


Cours au format 

- [PDF](../LATEX/PolyPoly21.pdf) 

-  ses sources [TEX](../LATEX/poly_21.tex)

- et les [fonctions Python](../polynomes_py/) associées.
