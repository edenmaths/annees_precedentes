//const config = {
//  type: 'line',
//  data: data,
//};

var lab = []
var dat= []

//var lab = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
function deff(f, a, b, N) {
    lab = []
    dat= []
    const h = (b - a)/N
    for (var j = 0; j < N; j++) {//j <lab.length; j++) {
	x = a + j*h;
	lab.push(x); //x = lab[j],
	y = f(x);
	dat.push(y);
    }
    //console.log(lab);
}

deff(t => 1/(1-t), 0, 1, 50);

var data1 = {
    labels: lab,
    datasets: [
        {
	    backgroundColor: "rgba(250,150,0,0.5)",
	    radius: 0,
	    fill:true,
	    cubicInterpolationMode: 'monotone',
            data:dat
        }
    ]
};


deff(t => 1/(t*t), 1, 11, 50);

var data2 = {
    labels: lab,
    datasets: [
        {
	    backgroundColor: "rgba(250,150,0,0.5)",
	    radius: 0,
	    fill:true,
	    cubicInterpolationMode: 'monotone',
            data:dat
        }
    ]
};


deff(t => 1/(t), 0.01, 1, 100);

var data3 = {
    labels: lab,
    datasets: [
        {
	    backgroundColor: "rgba(250,150,0,0.5)",
	    radius: 0,
	    fill:true,
	    cubicInterpolationMode: 'monotone',
            data:dat
        }
    ]
};


deff(t => 1/(t*t), -11, -1, 50);

var data4 = {
    labels: lab,
    datasets: [
        {
	    backgroundColor: "rgba(250,150,0,0.5)",
	    radius: 0,
	    fill:true,
	    cubicInterpolationMode: 'monotone',
            data:dat
        }
    ]
};


deff(t => 1/(3.14*(1+t*t)), -5, 5, 100);

var data5 = {
    labels: lab,
    datasets: [
        {
	    backgroundColor: "rgba(250,150,0,0.5)",
	    radius: 0,
	    fill:true,
	    cubicInterpolationMode: 'monotone',
            data:dat
        }
    ]
};




var ctx1 = document.getElementById("myChart1").getContext("2d");
var ctx2 = document.getElementById("myChart2").getContext("2d");
var ctx3 = document.getElementById("myChart3").getContext("2d");
var ctx4 = document.getElementById("myChart4").getContext("2d");
var ctx5 = document.getElementById("myChart5").getContext("2d");

var stackedLine1 = new Chart(ctx1, {
    type: 'line',
    data: data1,
    options: {
	aspectRatio: 2,
        scales: {
	   
            y: {
                stacked: true,
		display: true
            },
	    x: {
		display: true,
		ticks: {
		    callback: function(val) {
			let l = Math.floor(100*this.getLabelForValue(val));
			return l % 10 === 0 ? this.getLabelForValue(val) : '';
		    }
		}
	    }
        },
	plugins: {
	     legend: {
		display:false
	    }
	}
    }
});

var stackedLine2 = new Chart(ctx2, {
    type: 'line',
    data: data2,
    options: {
	aspectRatio: 2,
        scales: {
	   
            y: {
                stacked: true,
		display: true
            },
	    x: {
		display: true,
		ticks: {
		    callback: function(val) {
			let l = Math.floor(this.getLabelForValue(val));
			let v = this.getLabelForValue(val);
			return l === v ? this.getLabelForValue(val) : "";
		    }
		}
	    }
        },
	plugins: {
	     legend: {
		display:false
	    }
	}
    }
});



var stackedLine3 = new Chart(ctx3, {
    type: 'line',
    data: data3,
    options: {
	aspectRatio: 2,
        scales: {
	   
            y: {
                stacked: true,
		display: true,
		min: 0
            },
	    x: {
		min: 0,
		display: false,
		ticks: {
		    callback: function(val) {
			let l = Math.floor(100*this.getLabelForValue(val));
			return l % 10 === 0 ? this.getLabelForValue(val) : '';
		    }
		}
	    }
        },
	plugins: {
	     legend: {
		display:false
	    }
	}
    }
});

var stackedLine4 = new Chart(ctx4, {
    type: 'line',
    data: data4,
    options: {
	aspectRatio: 2,
        scales: {
	   
            y: {
                stacked: true,
		display: true
            },
	    x: {
		min:0,
		stacked: true,
		display: true,
		ticks: {
		    suggestedMax: -1,
		    suggestedMin: -11,
		    callback: function(val) {
		    	let l = Math.floor(this.getLabelForValue(val));
		    	let v = this.getLabelForValue(val);
		    	//console.log(v);
		    	return l === v ? this.getLabelForValue(val) : "";
		    }
		}
	    }
        },
	plugins: {
	     legend: {
		display:false
	    }
	}
    }
});


var stackedLine5 = new Chart(ctx5, {
    type: 'line',
    data: data5,
    options: {
	aspectRatio: 2,
        scales: {
	   
            y: {
                stacked: true,
		display: true
            },
	    x: {
		min:0,
		stacked: true,
		display: true,
		ticks: {
		    suggestedMax: 10,
		    suggestedMin: -10,
		    callback: function(val) {
		    	let l = Math.floor(this.getLabelForValue(val));
		    	let v = this.getLabelForValue(val);
		    	//console.log(v);
		    	return l === v ? this.getLabelForValue(val) : "";
		    }
		}
	    }
        },
	plugins: {
	     legend: {
		display:false
	    }
	}
    }
});


