{{ chapitre(9, "Dérivées et primitives: rappels")}}


Cours au format 

- [PDF](../LATEX/PolyDerPrimInt21.pdf) 

- et ses sources [TEX](../LATEX/DerPrimInt_21.tex)
