{{ chapitre(2, "Fractions", "Rappel N°")}}


## Au collège
![Placeholder](./IMG/martine_nulle.png){ align=right width=350}


!!! note "Petits rappels"

	Pour tous réels non nuls $a, b, c, d, k$ :

	$$
	\dfrac{a}{b} \times \dfrac{c}{d}=\dfrac{a \times  c}{b \times d}, \quad
	\dfrac{k a}{k b}=\dfrac{a}{b}
	$$
	
	$$
	\dfrac{1}{\left(\dfrac{c}{d}\right)}=\dfrac{d}{c},                \quad
	\dfrac{\left(\dfrac{a}{b}\right)}{\left(\dfrac{c}{d}\right)}=\dfrac{a
	d}{b c}
	$$
	
	$$
	 \dfrac{a}{d}+\dfrac{c}{d}=\dfrac{a+c}{d}. 
	$$





## Calculs dans l'ensemble des rationnels


!!! {{ exercice("Simplification de fractions")}} 

    === "Énoncé"

         Simplifier  les  fractions suivantes  (la  lettre  $k$ désigne  un
         entier naturel non nul). 
		 
		 1. $\dfrac{32}{40}$
		 2. $\dfrac{27^{-1} \times 4^{2}}{3^{-4} \times 2^{4}}$
		 3. $8^{3} \times \dfrac{1}{4^{2}}$
		 4. $\dfrac{(-2)^{2 k+1} \times 3^{2 k-1}}{4^{k} \times 3^{-k+1}}$

	=== "Indications"

         Ohhh
		 
	
!!! {{ exercice("Sommes, produits, quotients, puissances")}}

    === "Énoncé"

         Écrire les nombres suivants sous forme d'une fraction irréductible.
		 
		 1.  $\dfrac{2}{4}-\dfrac{1}{3}$
		 1.  $\dfrac{36}{25} \times \dfrac{15}{12} \times 5$
		 1.  $\dfrac{2}{3}-0,2$
		 1. $-\dfrac{2}{15} \div\left(-\dfrac{6}{5}\right)$


	=== "Indications"

         sol
		 
!!! {{ exercice("Sommes, produits, quotients, puissances")}}

    === "Énoncé"

         Écrire les nombres suivants sous forme d'une fraction irréductible.

	     1.        $(2      \times       3       \times      5       \times
	         7)\left(\dfrac{1}{2}+\dfrac{1}{3}+\dfrac{1}{5}+\dfrac{1}{7}\right)$ 
		 2. $\left(\dfrac{136}{15}-\dfrac{28}{5}+\dfrac{62}{10}\right) \times \dfrac{21}{24}$
		 3.  $\dfrac{5^{10}  \times 7^{3}-25^{5} \times  49^{2}}{(125 \times
		     7)^{3}+5^{9} \times 14^{3}}$ 
		 4. $\dfrac{1978 \times 1979+1980 \times 21+1958}{1980 \times 1979-1978 \times 1979}$


	=== "Indications"

         sol

!!! {{ exercice("Des nombres décimaux et des fractions.")}}

    === "Énoncé"
	
		 Dans chaque cas, donner le résultat sous forme d'une fraction irréductible.
    
		 1.  0,2
		 2.  1,35
		 3. $\dfrac{1}{3}-0,3$
		 4. $0,36$
		 5. $1,5+\dfrac{2}{3}$
		 6. $\dfrac{13,5}{18,2-3,2}$

	=== "Indications"

         sol


!!! {{ exercice("Une grosse fraction")}}

    === "Énoncé"

         Écrire
         $\dfrac{0,5-\dfrac{3}{17}+\dfrac{3}{37}}{\dfrac{5}{6}-\dfrac{5}{17}+\dfrac{5}{37}}+\dfrac{0,5-\dfrac{1}{3}+\dfrac{1}{4}-0,2}{\dfrac{7}{5}-\dfrac{7}{4}+\dfrac{7}{3}-3,5}$ 
         sous forme d'une fraction irréductible. 
		 
	=== "Indications"

         sol


!!! {{ exercice("Le calcul littéral à la rescousse.")}}

    === "Énoncé"

         En  utilisant les  identités remarquables  et le  calcul littéral,
         calculer les nombres suivants. 

	     1. $\dfrac{2022}{(-2022)^{2}+(-2021)(2023)}$
		 1.  $\dfrac{1235 \times 2469-1234}{1234 \times 2469+1235}$
		 1. $\dfrac{2021^{2}}{2020^{2}+2022^{2}-2}$
		 1.  $\dfrac{4002}{1000 \times 1002-999 \times 1001}$

	=== "Indications"

         sol


!!! {{ exercice("Les fractions et le calcul littéral.")}}

    === "Énoncé"

         Mettre sous  la forme d'une  seule fraction, qu'on écrira  sous la
         forme la plus simple possible. 
		 
		 1. $\dfrac{1}{(n+1)^{2}}+\dfrac{1}{n+1}-\dfrac{1}{n}$ pour $n \in \mathbb{N}^{*}$

	     1.    $\dfrac{a^{3}-b^{3}}{(a-b)^{2}}-\dfrac{(a+b)^{2}}{a-b}$   pour
	         $(a, b) \in \mathbb{Z}^{3}$, distincts deux à deux. 

	     1.           $\dfrac{\dfrac{6(n+1)}{n(n-1)(2          n-2)}}{\dfrac{2
	        n+2}{n^{2}(n-1)^{2}}}$     pour    $n     \in    \mathbb{N}^{*}
	        \backslash\{1,2\}$ 


	=== "Indications"

         sol

	
!!! {{ exercice("Le quotient de deux sommes de Gauss.")}}

    === "Énoncé"

         Simplifier  $\dfrac{\displaystyle\sum_{k=0}^{n^{2}}  k}{\displaystyle\sum_{k=0}^{n} k}$  pour
         tout   $n   \in   \mathbb{N}^{*}$,   en   utilisant   la   formule
         $1+2+\cdots+p=\dfrac{p(p+1)}{2} \ldots \ldots$ 

	=== "Indications"

         sol


!!! {{ exercice(" Décomposition en somme d'une partie entière et d'une partie décimale.")}}

    === "Énoncé"

         Soit  $k  \in \mathbb{R}  \backslash\{1\}$  et  $x \in  \mathbb{R}
         \backslash\{2\}$.  Écrire les  fractions suivantes  sous la  forme
         $a+\dfrac{b}{c}$ avec $b<c$. 
	     
		 1. $\dfrac{29}{6}$
		 1. $\dfrac{k}{k-1}$
		 1. $\dfrac{3 x-1}{x-2}$


	=== "Indications"

         sol

!!! {{ exercice("Un produit de fractions")}}

    === "Énoncé"

         Soit    $t   \in    \mathbb{R}    \backslash\{-1\}$.   On    donne
         $A=\dfrac{1}{1+t^{2}}-\dfrac{1}{(1+t)^{2}}$                       et
         $B=\left(1+t^{2}\right)(1+t)^{2}$. 

	     Simplifier $A\times B$ autant que possible. 

	=== "Indications"

         sol

	
### Comparaison
	
	
!!! {{ exercice("Règles de comparaison.")}}

    === "Énoncé"

         Comparer les  fractions suivantes  avec le  signe $«>»,  «<»$ ou
         $«=»$.
		 
	     1.  $\dfrac{3}{5} \ldots \dfrac{5}{9}$
		 2. $\dfrac{12}{11} \ldots \dfrac{10}{12}$
		 3. $\dfrac{125}{25} \ldots \dfrac{105}{21}$


	=== "Indications"

         sol
		 
		 
!!! {{ exercice("Produit en croix.")}}

    === "Énoncé"

	     Les nombres $A=\dfrac{33215}{66317}$ et $B=\dfrac{104348}{208341}$
		 sont-ils égaux?

	=== "Indications"

         sol
		 
		 
!!! {{ exercice("Produit en croix.")}}

    === "Énoncé"

         On           pose          $A=\dfrac{100001}{1000001}$           et
         $B=\dfrac{1000001}{10000001}:$ a-t-on $A>B, A=B$ ou $A<B$ ? 

	=== "Indications"

         sol


	
## Réponses mélangées

$$
\begin{aligned}
& 1000 \quad 2 t \quad \dfrac{13}{6} \quad 3 \quad \text { Non } \quad 2^{5} \quad \dfrac{-1}{n(n+1)^{2}} \quad \dfrac{3}{2} n \quad-\dfrac{a b}{a-b} \\
& \dfrac{12}{11}>\dfrac{10}{12} \quad \dfrac{n^{3}+n}{n+1} \quad \dfrac{1}{30} \quad 1+\dfrac{1}{k-1} \quad A>B \quad \dfrac{27}{20} \quad \dfrac{1}{9} \quad 2 \quad \dfrac{1}{5} \\
& \dfrac{9}{25} \quad \dfrac{203}{24} \quad \dfrac{-10}{3} \quad 1 \quad 247 \quad 4+\dfrac{5}{6} \quad \dfrac{1}{6} \quad \dfrac{3}{5}>\dfrac{5}{9} \quad \dfrac{9}{10} \quad \dfrac{7}{15} \\
& \dfrac{4}{5} \quad-2 \times 3^{3 k-2} \quad 2022 \quad \dfrac{16}{35} \quad 3+\dfrac{5}{x-2} \quad 9 \quad \dfrac{125}{25}=\dfrac{105}{21} \quad \dfrac{1}{2}
\end{aligned}
$$



!!! {{ exercice()}}

    === "Énoncé"

         1. $\dfrac{4 x^{2}}{\dfrac{2}{x^{2}}}=$
		    {{ qcm(["$2 x^{4}$", "2", "8"],[0])}}
		 2. $\dfrac{1}{2}-\dfrac{x+1}{x}=$
		    - [ ]  $-\left(\dfrac{x+2}{2 x}\right)$
		    - [ ]  $\dfrac{x-2}{2 x}$
		    - [ ]  $\dfrac{-x+2}{2 x}$
		 3. $\dfrac{1}{x(x+1)}-\dfrac{2}{x}=$
		    - [ ]  $\dfrac{-2 x-1}{x^{2}(x+1)}$
		    - [ ]  $\dfrac{-2 x-1}{x(x+1)}$
		    - [ ]  $\dfrac{-2 x+3}{x(x+1)}$
		 4. $\dfrac{1-x^{2}}{(x-1)^{4}}=$
		    - [ ]  $-\dfrac{1+x}{(1-x)^{3}}$
		    - [ ]  $\dfrac{1+x}{(x-1)^{3}}$
		    - [ ]  $\dfrac{1+x}{(1-x)^{3}}$
		 5. $\dfrac{e^{t}}{1+e^{t}}-1=$
		    - [ ]  $\dfrac{-1}{1+e^{t}}$
		    - [ ]  $\dfrac{2 e^{t}-1}{1+e^{t}}$
		    - [ ]  $\dfrac{e^{t}-1}{1+e^{t}}$
		 6. $\dfrac{1}{\dfrac{1}{a}+\dfrac{1}{b}}=$
		    - [ ]  $a+b$
		    - [ ]  $\dfrac{1}{a+b}$
		    - [ ]  $\dfrac{a b}{a+b}$
		 7. $2-\dfrac{2 x+1}{x+2}=$
		    - [ ]  $\dfrac{3}{x+2}$
		    - [ ]  $\dfrac{5}{x+2}$
		    - [ ]  $\dfrac{4 x+3}{x+2}$
		 8. $2-\dfrac{(x+1)^{2}}{x}=$
		    - [ ]  $\dfrac{-x^{2}+1}{x}$
		    - [ ]  $\dfrac{-x^{2}-1}{x}$
		    - [ ]  $-\dfrac{x^{2}-1}{x}$
		 9. $\dfrac{1}{x-1}-\dfrac{1}{x+1}=$
		    - [ ]  $\dfrac{2}{(x-1)}$
		    - [ ]  $\dfrac{2}{x^{2}-1}$
		    - [ ]  $\dfrac{2 x}{(x-1)(x+1)}$ 10. $\dfrac{1}{x^{2}-1}-\dfrac{1}{x-1}=$
		    - [ ]  $\dfrac{x-2}{x^{2}-1}$
		    - [ ]  $\dfrac{x}{x^{2}-1}$
		    - [ ]  $\dfrac{-x}{x^{2}-1}$


	=== "Indications"

         sol

