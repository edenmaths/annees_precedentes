Ce document s'adresse aux étudiants admis au lycée Henri-IV à la rentrée 2022.

En BCPST, le premier semestre de première année est consacré pour une bonne part à reprendre (et compléter) les connaissances de Mathématiques du lycée; si cela permet de combler d'éventuelles lacunes, le rythme soutenu de la prépa rend cependant cela difficile si lesdites lacunes sont trop nombreuses ou trop profondes. Il est donc très utile d'aborder l'année en ayant fait quelques révisions estivales !

Le principal est que vous soyez au point sur ce qui a été vu en première et terminale. À ce titre, le travail du présent document n'est pas obligatoire (on pourrait lui substituer le travail du cours et des exercices faits au lycée); mais il a été pensé pour accompagner vos révisions et vous permettre, sur certains points, d'aller un peu plus loin que ce qui a été fait au lycée, afin de faciliter la transition avec la prépa.

Ce document a été conçu avec le constat que l'une des principales sources d'erreur en classe préparatoire sont les erreurs de calcul. Ce genre d'erreurs ne diminue qu'avec un entraînement sérieux et régulier au calcul ; le but de ce document est donc aussi de vous permettre de mener cet entraînement technique, en pratiquant certains types de raisonnements qui reviennent très souvent en Mathématiques :

- Simplification, factorisation, développement d'expressions;
- Dérivation et Intégration;
- Calculs de limites ;
- Résolution d'équations et d'inéquations.

Ces thèmes sont complétés à la fin du document par quelques révisions sur la trigonométrie, les suites, et la géométrie dans le plan.

Chaque partie est constituée de quelques rappels de cours, suivis d'exercices se voulant de difficulté progressive. À noter que :

- Les rappels de cours ne sont pas exhaustifs : je vous renvoie à vos cours de lycée pour cela. Avant de vous lancer dans les exercices sur un thème donné, assurez-vous d'être au point sur le cours correspondant.
- Même si ce document a très majoritairement pour but la révisions des acquis du lycée et l'entraînement au calcul, il présente aussi parfois quelques résultats hors-programme au lycée et qui seront repris (et développés) une fois en prépa.
- Certains exercices, notamment ceux situés en fin de sections, peuvent être plus techniques ou complexes que ce que vous avez rencontré au lycée. Ils sont normalement pensés pour être tous abordables avec les connaissances que vous avez pour l'instant ; pour autant, il est donc normal de les trouver plus difficiles que ce à quoi vous êtes accoutumés. - Quelques notions mentionnées (notamment tout ce qui touche à la notion de composée de fonctions) ne sont abordées qu'en Spécialité Mathématiques en terminale. Je les ai incluses car elles sont fondamentales et peuvent, à mon sens, être facilement rattrapées en autonomie; à cette fin, j'ai beaucoup détaillé les rappels de cours correspondants. À part ces exceptions, j'ai exclu de cette feuille la plupart des notions vues seulement en Spécialité ou en option Mathématiques Expertes (raisonnement par récurrence, nombres complexes, intégration par parties), qui sont plus délicates et sur lesquelles on fera des mises au point spécifiques en classe une fois l'année commencée.
- Si vous repérez des coquilles, n'hésitez pas à les noter pour me les signaler ensuite à la rentrée !

Derniers conseils :

- Cette feuille est le résultat de la fusion de plusieurs feuilles d'exercices préexistantes. A ce titre, elle contient une grande quantité d'exercices. Il est donc tout à fait concevable de ne pas avoir le temps de tout chercher; si tel est le cas :
- Il est quand même recommandé de chercher au moins quelques exercices sur chaque thème.
- Privilégiez la qualité à la quantité. Mieux vaut chercher un nombre restreint d'exercices, mais en le faisant bien, c'est-à-dire en prenant le temps d'apprendre de ses erreurs (c'est-à-dire de répondre aux questions "qu'estce qui m'a posé problème dans cette question ?" ou encore "pourquoi ai-je commis cette erreur?"), et en cherchant à prendre du recul sur les méthodes employées.
- Enfin, laissez la calculatrice au placard : aux concours, elle n'est pas toujours autorisée, autant donc commencer à s'y habituer dès maintenant!

En vous souhaitant de bonnes révisions et un excellent été, 













# Puissances, Racines, Logarithme, Exponentielle Puissances

Étant donné un réel $x \in \mathbb{R}$ et un entier non nul $n \in \mathbb{N}$, on pose :

$$
x^{n}=\underbrace{x \times \ldots \times x}_{n \text { fois }}
$$

et, par convention, $x^{0}=1$. Enfin, si $x \neq 0$, on définit les puissances entières négatives de $x$ en posant $x^{-n}=\frac{1}{x^{n}}\left(\right.$ en particulier $x^{-1}=\frac{1}{x}$ ).

Alors, pour tous entiers $n, p \in \mathbb{Z}$, on a :

$(x y)^{n}=x^{n} y^{n}, \quad\left(\frac{x}{y}\right)^{n}=\frac{x^{n}}{y^{n}}, \quad x^{n} x^{p}=x^{n+p}, \quad\left(x^{n}\right)^{p}=x^{n p}, \quad \frac{x^{p}}{x^{n}}=x^{p-n}$

## Racine carrée

Soit $x$ un réel positif. La racine carrée de $x$ est l'unique réel positif $a$ tel que $a^{2}=x$

Alors, pour tous réels positifs $x, y \in \mathbb{R}^{+}$:

$$
(\sqrt{x})^{2}=x, \quad \sqrt{x y}=\sqrt{x} \sqrt{y}, \quad \sqrt{\frac{x}{y}}=\frac{\sqrt{x}}{\sqrt{y}} .
$$

Par ailleurs, pour tout $x \in \mathbb{R}$, on a $\sqrt{x^{2}}=|x|=\left\{\begin{aligned} x & \text { si } x \geqslant 0 \\ -x & \text { si } x \leqslant 0 .\end{aligned}\right.$

## Exponentielle

La fonction exp est l'unique fonction définie et dérivable sur $\mathbb{R}$ telle que (exp) ${ }^{\prime}=\exp$ et $\exp (0)=1$. On la note aussi $\exp (x)=e^{x}$, où $e=\exp (1)$.

Pour tous réels $x, y \in \mathbb{R}$, pour tout entier $n$, on a :

$$
e^{x+y}=e^{x} \times e^{y}, \quad e^{-x}=\frac{1}{e^{x}}, \quad e^{x-y}=\frac{e^{x}}{e^{y}}, \quad e^{n x}=\left(e^{x}\right)^{n} .
$$

## Logarithme népérien

Soit $x>0$. Le logarithme népérien de $x$, noté $\ln (x)$, est l'unique réel $y$ tel que $e^{y}=x$. On obtient ainsi une fonction $\ln$, définie sur $] 0,+\infty[$, et qui est la réciproque de la fonction exponentielle, c'est-à-dire que :

$$
\text { pour tout } x>0, e^{\ln (x)}=x \text { et pour tout } x \in \mathbb{R}, \ln \left(e^{x}\right)=x
$$

La fonction ln vérifie en outre les propriétés suivantes : pour tous $x, y>0$ et $n \in \mathbb{Z}$, $\ln (x y)=\ln (x)+\ln (y), \quad \ln \left(\frac{1}{x}\right)=-\ln (x), \quad \ln \left(\frac{x}{y}\right)=\ln (x)-\ln (y), \quad \ln \left(x^{n}\right)=n \ln (x)$. On a aussi $\ln (\sqrt{x})=\frac{1}{2} \ln (x)$

## Exercice 1

Pour chaque expression, une seule égalité est correcte.

$a, b, x, C$ représentent des réels, $n$ et $k$ représentent des entiers.


16. $\\dfrac{a^{2}}{\\sqrt{a}}=$
(A) $\\sqrt{a}$
(B) $a \\sqrt{a}$
(C) $\\dfrac{1}{\\sqrt{a}}$
17. $\\dfrac{\\sqrt{a^{2}+3 a^{2}}}{\\sqrt{2 a}}=$
(A) $2 \\sqrt{a}$
(B) $\\sqrt{2 a}$
(C) $\\sqrt{\\dfrac{a}{2}}$
18. Pour $x \\geqslant 0, \\dfrac{1}{x} \\sqrt{x^{3}+2 x^{2}+3}=$
(A) $\\sqrt{x+2+\\dfrac{3}{x^{2}}}$
(B) $\\sqrt{x^{2}+2 x+\\dfrac{3}{x}}$
(C) $\\sqrt{\\dfrac{1}{x}+x^{3}+2 x^{2}+3}$
19. $2 \\times\\left(\\left(\\dfrac{\\sqrt{2}}{2}\\right)^{n}\\right)^{2}=$
(A) $2^{n}$
(B) $2^{n+1}$
(C) $\\dfrac{1}{2^{n-1}}$
20. $e^{\\left(x+\\dfrac{1}{x}\\right)}=$
(A) $e^{x}+e^{\\dfrac{1}{x}}$
(B) $e^{x} \\times e^{\\dfrac{1}{x}}$
(C) $e^{x}+e^{-x}$
21. $\\dfrac{e^{x}}{e^{-y}}=$
(A) $e^{x}-e^{y}$
(B) $e^{x-y}$
(C) $e^{x+y}$
22. $C e^{-\\ln x}=$
(A) $C x$
(B) $-C x$
(C) $\\dfrac{C}{x}$
23. $1-e^{-x}=$
(A) $\\dfrac{e^{x}-1}{e^{-x}}$
(B) $\\dfrac{e^{x}+1}{e^{-x}}$
(C) $\\dfrac{e^{x}-1}{e^{x}}$
24. $\\dfrac{1}{e^{-x}-1}=$
(A) $\\dfrac{e^{x}}{e^{x}-1}$
(B) $e^{x}-1$
(C) $\\dfrac{e^{x}}{1-e^{x}}$
25. $\\ln \\left(x y^{2}\\right)+\\ln (x)=$
(A) $2 \\ln (x y)$
(B) $2 x \\ln (y)$
(C) $\\ln (x)\\left(y^{2}+1\\right)$
26. $\\ln \\left(x^{2}-1\\right)=$
(A) $2 \\ln (x)-1$
(B) $\\ln (x-1)+\\ln (x+1)$
(C) $\\ln (x-1)-\\ln (x+1)$
27. $-3 \\ln \\left(\\dfrac{1}{2}\\right)=$
(A) $\\ln \\left(\\dfrac{3}{2}\\right)$
(B) $\\ln 8$
(C) $\\ln 6$
28. $n \\ln \\left(\\dfrac{3}{2}\\right)=$
(A) $\\ln \\left(\\left(\\dfrac{2}{3}\\right)^{n}\\right)$
(B) $\\ln \\left(\\left(\\dfrac{2}{3}\\right)^{-n}\\right)$
(C) $\\dfrac{1}{\\ln \\left(\\left(\\dfrac{2}{3}\\right)^{n}\\right)}$
29. $\\ln \\left(x+x^{2}\\right)=$
(A) $\\ln (x)+\\ln \\left(x^{2}\\right)$
(B) $\\ln (x)+\\ln (1+x)$
(C) $\\ln (x) \\times \\ln (1+x)$
30. $\\ln \\left(1+\\dfrac{1}{n}\\right)=$
(A) $\\ln (n+1)-\\ln (n)$
(B) $\\ln (n+1)+\\ln (n)$
(C) $\\dfrac{\\ln (n+1)}{\\ln (n)}$
31. $\\ln \\left(1+e^{x}\\right)=$
(A) $x+\\ln \\left(e^{-x}+1\\right)$
(B) $-\\ln \\left(e^{-x}+1\\right)$
(C) $1+\\ln \\left(e^{-x}+1\\right)$

Correction de l'exercice 1

16. $\\mathrm{B}, \\operatorname{car} \\dfrac{a^{2}}{\\sqrt{a}}=\\dfrac{a \\times a}{\\sqrt{a}}=\\dfrac{a \\times(\\sqrt{a})^{2}}{\\sqrt{a}}=a \\sqrt{a}$.
17. $\\mathrm{B}, \\operatorname{car} \\dfrac{\\sqrt{a^{2}+3 a^{2}}}{\\sqrt{2 a}}=\\dfrac{\\sqrt{4 a^{2}}}{\\sqrt{2 a}}=\\dfrac{\\sqrt{(2 a)^{2}}}{\\sqrt{2 a}}=\\dfrac{\\sqrt{2 a} \\times \\sqrt{2 a}}{\\sqrt{2 a}}=\\sqrt{2 a}$.
18. A. En effet, pour $x>0$, on a $\\sqrt{x^{2}}=|x|=x$, donc

$$
\\dfrac{1}{x} \\sqrt{x^{3}+2 x^{2}+3}=\\dfrac{1}{\\sqrt{x^{2}}} \\sqrt{x^{3}+2 x^{2}+3}=\\sqrt{\\dfrac{1}{x^{2}}\\left(x^{3}+2 x^{2}+3\\right)}=\\sqrt{x+2+\\dfrac{3}{x^{2}}} .
$$

19. C. En effet,

$2 \\times\\left(\\left(\\dfrac{\\sqrt{2}}{2}\\right)^{n}\\right)^{2}=2 \\times\\left(\\dfrac{\\sqrt{2}}{2}\\right)^{2 n}=2 \\times\\left(\\left(\\dfrac{\\sqrt{2}}{2}\\right)^{2}\\right)^{n}=2 \\times\\left(\\dfrac{2}{4}\\right)^{n}=2 \\times\\left(\\dfrac{1}{2}\\right)^{n}=\\dfrac{2}{2^{n}}=\\dfrac{1}{2^{n-1}}$

20. B, par propriété de l'exponentielle.
21. $\\mathrm{C}, \\operatorname{car} \\dfrac{e^{x}}{e^{-y}}=e^{x} e^{y}=e^{x+y}$.
22. $\\mathrm{C}, \\operatorname{car} C e^{-\\ln x}=\\dfrac{C}{e^{\\ln (x)}}=\\dfrac{C}{x}$.
23. $\\mathrm{C}$, car $1-e^{-x}=1-\\dfrac{1}{e^{x}}=\\dfrac{e^{x}-1}{e^{x}}$.
24. C, en multipliant en haut et en bas par $e^{x}$.
25. A. En effet,

$$
\\begin{aligned}
\\ln \\left(x y^{2}\\right)+\\ln (x) & =\\ln (x)+\\ln \\left(y^{2}\\right)+\\ln (x) \\\\
& =2 \\ln (x)+2 \\ln (y)=2(\\ln (x)+\\ln (y))=2 \\ln (x y)
\\end{aligned}
$$

26. B, car $\\ln \\left(x^{2}-1\\right)=\\ln ((x-1)(x+1))=\\ln (x-1)+\\ln (x+1)$.
27. B, car $-3 \\ln \\left(\\dfrac{1}{2}\\right)=3 \\ln (2)=\\ln \\left(2^{3}\\right)=\\ln (8)$.
28. B, par propriété de ln.
29. B, car $\\ln \\left(x+x^{2}\\right)=\\ln (x(1+x))=\\ln (x)+\\ln (1+x)$.
30. A, car $\\ln \\left(1+\\dfrac{1}{n}\\right)=\\ln \\left(\\dfrac{n+1}{n}\\right)=\\ln (n+1)-\\ln (n)$.
31. $\\mathrm{A}$, car $\\ln \\left(1+e^{x}\\right)=\\ln \\left(e^{x}\\left(e^{-x}+1\\right)\\right)=\\ln \\left(e^{x}\\right)+\\ln \\left(e^{-x}+1\\right)=x+\\ln \\left(e^{-x}+1\\right)$.

## Exercice 2

Soient $a$ et $b$ deux réels non nuls. Simplifier au maximum les expressions ci-dessous :

$$
\\begin{gathered}
A=\\left[\\left(a^{2} b^{3}\\right)^{4}\\right]^{5} \\quad B=\\left(a^{3} b^{-4}\\right)^{2}\\left(-2 a^{-5} b^{6}\\right)^{3} \\\\
C=\\left(\\dfrac{a^{2}}{b^{3}}\\right)^{2}\\left(\\dfrac{a}{4 b}\\right)^{3}\\left(\\dfrac{b^{2}}{a}\\right)^{2} \\quad D=\\left(\\dfrac{a^{1} b^{-2}}{a^{-3} b^{4}}\\right)^{5}\\left(\\dfrac{a^{-6} b^{5}}{a^{4} b^{-3}}\\right)^{3}
\\end{gathered}
$$

Correction de l'exercice 2

$$
A=a^{40} b^{60} \\quad B=-\\dfrac{8 b^{10}}{a^{9}} \\quad C=\\dfrac{a^{5}}{64 b^{5}} \\quad D=\\dfrac{1}{a^{10} b^{6}}
$$

## Pour aller plus loin : puissances non entières

On aborde dans cette section un concept qui n'est pas forcément présenté en terminale, et que vous verrez plus en détails en début d'année en BCPST. Si vous vous sentez à l'aise sur les calculs de puissances, ce qui suit vous permettra de compléter ce que vous avez vu au lycée; dans le cas contraire, n'hésitez pas à sauter cette section en première lecture.

On constate que, pour $x \\in \\mathbb{R}$ et $n \\in \\mathbb{N}$, on peut écrire

$$
x^{n}=\\left(e^{\\ln (x)}\\right)^{n}=e^{n \\ln (x)}
$$

En utilisant cette expression, on peut généraliser la notion de puissance : pour $x>0$ et $\\alpha \\in \\mathbb{R}$ (pas forcément entier!), on définit

$$
x^{\\alpha}=e^{\\alpha \\ln (x)}
$$

Avec cette nouvelle définition, on peut ainsi parler, par exemple, de $3^{1.2}$ ou encore de $5^{\\pi}$. De plus, cette nouvelle notion de puissance vérifie les mêmes propriétés que les puissances entières auxquelles vous êtes habitués (cf. règles de calcul énoncées en début de section $\\S I \\bar{I}$.

Un des intérêts de cette nouvelle définition est que l'on peut interpréter la racine carrée comme une "puissance $1 / 2$ " :

$$
\\text { pour tout } x>0, \\quad x^{\\dfrac{1}{2}}=\\sqrt{x} .
$$

En effet, pour justifier cela, il suffit de vérifier que $x^{\\dfrac{1}{2}}$ est bien le nombre qui, élevé au carré, est égal à $x$; or on a bien :

$$
\\left(x^{\\dfrac{1}{2}}\\right)^{2}=x^{\\dfrac{1}{2} \\times 2}=x
$$

D'une manière plus générale, si $x>0$ et $n \\in \\mathbb{N}^{*}$, alors $x^{\\dfrac{1}{n}}$ est l'unique réel qui, élevé à la puissance $n$, est égal à $x$. On le note aussi $\\sqrt[n]{x}$; c'est ce que l'on appelle la racine $n$-ième de $x$.

## Calcul algébrique

## II.1 Opérations sur les fractions

Pour tous réels non nuls $a, b, c, d, k$ :

$$
\\dfrac{a}{b} \\times \\dfrac{c}{d}=\\dfrac{a \\times c}{b \\times d}, \\quad \\dfrac{k a}{k b}=\\dfrac{a}{b}, \\quad \\dfrac{1}{\\left(\\dfrac{c}{d}\\right)}=\\dfrac{d}{c}, \\quad \\dfrac{\\left(\\dfrac{a}{b}\\right)}{\\left(\\dfrac{c}{d}\\right)}=\\dfrac{a d}{b c}, \\quad \\dfrac{a}{d}+\\dfrac{c}{d}=\\dfrac{a+c}{d} .
$$

Attention $: \\dfrac{a}{d}-\\dfrac{b+c}{d}=\\dfrac{a-(b+c)}{d}=\\dfrac{a-b-c}{d}$.

## II.2 Identités remarquables

Pour tous réels $a$ et $b$ :

$$
(a+b)^{2}=a^{2}+2 a b+b^{2}, \\quad(a-b)^{2}=a^{2}-2 a b+b^{2}, \\quad a^{2}-b^{2}=(a+b)(a-b) .
$$

Remarques :

- La formule pour $(a+b)^{2}$ se généralise au cas d'une somme de trois termes ou plus; il faut juste tenir compte de tous les doubles produits possibles :

$$
(a+b+c)^{2}=a^{2}+b^{2}+c^{2}+2 a b+2 b c+2 a c
$$

- On voit en première année de classe préparatoire la formule du binôme de Newton, qui donne une expression générale pour $(a+b)^{n}$ :

$$
(a+b)^{n}=\\sum_{k=0}^{n}\\left(\\begin{array}{l}
n \\\\
k
\\end{array}\\right) a^{k} b^{n-k}
$$

Vous pouvez déjà retenir ce que donne cette formule dans le cas $n=3$ :

$$
(a+b)^{3}=a^{3}+3 a^{2} b+3 a b^{2}+b^{3}
$$

## Exercice 3

Pour chaque expression, une seule égalité est correcte. $a, b, x, t$ représentent des réels.

1. $\\dfrac{4 x^{2}}{\\dfrac{2}{x^{2}}}=$
(A) $2 x^{4}$
(B) 2
(C) 8
2. $\\dfrac{1}{2}-\\dfrac{x+1}{x}=$
(A) $-\\left(\\dfrac{x+2}{2 x}\\right)$
(B) $\\dfrac{x-2}{2 x}$
(C) $\\dfrac{-x+2}{2 x}$
3. $\\dfrac{1}{x(x+1)}-\\dfrac{2}{x}=$
(A) $\\dfrac{-2 x-1}{x^{2}(x+1)}$
(B) $\\dfrac{-2 x-1}{x(x+1)}$
(C) $\\dfrac{-2 x+3}{x(x+1)}$
4. $\\dfrac{1-x^{2}}{(x-1)^{4}}=$
(A) $-\\dfrac{1+x}{(1-x)^{3}}$
(B) $\\dfrac{1+x}{(x-1)^{3}}$
(C) $\\dfrac{1+x}{(1-x)^{3}}$
5. $\\dfrac{e^{t}}{1+e^{t}}-1=$
(A) $\\dfrac{-1}{1+e^{t}}$
(B) $\\dfrac{2 e^{t}-1}{1+e^{t}}$
(C) $\\dfrac{e^{t}-1}{1+e^{t}}$
6. $\\dfrac{1}{\\dfrac{1}{a}+\\dfrac{1}{b}}=$
(A) $a+b$
(B) $\\dfrac{1}{a+b}$
(C) $\\dfrac{a b}{a+b}$
7. $2-\\dfrac{2 x+1}{x+2}=$
(A) $\\dfrac{3}{x+2}$
(B) $\\dfrac{5}{x+2}$
(C) $\\dfrac{4 x+3}{x+2}$
8. $2-\\dfrac{(x+1)^{2}}{x}=$
(A) $\\dfrac{-x^{2}+1}{x}$
(B) $\\dfrac{-x^{2}-1}{x}$
(C) $-\\dfrac{x^{2}-1}{x}$
9. $\\dfrac{1}{x-1}-\\dfrac{1}{x+1}=$
(A) $\\dfrac{2}{(x-1)}$
(B) $\\dfrac{2}{x^{2}-1}$
(C) $\\dfrac{2 x}{(x-1)(x+1)}$ 10. $\\dfrac{1}{x^{2}-1}-\\dfrac{1}{x-1}=$
(A) $\\dfrac{x-2}{x^{2}-1}$
(B) $\\dfrac{x}{x^{2}-1}$
(C) $\\dfrac{-x}{x^{2}-1}$
10. $\\left(\\dfrac{1-\\sqrt{5}}{2}\\right)^{2}=$
(A) $\\dfrac{3+\\sqrt{5}}{2}$
(B) $\\dfrac{3-\\sqrt{5}}{2}$
(C) $\\dfrac{-2+\\sqrt{5}}{2}$
11. $\\dfrac{1}{\\sqrt{2}-1}=$
(A) $\\dfrac{-1}{\\sqrt{2}+1}$
(B) $\\sqrt{2}+1$
(C) $\\dfrac{1}{\\sqrt{2}}-1$
12. $\\left(1-x^{2}\\right)(1-x)=$
(A) $(1-x)^{2}(1+x)$
(B) $(1-x)(1+x)^{2}$
(C) $1-x^{3}$
13. $2 x^{2}-5 x-3=$
(A) $(2 x+1)(x-3)$
(B) $(2 x-1)(x+3)$
(C) $\\left(x+\\dfrac{1}{2}\\right)(x-3)$
14. $5-2(x-1)^{2}=$
(A) $-2 x^{2}+4 x+7$
(B) $-2 x^{2}+4 x+3$
(C) $-2 x^{2}-4 x+7$
15. $3(x+2)^{2}-8=$
(A) $3 x^{2}+4 x-4$
(B) $3 x^{2}+12 x+4$
(C) $3 x^{2}+6 x+4$
16. $(3 x-2)^{2}-(3 x-2)(x-1)=$
(A) $(3 x-2)(2 x-1)$
(B) $(3 x-2)(2 x-3)$
(C) $(3 x-2)(2-x)$

## Correction de l'exercice 3

1. $\\mathrm{A}$;
2. A (mise au même dénominateur)
3. $\\mathrm{B} \\operatorname{car} \\dfrac{1}{x(x+1)}-\\dfrac{2}{x}=\\dfrac{1}{x(x+1)}-\\dfrac{2(x+1)}{x(x+1)}=\\dfrac{-2 x-1}{x(x+1)}$.
4. $\\mathrm{C} \\operatorname{car} \\dfrac{1-x^{2}}{(x-1)^{4}}=\\dfrac{(1-x)(1+x)}{(x-1)^{4}}=\\dfrac{(1-x)(1+x)}{(1-x)^{4}}=\\dfrac{1+x}{(1-x)^{3}}$.

Ici, on a utilisé le fait que $(x-1)^{4}=(-(1-x))^{4}=(1-x)^{4}$ puisque 4 est pair.

5. A (mise au même dénominateur)
6. $\\mathrm{C}$ (multiplier en haut et en bas par $a b)$
7. A (mise au même dénominateur)
8. B (mise au même dénominateur)
9. B (mise au même dénominateur, en utilisant que $\\left.(x-1)(x+1)=x^{2}-1\\right)$
10. C. En effet (en essayant de minimiser les calculs) :

$$
\\begin{aligned}
\\dfrac{1}{x^{2}-1}-\\dfrac{1}{x-1} & =\\dfrac{1}{(x-1)(x+1)}-\\dfrac{1}{x-1} \\\\
& =\\dfrac{1}{(x-1)(x+1)}-\\dfrac{x+1}{(x-1)(x+1)}=\\dfrac{-x}{(x-1)(x+1)}=\\dfrac{-x}{x^{2}-1} .
\\end{aligned}
$$

11. $\\mathrm{B}$ (on développe en utilisant $\\left.(a-b)^{2}=a^{2}-2 a b+b^{2}\\right)$.
12. B. Pour obtenir cela, on multiplie en haut et en bas par $\\sqrt{2}+1$ (ce que l'on appelle la "quantité conjuguée" à $\\sqrt{2}-1$ ).

$$
\\dfrac{1}{\\sqrt{2}-1}=\\dfrac{\\sqrt{2}+1}{(\\sqrt{2}-1)(\\sqrt{2}+1)}=\\dfrac{\\sqrt{2}+1}{(\\sqrt{2})^{2}-1^{2}}=\\dfrac{\\sqrt{2}+1}{2-1}=\\sqrt{2}+1 .
$$

13. $\\mathrm{A}$, car $\\left(1-x^{2}\\right)(1-x)=(1-x)(1+x)(1-x)=(1-x)^{2}(1+x)$.
14. $\\mathrm{A}$;
15. $\\mathrm{B}$
16. $\\mathrm{B}$;
17. A (factoriser par $3 x-2$ ).

## Exercice 4

Simplifier et factoriser au maximum chacune des expressions suivantes.

1. $\\dfrac{x^{2}(x-1)}{2}-\\dfrac{x(x-1)(2 x-1)}{6}$
2. $\\dfrac{x(2 x-1)}{3}-\\dfrac{x(x-1)}{2}$
3. $\\dfrac{x(x+1)}{6}-\\dfrac{(x+1)^{2}}{9}$
4. $\\dfrac{x^{2}(x+1)^{2}}{4}-\\dfrac{x(x+1)(2 x+1)}{6}$
5. $\\dfrac{x(x+1)}{2}-\\dfrac{x(x-1)}{4}-\\dfrac{2 x-1}{6}$

Indications pour l'exercice 4

Surtout ne pas développer directement les expressions données! Il vaut mieux :

- identifier les facteurs communs, et les mettre en facteur;
- mettre toutes les fractions au même dénominateur (pour cela, déterminer le plus petit multiple possible des dénominateurs en présence), puis les regrouper. Correction de l'exercice 4

1. En suivant la stratégie décrite dans l'indication :

$$
\\begin{aligned}
\\dfrac{x^{2}(x-1)}{2}-\\dfrac{x(x-1)(2 x-1)}{6} & =x(x-1)\\left(\\dfrac{x}{2}-\\dfrac{2 x-1}{6}\\right) \\\\
& =x(x-1)\\left(\\dfrac{3 x}{6}-\\dfrac{2 x-1}{6}\\right) \\\\
& =x(x-1)\\left(\\dfrac{3 x-(2 x-1)}{6}\\right) \\quad \\text { (ne pas oublier la parenthèse !) } \\\\
& =\\dfrac{x(x-1)(x+1)}{6} .
\\end{aligned}
$$

2. On prend 6 comme dénominateur commun, et on met $x$ en facteur :

$\\dfrac{x(2 x-1)}{3}-\\dfrac{x(x-1)}{2}=x\\left(\\dfrac{2 x-1}{3}-\\dfrac{x-1}{2}\\right)=x\\left(\\dfrac{2(2 x-1)}{6}-\\dfrac{3(x-1)}{6}\\right)=\\dfrac{x(x+1)}{6}$.

3. De même, $\\dfrac{x(x+1)}{6}-\\dfrac{(x+1)^{2}}{9}=(x+1)\\left(\\dfrac{3 x}{18}-\\dfrac{2(x+1)}{18}\\right)=\\dfrac{(x+1)(x-2)}{18}$.
4. On prend 12 comme dénominateur commun, et on met $x(x+1)$ en facteur:

$$
\\begin{aligned}
\\dfrac{x^{2}(x+1)^{2}}{4}-\\dfrac{x(x+1)(2 x+1)}{6} & =x(x+1)\\left(\\dfrac{x(x+1)}{4}-\\dfrac{2 x+1}{6}\\right) \\\\
& =x(x+1)\\left(\\dfrac{3 x(x+1)}{12}-\\dfrac{2(2 x+1)}{12}\\right) \\\\
& =\\dfrac{x(x+1)\\left(3 x^{2}-x-2\\right)}{12} .
\\end{aligned}
$$

On remarque que le polynôme $3 x^{2}-x-2$ s'annule pour $x=1$, on peut donc le factoriser par $x-1$; en tâtonnant un peu avec les coefficients, on trouve que $3 x^{2}-x-2=(x-1)(3 x+2)$.

Finalement : $\\dfrac{x^{2}(x+1)^{2}}{4}-\\dfrac{x(x+1)(2 x+1)}{6}=\\dfrac{x(x+1)(x-1)(3 x+2)}{12}$.

5. En prenant 12 comme dénominateur commun :

$\\dfrac{x(x+1)}{2}-\\dfrac{x(x-1)}{4}-\\dfrac{2 x-1}{6}=\\dfrac{6 x(x+1)}{12}-\\dfrac{3 x(x-1)}{12}-\\dfrac{2(2 x-1)}{12}=\\dfrac{3 x^{2}+5 x+2}{12}$. De même qu'à la question précédente, on remarque que le polynôme $3 x^{2}+5 x+2$ s'annule pour une valeur "évidente" : $x=-1$ (on pourrait aussi trouver cette valeur en faisant une recherche de racines classique, via un calcul de discriminant!). On peut donc factoriser $3 x^{2}+5 x+2$ par $x+1$, et on voit que $3 x^{2}+5 x+2=(x+1)(3 x+2)$. Ainsi finalement :

$$
\\dfrac{x^{2}(x+1)^{2}}{4}-\\dfrac{x(x+1)(2 x+1)}{6}=\\dfrac{(x+1)(3 x+2)}{12} .
$$

## III Équations, inéquations

## III.1 Équations polynômiales du second degré

On considère l'équation $(E): a x^{2}+b x+c=0$, où $a, b, c$ sont des réels fixés (avec $a \\neq 0)$. On définit le discriminant $\\Delta$ de cette équation par $\\Delta=b^{2}-4 a c$.

- Si $\\Delta \\geq 0$, alors l'équation $(E)$ a 2 solutions réelles :

$$
x_{1}=\\dfrac{-b-\\sqrt{\\Delta}}{2 a} \\text { et } x_{2}=\\dfrac{-b+\\sqrt{\\Delta}}{2 a} .
$$

On a alors la factorisation suivante, pour tout réel $x$ :

$$
a x^{2}+b x+c=a\\left(x-x_{1}\\right)\\left(x-x_{2}\\right)
$$

On en déduit notamment que $a x^{2}+b x+c$ est du signe opposé à celui de $a$ entre les deux racines $x_{1}$ et $x_{2}$, et du même signe que $a$ hors de $\\left[x_{1}, x_{2}\\right]$
![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-08.jpg?height=320&width=1180&top_left_y=873&top_left_x=226)

- Si $\\Delta=0$, alors l'équation $(E)$ a une solution réelle $: x_{0}=\\dfrac{-b}{2 a}$ On a alors la factorisation suivante, pour tout réel $x$ :

$$
a x^{2}+b x+c=a\\left(x-x_{0}\\right)^{2}
$$

En particulier, $a x^{2}+b x+c$ est de signe constant, égal au signe de $a$.
![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-08.jpg?height=396&width=1178&top_left_y=1584&top_left_x=228)

- Si $\\Delta<0$, alors l'équation $(E)$ ne possède aucune solution réelle. En particulier, $a x^{2}+b x+c$ est de signe (strict) constant, égal au signe de $a$.
![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-08.jpg?height=440&width=1202&top_left_y=254&top_left_x=1578)


## III.2 Résolution d'équations

On commence toujours par déterminer le domaine sur lequel on résout (en d'autres termes : pour quelles valeurs de $x$ l'équation fait-elle sens?).

Puis on passe à la résolution proprement dite. Il y a en réalité assez peu d'équations que l'on sait résoudre : guère plus, en fait, que les équations polynomiales du premier degré (du type $a x+b=0$ ) ou du second degré (du type $a x^{2}+b x+c=0$ ).

Face à une équation plus compliquée, la stratégie est donc de se ramener à une (ou plusieurs) équation(s) de ce type. Cela peut se faire :

- Par changement de variable, en introduisant une nouvelle variable $X$ (dépendant de $x$ ). On obtient une nouvelle équation en $X$, que l'on résout ; puis on n'oublie pas de revenir à la variable initiale, en déterminant quelles sont les valeurs de $x$ associées aux valeurs trouvées pour $X$.
- En passant tous les termes à gauche du signe "=" et en factorisant au maximum l'expression obtenue. On utilise alors le fait qu'un produit de termes est nul si et seulement si au moins l'un des termes est nul.

On peut aussi parfois simplifier l'équation en utilisant que, pour tous réels $x, y$,

$$
\\begin{aligned}
& e^{x}=e^{y} \\Leftrightarrow x=y \\\\
& \\ln (x)=\\ln (y) \\Leftrightarrow x=y \\quad(\\text { pour } x, y>0) \\\\
& \\sqrt{x}=\\sqrt{y} \\Leftrightarrow x=y \\quad(\\text { pour } x, y \\geqslant 0) \\\\
& x^{2}=y^{2} \\Leftrightarrow x=y \\text { ou } x=-y \\quad \\text { (de même pour toute puissance paire) } \\\\
& x^{3}=y^{3} \\Leftrightarrow x=y \\quad \\text { (de même pour toute puissance impaire) } \\\\
& |x|=|y| \\Leftrightarrow x=y \\text { ou } x=-y
\\end{aligned}
$$

Exercice 5 Résoudre dans $\\mathbb{R}$ :

1. $2 x^{2}+3 x-2=0$
2. $x(x+2)=2 x(3 x-4)$
3. $x^{2}-x-1=0$
4. $2 x^{2}+x+2=2 x^{2}+3 x-3$
5. $6 x^{4}-5 x^{2}+1=0$
6. $\\left|2 x^{2}+x+2\\right|=\\left|2 x^{2}+3 x-3\\right|$
7. $x^{3}-x^{2}-2 x=0$
8. $\\left(2 x^{2}+x+2\\right)^{2}=\\left(2 x^{2}+3 x-3\\right)^{2}$
9. $6 e^{2 x}-5 e^{x}+1=0$
10. $e^{x-1}=\\dfrac{1}{2}$
11. $x+\\sqrt{x}-1=0$
12. $\\ln \\left(x^{2}-4 e^{2}\\right)=1+\\ln (3 x)$
13. $x+\\dfrac{1}{x}=3$
14. $\\ln \\left(1+e^{-x}\\right)=x$
15. $\\dfrac{x+3}{x+2}=\\dfrac{x-2}{x-1}$

Indications pour l'exercice 5

1. 
2. 
3. Poser $X=x^{2}$.
4. Commencer par factoriser un peu l'expression.
5. Poser $X=e^{x}$.
6. Poser $X=\\sqrt{x}$.
7. Poser $X=\\sqrt{x}$.
8. Multiplier par $x$.
9. Poser $X=e^{x}$.
10. Se ramener à " $\\ldots=0 "$.
11. Se ramener à " $\\ldots=0 "$.
12. $|a|=|b| \\Leftrightarrow a=b$ ou $a=-b$
13. $a^{2}=b^{2} \\Leftrightarrow a=b$ ou $a=-b$
14. Utiliser la fonction $\\ln$.
15. Utiliser la fonction exp... et attention au domaine!
16. Utiliser la fonction exp.
17. Se débarrasser des fractions en multipliant par $x+2$ et $x-1$.

## Correction de l'exercice 5

1. On résout pour $x \\in \\mathbb{R}$. C'est une équation polynomiale de degré 2 , de discriminant $\\Delta=3^{2}-4 \\times 2 \\times(-2)=25>0$. Elle admet donc deux solutions réelles distinctes :

$$
x_{1}=\\dfrac{-3-\\sqrt{25}}{4}=-2 \\text { et } x_{2}=\\dfrac{-3+\\sqrt{25}}{4}=\\dfrac{1}{2} .
$$

Ainsi $S=\\left\\{-2, \\dfrac{1}{2}\\right\\}$

2. On résout pour $x \\in \\mathbb{R}$.

En procédant comme à la question précédente, $S=\\left\\{\\dfrac{1-\\sqrt{5}}{2}, \\dfrac{1+\\sqrt{5}}{2}\\right\\}$

3. On résout pour $x \\in \\mathbb{R}$. Posons $X=x^{2}$. Alors

$$
\\begin{aligned}
6 x^{4}-5 x^{2}+1=0 & \\Leftrightarrow 6 X^{2}-5 X+1=0 \\\\
& \\Leftrightarrow(\\ldots) \\Leftrightarrow X=\\dfrac{1}{3} \\text { ou } X=\\dfrac{1}{2} \\\\
& \\Leftrightarrow x^{2}=\\dfrac{1}{3} \\text { ou } x^{2}=\\dfrac{1}{2} \\\\
& \\Leftrightarrow x=\\dfrac{1}{\\sqrt{3}} \\text { ou } x=\\dfrac{-1}{\\sqrt{3}} \\text { ou } x=\\dfrac{1}{\\sqrt{2}} \\text { ou } x=\\dfrac{-1}{\\sqrt{2}} .
\\end{aligned}
$$

et ainsi $S=\\left\\{\\dfrac{1}{\\sqrt{2}},-\\dfrac{1}{\\sqrt{2}}, \\dfrac{1}{\\sqrt{3}},-\\dfrac{1}{\\sqrt{3}}\\right\\}$

4. On résout pour $x \\in \\mathbb{R}$. On a :

$$
\\begin{aligned}
x^{3}-x^{2}-2 x=0 & \\Leftrightarrow x\\left(x^{2}-x-2\\right)=0 \\\\
& \\Leftrightarrow x=0 \\text { ou } x^{2}-x-2=0 \\\\
& \\Leftrightarrow x=0 \\text { ou } x=-1 \\text { ou } x=2 .
\\end{aligned}
$$

Ainsi $S=\\{0,-1,2\\}$.

5. On résout pour $x \\in \\mathbb{R}$. Posons $X=e^{x}$. Alors

$$
\\begin{aligned}
6 e^{2 x}-5 e^{x}+1=0 & \\Leftrightarrow 6 X^{2}-5 X+1=0 \\\\
& \\Leftrightarrow(\\ldots) \\Leftrightarrow X=\\dfrac{1}{3} \\text { ou } X=\\dfrac{1}{2} \\\\
& \\Leftrightarrow e^{x}=\\dfrac{1}{3} \\text { ou } e^{x}=\\dfrac{1}{2} \\\\
& \\Leftrightarrow x=\\ln \\left(\\dfrac{1}{3}\\right)=-\\ln (3) \\text { ou } x=\\ln \\left(\\dfrac{1}{2}\\right)=-\\ln (2)
\\end{aligned}
$$

Ainsi $S=\\{-\\ln (2),-\\ln (3)\\}$. 6. On résout pour $x \\in[0,+\\infty[$ (pour que $\\sqrt{x}$ soit bien définie). Posons $X=\\sqrt{x}$. Alors

$$
\\begin{aligned}
x-2 \\sqrt{x}+1=0 & \\Leftrightarrow X^{2}-2 X+1=0 \\\\
& \\Leftrightarrow X=1 \\Leftrightarrow \\sqrt{x}=1 \\Leftrightarrow x=1 .
\\end{aligned}
$$

Ainsi $S=\\{1\\}$

7. On résout pour $x \\in[0,+\\infty[$ (pour que $\\sqrt{x}$ soit bien définie). Posons $X=\\sqrt{x}$. Alors

$$
\\begin{aligned}
x+\\sqrt{x}-1=0 & \\Leftrightarrow X^{2}+X-1=0 \\\\
& \\Leftrightarrow X=\\dfrac{-1-\\sqrt{5}}{2} \\text { ou } X=\\dfrac{-1+\\sqrt{5}}{2} \\\\
& \\Leftrightarrow \\sqrt{x}=\\dfrac{-1-\\sqrt{5}}{2} \\text { ou } \\sqrt{x}=\\dfrac{-1+\\sqrt{5}}{2} .
\\end{aligned}
$$

Le première équation n'a pas de solution car $\\sqrt{x} \\geqslant 0$ tandis que $\\dfrac{-1-\\sqrt{5}}{2}<0$. Pour la seconde, au contraire, $\\dfrac{-1+\\sqrt{5}}{2} \\geqslant 0$, donc

$$
\\sqrt{x}=\\dfrac{-1+\\sqrt{5}}{2} \\Leftrightarrow x=\\left(\\dfrac{-1+\\sqrt{5}}{2}\\right)^{2}
$$

Ainsi $S=\\left\\{\\left(\\dfrac{-1+\\sqrt{5}}{2}\\right)^{2}\\right\\}$

8. On résout pour $x \\neq 0$ (pour que $\\dfrac{1}{x}$ soit bien défini). Alors :

$$
\\begin{aligned}
x+\\dfrac{1}{x}=3 & \\left.\\Leftrightarrow x^{2}+1=3 x \\quad \\text { (en multipliant par } x, \\text { qui est non nul }\\right) \\\\
& \\Leftrightarrow x^{2}-3 x+1=0 .
\\end{aligned}
$$

C'est une équation du second degré : en l'étudiant par la méthode classique (discriminant, etc.), on trouve que $S=\\left\\{\\dfrac{3-\\sqrt{5}}{2}, \\dfrac{3+\\sqrt{5}}{2}\\right\\}$.

9. On résout pour $x \\in \\mathbb{R}$. Posons $X=e^{x}$. Alors $e^{-x}=\\dfrac{1}{X}$, donc :

$$
\\begin{aligned}
e^{x}+2 e^{-x}=3 & \\Leftrightarrow X+\\dfrac{2}{X}=3 \\\\
& \\Leftrightarrow X^{2}+2=3 X \\quad \\text { (en multipliant par } X, \\text { qui est non nul car } X=e^{x} \\text { ) } \\\\
& \\Leftrightarrow X^{2}-3 X+2=0 \\\\
& \\Leftrightarrow X=1 \\text { ou } X=2 \\\\
& \\Leftrightarrow e^{x}=1 \\text { ou } e^{x}=2 \\\\
& \\Leftrightarrow x=\\ln (1)=0 \\text { ou } x=\\ln (2)
\\end{aligned}
$$

Ainsi $S=\\{\\ln (2), 0\\}$ 10. On résout pour $x \\in \\mathbb{R}$.

On évite de développer ici, car il y a un facteur commun dont on peut profiter :

$$
\\begin{aligned}
x(x+2)=2 x(3 x-4) & \\Leftrightarrow x(x+2)-2 x(3 x-4)=0 \\\\
& \\Leftrightarrow x(x+2-2(3 x-4))=0 \\\\
& \\Leftrightarrow x(-5 x+10)=0 \\\\
& \\Leftrightarrow x=0 \\text { ou }-5 x+10=0 \\\\
& \\Leftrightarrow x=0 \\text { ou } x=2 .
\\end{aligned}
$$

Ainsi $S=\\{0,2\\}$

11. On résout pour $x \\in \\mathbb{R}$.

$$
2 x^{2}+x+2=2 x^{2}+3 x-3 \\Leftrightarrow 2 x-5=0 \\Leftrightarrow x=\\dfrac{5}{2} .
$$

Ainsi $S=\\left\\{\\dfrac{5}{2}\\right\\}$

12. On résout pour $x \\in \\mathbb{R}$.

$\\left|2 x^{2}+x+2\\right|=\\left|2 x^{2}+3 x-3\\right| \\Leftrightarrow 2 x^{2}+x+2=2 x^{2}+3 x-3$ ou $2 x^{2}+x+2=-\\left(2 x^{2}+3 x-3\\right)$

$$
\\begin{aligned}
& \\Leftrightarrow 2 x-5=0 \\text { ou } 4 x^{2}+4 x-1=0 \\\\
& \\Leftrightarrow x=\\dfrac{5}{2} \\text { ou } x=\\dfrac{-1-\\sqrt{2}}{2} \\text { ou } x=\\dfrac{-1+\\sqrt{2}}{2} .
\\end{aligned}
$$

Ainsi $S=\\left\\{\\dfrac{5}{2}, \\dfrac{-1-\\sqrt{2}}{2}, \\dfrac{-1+\\sqrt{2}}{2}\\right\\}$

13. On résout pour $x \\in \\mathbb{R}$.

$$
\\begin{aligned}
\\left(2 x^{2}+x+2\\right)^{2}=\\left(2 x^{2}+3 x-3\\right)^{2} & \\Leftrightarrow \\sqrt{\\left(2 x^{2}+x+2\\right)^{2}}=\\sqrt{\\left(2 x^{2}+3 x-3\\right)^{2}} \\\\
& \\Leftrightarrow\\left|2 x^{2}+x+2\\right|=\\left|2 x^{2}+3 x-3\\right|
\\end{aligned}
$$

On retombe sur l'équation étudiée à la question précédente!

Ainsi $S=\\left\\{\\dfrac{5}{2}, \\dfrac{-1-\\sqrt{2}}{2}, \\dfrac{-1+\\sqrt{2}}{2}\\right\\}$.

14. On résout pour $x \\in \\mathbb{R}$, en utilisant le fait que $y=e^{x} \\Leftrightarrow x=\\ln (y)$ :

$$
e^{x-1}=\\dfrac{1}{2} \\Leftrightarrow x-1=\\ln \\left(\\dfrac{1}{2}\\right) \\Leftrightarrow x=1+\\ln \\left(\\dfrac{1}{2}\\right)=1-\\ln (2) .
$$

Ainsi $S=\\{1-\\ln (2)\\}$ 15. Pour que les deux logarithmes soient bien définies, il faut et il suffit que $3 x>0$ et que $x^{2}-4 e^{2}>0$. Or

$$
\\begin{aligned}
x^{2}-4 e^{2}>0 & \\Leftrightarrow x^{2}>4 e^{2} \\\\
& \\Leftrightarrow \\sqrt{x^{2}}>\\sqrt{4 e^{2}} \\quad \\text { car la racine carrée est strictement croissante sur }[0,+\\infty[ \\\\
& \\Leftrightarrow|x|>2 e .
\\end{aligned}
$$

Pour que cette condition et la condition $3 x>0$ soient toutes deux vérifiées, il faut donc que $x \\in] 2 e,+\\infty[$. On résout donc pour $x$ dans cet intervalle.

Appliquons l'exponentielle à l'équation étudiée :

$$
\\begin{aligned}
\\ln \\left(x^{2}-4 e^{2}\\right)=1+\\ln (3 x) & \\Leftrightarrow e^{\\ln \\left(x^{2}-4 e^{2}\\right)}=e^{1+\\ln (3 x)} \\\\
& \\Leftrightarrow x^{2}-4 e^{2}=e^{1} e^{\\ln (3 x)} \\\\
& \\Leftrightarrow x^{2}-4 e^{2}=3 e x \\\\
& \\Leftrightarrow x^{2}-3 e x-4 e^{2}=0 .
\\end{aligned}
$$

C'est une équation du second degré de discriminant $\\Delta=25 e^{2}>0$, elle admet donc deux solutions : $\\dfrac{3 e-5 e}{2}=-e$ (impossible car $x$ est supposé positif), et $\\dfrac{3 e+5 e}{2}=4 e($ qui convient bien car $4 e \\in] 2 e,+\\infty[)$.

Ainsi $S=\\{4 e\\}$

16. Pour tout $x \\in \\mathbb{R}$, on a $e^{-x}>0$, donc $1+e^{-x}>0$ et donc $\\ln \\left(1+e^{-x}\\right)$ est bien défini. On résout donc pour $x \\in \\mathbb{R}$, en utilisant le fait que $\\ln (y)=x \\Leftrightarrow y=e^{x}$ :

$$
\\ln \\left(1+e^{-x}\\right)=x \\Leftrightarrow 1+e^{-x}=e^{x} .
$$

Posons alors $X=e^{x}$

$$
\\begin{aligned}
1+e^{-x}=e^{x} & \\Leftrightarrow 1+\\dfrac{1}{X}=X \\\\
& \\Leftrightarrow X+1=X^{2} \\quad \\text { (on multiplie par } X, \\text { qui est non nul car } X=e^{x} \\text { ) } \\\\
& \\Leftrightarrow X^{2}-X-1=0 \\\\
& \\Leftrightarrow X=\\dfrac{1-\\sqrt{5}}{2} \\text { ou } X=\\dfrac{1+\\sqrt{5}}{2} \\\\
& \\Leftrightarrow e^{x}=\\dfrac{1-\\sqrt{5}}{2} \\text { ou } e^{x}=\\dfrac{1+\\sqrt{5}}{2}
\\end{aligned}
$$

La première équation n'a pas de solution car $e^{x}>0$ tandis que $\\dfrac{1-\\sqrt{5}}{2}<0$. Pour la seconde, au contraire, $\\dfrac{1+\\sqrt{5}}{2}>0$, donc

$$
e^{x}=\\dfrac{1+\\sqrt{5}}{2} \\Leftrightarrow x=\\ln \\left(\\dfrac{1+\\sqrt{5}}{2}\\right) .
$$

Ainsi $S=\\left\\{\\ln \\left(\\dfrac{1+\\sqrt{5}}{2}\\right)\\right\\}$

17. On résout pour $x \\in \\mathbb{R} \\backslash\\{-2,1\\}$ (c'est-à-dire que $x$ est un réel différent de -2 et 1 , pour que les deux fractions soient bien définies).

$$
\\begin{aligned}
\\dfrac{x+3}{x+2}=\\dfrac{x-2}{x-1} & \\Leftrightarrow(x+3)(x-1)=(x+2)(x-2) \\quad(\\text { car } x+2 \\neq 0 \\text { et } x-1 \\neq 0) \\\\
& \\Leftrightarrow 2 x+1=0 \\quad \\text { (on développe et on passe tout à gauche) } \\\\
& \\Leftrightarrow x=-\\dfrac{1}{2} .
\\end{aligned}
$$

Ainsi $S=\\left\\{-\\dfrac{1}{2}\\right\\}$

## III.3 Manipulation d'inégalités

Opérations autorisées :

- Sommer des inégalités :

$$
\\left\\{\\begin{array}{l}
x \\leqslant y \\\\
x^{\\prime} \\leqslant y^{\\prime}
\\end{array} \\quad \\Longrightarrow \\quad x+x^{\\prime} \\leqslant y+y^{\\prime} .\\right.
$$

- Multiplier des inégalités terme à terme, si tous les termes sont positifs

$$
\\left\\{\\begin{array}{l}
0 \\leqslant x \\leqslant y \\\\
0 \\leqslant x^{\\prime} \\leqslant y^{\\prime}
\\end{array} \\quad \\Longrightarrow \\quad 0 \\leqslant x x^{\\prime} \\leqslant y y^{\\prime}\\right.
$$

- Passer à l'inverse, si tous les termes sont de même signe. Cette opération inverse l'ordre :

$$
\\begin{aligned}
& 0<x \\leqslant y \\quad \\Longrightarrow \\quad 0<\\dfrac{1}{y} \\leqslant \\dfrac{1}{x} \\\\
& x \\leqslant y<0 \\quad \\Longrightarrow \\quad \\dfrac{1}{y} \\leqslant \\dfrac{1}{x}<0 .
\\end{aligned}
$$

- (Cas particulier du point précédent) Multiplier une inégalité par un réel :

$$
\\begin{aligned}
& \\text { Si } a \\geqslant 0, \\quad x \\leqslant y \\Longrightarrow a x \\leqslant a y \\\\
& \\text { Si } a \\leqslant 0, \\quad x \\leqslant y \\Longrightarrow a x \\geqslant a y .
\\end{aligned}
$$

- Les fonctions racine carrée, exponentielle et logarithme sont strictement croissantes sur leurs ensembles de définition respectifs; elles préservent donc les inégalités :

Pour tous $x, y \\in \\mathbb{R}, \\quad e^{x}>e^{y} \\Leftrightarrow x>y$

Pour tous $x, y>0, \\quad \\ln (x)>\\ln (y) \\Leftrightarrow x>y$

Pour tous $x, y \\geqslant 0, \\quad \\sqrt{x}>\\sqrt{y} \\Leftrightarrow x>y$.

- $|x| \\leqslant a \\Leftrightarrow-a \\leqslant x \\leqslant a$
- Si $x \\geqslant 1$, les puissances successives de $x$ sont rangées par ordre croissant :

$$
x \\leqslant x^{2} \\leqslant x^{3} \\leqslant
$$

- Si $x \\in[0,1]$, les puissances successives de $x$ sont rangées par ordre décroissant :

$$
x \\geqslant x^{2} \\geqslant x^{3} \\geqslant
$$

Opérations interdites : on ne peut jamais soustraire ou diviser terme à terme des inégalités.

## III.4 Résolutions d'inéquations

Stratégie générale :

1) On passe tout d'un même côté de l'inégalité.
2) Si des fractions sont présentes, on regroupe tout en une seule fraction en mettant tous les termes au même dénominateur.
3) On factorise au maximum l'expression obtenue.
4) On étudie le signe de chacun des facteurs présents. On peut parfois avoir besoin de faire un changement de variable pour se ramener à des expressions plus simples (typiquement, des polynômes de degré 2 , dont on sait étudier le signe).
5) On conclut en faisant un tableau de signe.

En parallèle des points précédents, on peut aussi parfois simplifier les inégalités rencontrées en leur appliquant une fonction bien choisie (typiquement l'exponentielle, le logarithme ou la racine carrée, qui sont croissantes donc préservent les inégalités).

Exercice 6 Résoudre les inéquations suivantes.

Dans cet exercice, les points 1) à 3) de la stratégie exposée ci-dessus ont déjà été appliqués. Il vous reste à mener l'étude de signe finale (points 4) et 5)).

1. $2 x^{2}+3 x-2>0$
2. $x-2 \\sqrt{x}+1>0$
3. $x^{2}-x-1 \\leqslant 0$
4. $6 e^{2 x}-5 e^{x}+1 \\leqslant 0$
5. $x^{2}+x+1>0$
6. $e^{x}+2 e^{-x}>3$
7. $x^{2}-6 x+9 \\leqslant 0$
8. $(3 x-2)(2 x-1)>0$
9. $4 x^{2}+4 x+1<0$
10. $\\left(e^{x}-2\\right)(2 x-3) \\leqslant 0$
11. $x^{2}+x \\sqrt{2}+1<0$
12. $(1+\\ln x)(2-x) \\geqslant 0$

Correction de l'exercice 6

1. $\\mathcal{S}=]-\\infty,-2[\\cup] \\dfrac{1}{2},+\\infty[$
2. $\\mathcal{S}=\\left[\\dfrac{1-\\sqrt{5}}{2}, \\dfrac{1+\\sqrt{5}}{2}\\right]$
3. $\\mathcal{S}=\\mathbb{R}$
4. $\\mathcal{S}=\\{3\\}$
5. $\\mathcal{S}=\\emptyset$
6. $\\mathcal{S}=\\emptyset$
7. On résout pour $x \\in\\left[0,+\\infty\\left[\\right.\\right.$. On pose $X=\\sqrt{x}$ (donc $\\left.x=X^{2}\\right)$. Alors

$$
x-2 \\sqrt{x}+1>0 \\Leftrightarrow X^{2}-2 X+1>0 \\Leftrightarrow(X-1)^{2}>0
$$

(plus simple que de passer par un calcul de discriminant!)

Cette dernière inéquation équivaut à $X \\neq 1$ (car un carré est toujours positif), ce qui équivaut à $x \\neq 1$ car $x=X^{2}$.

Ainsi $\\mathcal{S}=[0,1[\\cup] 1,+\\infty[$. 8. On résout pour $x \\in \\mathbb{R}$. Posons $X=e^{x}$ (donc $x=\\ln (X)$ ). Alors

$$
6 e^{2 x}-5 e^{x}+1 \\leqslant 0 \\Leftrightarrow 6 X^{2}-5 X+1 \\leqslant 0
$$

Or, après calculs, $6 X^{2}-5 X+1$ a pour racines $\\dfrac{1}{3}$ et $\\dfrac{1}{2}$. Puisque le coefficient dominant (6) est positif, ce polynôme est négatif entre ses deux racines. D'où :

$$
\\begin{aligned}
6 X^{2}-5 X+1 \\leqslant 0 & \\Leftrightarrow \\dfrac{1}{3} \\leqslant X \\leqslant \\dfrac{1}{2} \\\\
& \\Leftrightarrow \\dfrac{1}{3} \\leqslant e^{x} \\leqslant \\dfrac{1}{2} \\\\
& \\Leftrightarrow \\ln \\left(\\dfrac{1}{3}\\right) \\leqslant \\ln \\left(e^{x}\\right) \\leqslant \\ln \\left(\\dfrac{1}{2}\\right) \\quad \\text { par stricte croissance de } \\ln \\\\
& \\Leftrightarrow-\\ln (3) \\leqslant x \\leqslant-\\ln (2)
\\end{aligned}
$$

Ainsi $\\mathcal{S}=[-\\ln (3),-\\ln (2)]$

| $x$ | $-\\infty$ | $\\ln (2)$ |  | $\\dfrac{3}{2}$ | $+\\infty$ |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Signe de $e^{x}-2$ | - | 0ं | + |  | + |
| Signe de $2 x-3$ | - |  | - | 0 | + |
| Signe de $\\left(e^{x}-2\\right)(2 x-3)$ | + | 0 | - | 0 | + |

9. En raisonnant comme à la question précédente, on trouve $]-\\infty, 0[\\cup] \\ln (2),+\\infty[$
10. En faisant un tableau de signe, on trouve $\\mathcal{S}=]-\\infty, \\dfrac{1}{2}[\\cup] \\dfrac{2}{3},+\\infty[$
11. On résout pour $x \\in \\mathbb{R}$. Par stricte croissance de $\\ln$ :

$$
e^{x}-2>0 \\Leftrightarrow e^{x}>2 \\Leftrightarrow \\ln \\left(e^{x}\\right)>\\ln (2) \\Leftrightarrow x>\\ln (2)
$$

Ainsi $\\mathcal{S}=\\left[\\ln (2), \\dfrac{3}{2}\\right]$

12. On résout pour $x \\in] 0,+\\infty[$. Par stricte croissance de exp, on a :

$$
1+\\ln x>0 \\Leftrightarrow \\ln x>-1 \\Leftrightarrow e^{\\ln x}>e^{-1} \\Leftrightarrow x>e^{-1} .
$$

En faisant un tableau de signe comme à la question précédente, on trouve $\\mathcal{S}=\\left[e^{-1}, 2\\right]$

## Exercice 7 Résoudre les inéquations suivantes :

1. $8 x+1 \\geqslant 2 x-5$
2. $\\dfrac{x^{2}+1}{x-1} \\geqslant 5$
3. $(5-2 x)^{2}>(5-2 x)(x-2)$
4. $\\dfrac{1}{x}<-2$
5. $x^{2} \\leqslant 4$
6. $\\dfrac{x+3}{x+2} \\geqslant \\dfrac{x-2}{x-1}$
7. $1-x^{4} \\geq 0$
8. $|\\ln (x)|<1$
9. $x-\\dfrac{1}{x} \\geqslant 0$
10. $\\left(\\dfrac{2}{3}\\right)^{n} \\leqslant 10^{-2}($ où $n \\in \\mathbb{N})$
11. $\\dfrac{1}{x-2} \\leqslant \\dfrac{1}{x}$

## Indications pour l'exercice 7

1. Facile.
2. Se ramener à " $\\ldots>0$ ", factoriser le terme de gauche et faire un tableau de signe.
3. Plusieurs approches possibles : on peut se ramener à l'étude du signe d'un polynôme de degré 2 , ou bien utiliser la croissance de la racine carrée, ou encore factoriser l'expression ...
4. Deux approches possibles ici :

- se ramener à " $x^{4} \\leqslant 1^{\\prime \\prime}$ et utiliser la croissance de la racine carrée;
- ou bien factoriser $1-x^{4}$ à l'aide d'une certaine identité remarquable.

5. Mettre au même dénominateur et faire un tableau de signe.
6. Se ramener à une équation du type ".. $\\leqslant 0 "$, mettre au même dénominateur et faire un tableau de signe.

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-13.jpg?height=48&width=1279&top_left_y=1311&top_left_x=1494)
faire un tableau de signe.

8. Se ramener à une équation du type "..<0", mettre au même dénominateur et faire un tableau de signe.
9. Se ramener à une équation du type " $\\ldots \\geqslant 0 "$, mettre au même dénominateur et faire un tableau de signe.
10. $|\\ln (x)|<1 \\Leftrightarrow-1<\\ln (x)<1$.
11. Utiliser la fonction ln et ses différentes propriétés. Attention à la division par un nombre négatif!

## Correction de l'exercice 7

1. On résout pour $x \\in \\mathbb{R}$.

$8 x+1 \\geqslant 2 x-5 \\Leftrightarrow 6 x \\geqslant-6 \\Leftrightarrow x \\geqslant-1$, donc $S=[-1,+\\infty[$. 2. On résout pour $x \\in \\mathbb{R}$.

Attention : une grosse (et fréquente) erreur consisterait ici à commencer par diviser l'équation par $5-2 x$. Cela serait incorrect car on ne connaît pas le signe de $5-2 x$ : si $5-2 x<0$, alors diviser par cette quantité aurait pour effet de changer le sens de l'inégalité. On pourrait certes s'en sortir en distinguant deux cas $(x<5 / 2$ et $x>5 / 2)$, mais cela conduirait à résoudre deux inéquations au lieu d'une ... Le plus simple est donc de suivre la stratégie présentée en début de section : on passe tout à gauche, on factorise, et on étudie le signe à l'aide d'un tableau de signe.

$$
\\begin{aligned}
(5-2 x)^{2}>(5-2 x)(x-2) & \\Leftrightarrow(5-2 x)^{2}-(5-2 x)(x-2)>0 \\\\
& \\Leftrightarrow(5-2 x)(5-2 x-(x-2))>0 \\\\
& \\Leftrightarrow(5-2 x)(7-3 x)>0 .
\\end{aligned}
$$

En faisant un tableau de signe, on trouve $S=]-\\infty, \\dfrac{7}{3}[\\cup] \\dfrac{5}{2},+\\infty[$.

3. On résout pour $x \\in \\mathbb{R}$

Première méthode possible : La racine carrée étant croissante sur $[0,+\\infty[$,

$$
x^{2} \\leqslant 4 \\Leftrightarrow \\sqrt{x^{2}} \\leqslant \\sqrt{4} \\Leftrightarrow|x| \\leqslant 2
$$

Ainsi $S=[-2,2]$

Seconde méthode possible : $x^{2} \\leqslant 4 \\Leftrightarrow x^{2}-4 \\leqslant 0$. Or $x^{2}-4$ est un polynôme de degré 2 ayant 2 et -2 comme racines; il est négatif entre ses deux racines, donc $S=[-2,2]$

4. On résout pour $x \\in \\mathbb{R}$. Factorisons $: 1-x^{4}=\\left(1-x^{2}\\right)\\left(1+x^{2}\\right)$.

Pour tout $x \\in \\mathbb{R}, 1+x^{2}>0$, donc $1-x^{4}$ est du signe de $1-x^{2}$. Or $1-x^{2}$ est un polynôme de degré 2 ayant 1 et -1 comme racines; puisque son coefficient dominant $(-1)$ est négatif, ce polynôme est positif entre ses deux racines. Ainsi $S=[-1,1]$

| $x$ | $-\\infty$ | -1 |  | 0 |  | 1 | $+\\infty$ |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Signe de $x^{2}-1$ | + | $\\ddot{9}$ | - |  | - | 0 | + |
| Signe de $x$ | - |  | - | $\\stackrel{\\theta}{\\theta}$ | + |  | + |
| Signe de $\\dfrac{x^{2}-1}{x}$ | - | فं | + |  | - | 0 | + |

5. On résout pour $x \\neq 0$.

On met tout au même dénominateur et on fait un tableau de signe :

$$
x-\\dfrac{1}{x} \\geqslant 0 \\Leftrightarrow \\dfrac{x^{2}-1}{x} \\geqslant 0
$$

Ainsi $S=[-1,0[\\cup[1,+\\infty[$. 6. On résout pour $x \\neq 0$ et $x \\neq 2$ (pour que les dénominateurs ne s'annulent pas). On met tout au même dénominateur et on fait un tableau de signe $\\dfrac{1}{x-2} \\leqslant \\dfrac{1}{x} \\Leftrightarrow \\dfrac{1}{x-2}-\\dfrac{1}{x} \\leqslant 0 \\Leftrightarrow \\dfrac{x}{x(x-2)}-\\dfrac{x-2}{x(x-2)} \\leqslant 0 \\Leftrightarrow \\dfrac{2}{x(x-2)} \\leqslant 0$. À l'aide d'un nouveau tableau de signe, on en déduit que $S=] 0,2[$.

7. On résout pour $x \\neq 1$ (pour que le dénominateur ne s'annule pas). On met tout au même dénominateur et on fait un tableau de signe : $\\dfrac{x^{2}+1}{x-1} \\geqslant 5 \\Leftrightarrow \\dfrac{x^{2}+1}{x-1}-5 \\geqslant 0 \\Leftrightarrow \\dfrac{x^{2}+1-5(x-1)}{x-1} \\geqslant 0 \\Leftrightarrow \\dfrac{x^{2}-5 x+6}{x-1} \\geqslant 0$. Le polynôme $x^{2}-5 x+6$ a pour racines 2 et 3 ; il est négatif sur [2,3] et négatif hors de cet intervalle. Ainsi, en faisant un tableau de signe, on en déduit que $S=] 1,2] \\cup[3,+\\infty[$.
8. On résout pour $x \\neq 0$. On met tout au même dénominateur :

$$
\\dfrac{1}{x}<-2 \\Leftrightarrow \\dfrac{1}{x}+2<0 \\Leftrightarrow \\dfrac{1+2 x}{x}<0
$$

À l'aide d'un nouveau tableau de signe, on en déduit que $S=]-\\dfrac{1}{2}, 0[$.

9. On résout pour $x \\neq-2$ et $x \\neq 1$ (pour que les dénominateurs ne s'annulent pas). On met tout au même dénominateur et on fait un tableau de signe :

$$
\\begin{aligned}
\\dfrac{x+3}{x+2} \\geqslant \\dfrac{x-2}{x-1} \\Leftrightarrow \\dfrac{x+3}{x+2}-\\dfrac{x-2}{x-1} \\geqslant 0 & \\Leftrightarrow \\dfrac{(x+3)(x-1)-(x-2)(x+2)}{(x+2)(x-1)} \\geqslant 0 \\\\
& \\Leftrightarrow \\dfrac{2 x+1}{(x+2)(x-1)} \\geqslant 0 .
\\end{aligned}
$$

À l'aide d'un nouveau tableau de signe, on en déduit que $\\left.\\left.S=]-2,-\\dfrac{1}{2}\\right] \\cup\\right] 1,+\\infty[$.

10. On résout pour $x \\in] 0,+\\infty[$

En utilisant le fait que exp est strictement croissante sur $\\mathbb{R}$ :

$$
|\\ln (x)|<1 \\Leftrightarrow-1<\\ln (x)<1 \\Leftrightarrow e^{-1}<e^{\\ln (x)}<e^{1} \\Leftrightarrow e^{-1}<x<e .
$$

Ainsi $S=] e^{-1}, e[$

11. On résout pour $n \\in \\mathbb{N}$.

En utilisant le fait que ln est strictement croissante sur $] 0,+\\infty[$

$$
\\left(\\dfrac{2}{3}\\right)^{n} \\leqslant 10^{-2} \\Leftrightarrow \\ln \\left(\\left(\\dfrac{2}{3}\\right)^{n}\\right) \\leqslant \\ln \\left(10^{-2}\\right) \\Leftrightarrow n \\ln \\left(\\dfrac{2}{3}\\right) \\leqslant-2 \\ln (10) \\Leftrightarrow n \\geqslant-\\dfrac{2 \\ln (10)}{\\ln \\left(\\dfrac{2}{3}\\right)} .
$$

Attention au fait que $\\ln \\left(\\dfrac{2}{3}\\right)<0\\left(\\operatorname{car} \\dfrac{2}{3}<1\\right)$ : le fait de diviser par cette quantité change donc le sens de l'inégalité!

## Limites de suites et de fonctions

Notation : Pour dire que $f(x)$ tend vers $\\ell$ quand $x$ tend vers $a$, on écrit :

$$
f(x) \\underset{x \\rightarrow a}{\\longrightarrow} \\ell \\quad \\text { ou encore } \\quad \\lim _{x \\rightarrow a} f(x)=\\ell
$$

## IV.1 Fonctions usuelles

Racine carrée

Soit $f(x)=\\sqrt{x}$

- définie sur $[0,+\\infty$
- dérivable sur $] 0,+\\infty[$
- tangente verticale en 0
- $f^{\\prime}(x)=\\dfrac{1}{2 \\sqrt{x}}$

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-15.jpg?height=377&width=608&top_left_y=509&top_left_x=740)

Exponentielle

- définie et dérivable sur $\\mathbb{R}$
- $\\exp ^{\\prime}(x)=\\exp (x)$.
- $e^{x} \\underset{x \\rightarrow-\\infty}{\\longrightarrow} 0$
- $e^{x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty$
- $e^{0}=1$.

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-15.jpg?height=455&width=784&top_left_y=996&top_left_x=656)

Logarithme népérien

- définie et dérivable sur $] 0,+\\infty[$
- $\\ln ^{\\prime}(x)=\\dfrac{1}{x}$.
- $\\ln (x) \\underset{x \\rightarrow 0^{+}}{\\longrightarrow}-\\infty$
- $\\ln (x) \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty$
- $\\ln (1)=0$.


## Cosinus

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-15.jpg?height=321&width=1329&top_left_y=142&top_left_x=1506)

- définie et dérivable sur $\\mathbb{R}$
- $\\cos ^{\\prime}(x)=-\\sin (x)$
- Pas de limite en $\\pm \\infty$.

Sinus

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-15.jpg?height=332&width=1346&top_left_y=766&top_left_x=1490)

- définie et dérivable sur $\\mathbb{R}$
- $\\sin ^{\\prime}(x)=\\cos (x)$
- Pas de limite en $\\pm \\infty$.


## IV.2 Limites usuelles

## Croissances comparées

Les croissances comparées expriment essentiellement que, en cas d'indétermination dans un produit ou un quotient, les termes en $e^{x}$ "l'emportent" sur les termes polynomiaux (en $x^{n}$ ), c'est-à-dire que c'est l'exponentielle qui va dicter la limite finale.

Pour tout entier $n \\in \\mathbb{N}$

$$
\\dfrac{e^{x}}{x^{n}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty \\quad \\text { et } x^{n} e^{x} \\underset{x \\rightarrow-\\infty}{\\longrightarrow} 0 \\text {. }
$$

De même, les termes polynomiaux l'emportent sur les termes en $\\ln (x)$ :

Pour tout entier $n \\geqslant 1$

$$
\\dfrac{\\ln (x)}{x^{n}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0 \\text { et } x^{n} \\ln (x) \\underset{x \\rightarrow 0^{+}}{\\longrightarrow} 0 \\text {. }
$$

Limites obtenues à l'aide de taux d'accroissements

Si $f$ est une fonction dérivable en $a$, on sait que par définition

$$
\\dfrac{f(x)-f(a)}{x-a} \\underset{x \\rightarrow a}{\\longrightarrow} f^{\\prime}(a)
$$

En appliquant cette formule avec $a=0$ et $f(x)=e^{x}$, on trouve (puisque $f^{\\prime}(x)=e^{x}$ ) :

$$
\\dfrac{e^{x}-e^{0}}{x-0} \\underset{x \\rightarrow 0}{\\longrightarrow} e^{0} \\quad \\text { c'est-à-dire } \\quad \\dfrac{e^{x}-1}{x} \\underset{x \\rightarrow 0}{\\longrightarrow} 1
$$

De même, en appliquant cette formule avec $f(x)=\\sin (x)$ ou $f(x)=\\ln (1+x)$, on trouve les limites suivantes

$$
\\dfrac{\\sin (x)}{x} \\underset{x \\rightarrow 0}{\\longrightarrow} 1 \\text { et } \\dfrac{\\ln (1+x)}{x} \\underset{x \\rightarrow 0}{\\longrightarrow} 1
$$

(Vérifiez que c'est bien le cas, en faisant vous-mêmes le détail du calcul!)

## Limites de suites usuelles

- Si $\\alpha$ est un entier strictement positif, alors $n^{\\alpha} \\underset{n \\rightarrow+\\infty}{\\longrightarrow}+\\infty$.
- Si $\\alpha$ est un entier strictement négatif, alors $n^{\\alpha} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 0$.
- Si $q \\in] 1,+\\infty\\left[\\right.$, alors $q^{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow}+\\infty$.
- Si $q \\in]-1,1\\left[\\right.$, alors $q^{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 0$.


## IV.3 Opérations sur les limites

Soient $f$ et $g$ deux fonctions admettant des limites (finies ou infinies) en un point $a$ ( $a$ peut être un réel, ou bien $+\\infty$ ou $-\\infty$ ). Soient également deux réels $\\ell$ et $\\ell^{\\prime}$.

## Limite d'une somme

| Si $\\boldsymbol{f}$ a pour limite en $\\boldsymbol{a}$ | $\\ell$ | $\\ell$ | $\\ell$ | $+\\infty$ | $-\\infty$ | $+\\infty$ |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| et si $\\boldsymbol{g}$ a pour limite en $\\boldsymbol{a}$ | $\\ell^{\\prime}$ | $+\\infty$ | $-\\infty$ | $+\\infty$ | $-\\infty$ | $-\\infty$ |
| alors $\\boldsymbol{f}+\\boldsymbol{g}$ a pour limite en $\\boldsymbol{a}$ | $\\ell+\\ell^{\\prime}$ | $+\\infty$ | $-\\infty$ | $+\\infty$ | $-\\infty$ | F.I. |

Limite d'un produit

| Si $\\boldsymbol{f}$ a pour limite en $\\boldsymbol{a}$ | $\\ell$ | $\\ell>0$ | $\\ell>0$ | $\\ell<0$ | $\\ell<0$ | $+\\infty$ | $+\\infty$ | $-\\infty$ | 0 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| et si $\\boldsymbol{g}$ a pour limite en $\\boldsymbol{a}$ | $\\ell^{\\prime}$ | $+\\infty$ | $-\\infty$ | $+\\infty$ | $-\\infty$ | $+\\infty$ | $-\\infty$ | $-\\infty$ | $\\pm \\infty$ |
| alors $\\boldsymbol{f g}$ a pour limite en $\\boldsymbol{a}$ | $\\ell \\ell^{\\prime}$ | $+\\infty$ | $-\\infty$ | $-\\infty$ | $+\\infty$ | $+\\infty$ | $-\\infty$ | $+\\infty$ | F.I. |

Limite d'un quotient

| Si $\\boldsymbol{f}$ a pour limite en $\\boldsymbol{a}$ | $\\ell$ | $\\ell$ | $+\\infty$ | $+\\infty$ | $-\\infty$ | $-\\infty$ | $\\pm \\infty$ |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| et si $\\boldsymbol{g}$ a pour limite en $\\boldsymbol{a}$ | $\\ell^{\\prime} \\neq 0$ | $\\pm \\infty$ | $\\ell^{\\prime}>0$ | $\\ell^{\\prime}<0$ | $\\ell^{\\prime}>0$ | $\\ell^{\\prime}<0$ | $\\pm \\infty$ |
| alors $\\dfrac{f}{g}$ a pour limite en $\\boldsymbol{a}$ | $\\dfrac{\\ell}{\\ell^{\\prime}}$ | 0 | $+\\infty$ | $-\\infty$ | $-\\infty$ | $+\\infty$ | F.I. |

Si le dénominateur tend vers 0 en $a$, il faut tenir compte de son signe au voisinage de $a$ :

| Si $\\boldsymbol{f}$ a pour limite | $\\ell>0$ ou $+\\infty$ | $\\ell<0$ ou $-\\infty$ | $\\ell>0$ ou $+\\infty$ | $\\ell<0$ ou $-\\infty$ | 0 |
| :---: | :---: | :---: | :---: | :---: | :---: |
| et si $\\boldsymbol{g}$ a pour limite | 0 avec $g(x)>0$ si $x$ proche de $a$ | 0 avec $g(x)<0$ si $x$ proche de $a$ | 0 |  |  |
| alors $\\dfrac{f}{g}$ a pour limite | $+\\infty$ | $-\\infty$ | $-\\infty$ | $+\\infty$ | F.I. |

Dans les tableaux ci-dessus, F.I. désigne une forme indéterminée; il y a 4 types de formes indéterminées à savoir repérer :

$$
\\infty-\\infty, \\quad 0 \\times \\infty, \\quad \\dfrac{\\infty}{\\infty}, \\quad \\dfrac{0}{0}
$$

Face à une forme indéterminée, les opérations usuelles ci-dessus ne suffisent pas à conclure; il faut alors recourir des techniques plus avancées. On peut par exemple :

- Face à un F.I. du type $\\infty-\\infty$, mettre en facteur le terme dominant (le terme le "plus gros").
- Face à une F.I. du type $\\dfrac{\\infty}{\\infty}$, mettre en facteur les termes dominants du numérateur et du dénominateur, et simplifier l'expression obtenue.
- Face à une F.I. du type $\\dfrac{0}{0}$, factoriser numérateur et dénominateur pour faire apparaître des facteurs tendant vers 0 , et simplifier ces facteurs entre eux.
- Se ramener par factorisation à une limite usuelle (croissances comparées ou taux d'accroissement).
- Utiliser le théorème de comparaison ou le théorème des gendarmes. Pour cela, il faut pouvoir encadrer, majorer ou minorer l'expression étudiée; on procède donc ainsi surtout lorsque l'on est face à des cosinus ou sinus (on exploite alors le fait que, pour tout $x \\in \\mathbb{R},-1 \\leqslant \\cos (x) \\leqslant 1$ et $-1 \\leqslant \\sin (x) \\leqslant 1)$.


## Composition de limites

Le théorème de composition de limites n'est pas présent dans les programmes officiels de terminale, mais est souvent implicitement utilisé. Il s'énonce ainsi :

$$
\\text { Si }\\left\\{\\begin{array}{l}
u(x) \\underset{x \\rightarrow a}{\\longrightarrow} b \\\\
f(y) \\underset{y \\rightarrow b}{\\longrightarrow} \\ell
\\end{array}, \\text { alors } f(u(x)) \\underset{x \\rightarrow a}{\\longrightarrow} \\ell .\\right.
$$

Exemple : déterminer $\\lim _{x \\rightarrow 1^{-}} \\exp \\left(\\dfrac{1}{x-1}\\right)$. On a :

- Quand $x$ tend vers 1 par la gauche, $x-1$ tend vers 0 par valeurs négatives; par conséquent $\\dfrac{1}{x-1} \\underset{x \\rightarrow 1^{-}}{\\longrightarrow}-\\infty$
- $\\exp (y) \\underset{y \\rightarrow-\\infty}{\\longrightarrow} 0$.

Par composition, on a donc $\\exp \\left(\\dfrac{1}{x-1}\\right) \\underset{x \\rightarrow 1^{-}}{\\longrightarrow} 0$.

Exemple 2 : la règle de composition de limites fonctionne aussi avec les suites. Par exemple, déterminons $\\lim _{n \\rightarrow+\\infty} \\cos \\left(\\dfrac{1}{n}\\right)$. On a :

- $\\dfrac{1}{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 0$
- $\\cos (y) \\underset{y \\rightarrow 0}{\\longrightarrow} \\cos (0)=1$

donc par composition, $\\cos \\left(\\dfrac{1}{n}\\right) \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 1$.

## Méthode de l'expression conjuguée

Face à une indétermination du type $\\infty-\\infty$, la technique de la factorisation par le terme dominant ne fonctionne pas toujours ... car parfois, il n'y a pas de terme dominant!

Si l'expression à étudier contient une ou plusieurs racines carrées, il est alors fréquent d'utiliser la méthode dite de l'expression conjuguée.

Exemple : déterminer $\\lim _{n \\rightarrow+\\infty} \\sqrt{n+1}-\\sqrt{n}$

Ici les deux termes en présence dont du même "ordre de grandeur" quand $n$ tend vers $+\\infty$ : aucun ne domine l'autre. Si l'on essaye de factoriser par l'un des deux, mettons $\\sqrt{n}$ :

$$
\\sqrt{n+1}-\\sqrt{n}=\\sqrt{n}\\left(\\sqrt{1+\\dfrac{1}{n}}-1\\right)
$$

cela ne nous avance guère car on obtient une nouvelle F.I. du type $+\\infty \\times 0$.

Pour lever cette indétermination, on va multiplier l'expression initiale, en haut et en bas, par sa quantité conjuguée $1^{1} \\sqrt{n+1}+\\sqrt{n}$ :

$$
\\sqrt{n+1}-\\sqrt{n}=\\dfrac{(\\sqrt{n+1}-\\sqrt{n})(\\sqrt{n+1}+\\sqrt{n})}{\\sqrt{n+1}+\\sqrt{n}}
$$

On utilise alors l'identité $(a-b)(a+b)=a^{2}-b^{2}$ pour faire disparaître les racines au numérateur :

$$
\\sqrt{n+1}-\\sqrt{n}=\\dfrac{(\\sqrt{n+1})^{2}-(\\sqrt{n})^{2}}{\\sqrt{n+1}+\\sqrt{n}}=\\dfrac{n+1-n}{\\sqrt{n+1}+\\sqrt{n}}=\\dfrac{1}{\\sqrt{n+1}+\\sqrt{n}} .
$$

L'indétermination est levée : cette quantité tend vers 0 quand $n$ tend vers $+\\infty$.

1. La quantité conjuguée d'une expression de la forme $a-b$ est l'expression $a+b$, et réciproquement la quantité conjuguée de $a+b$ est $a-b$.

## Correction de l'exercice 8

1. C'est une composition de limites usuelles :
2. $u_{n}=\\ln \\left(1+e^{-n}\\right)$
3. $u_{n}=\\dfrac{3 n^{2}-n+1}{2 n^{2}-1}$

- $1+e^{-n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 1$ car $e^{x} \\underset{x \\rightarrow-\\infty}{\\longrightarrow} 0$
- $\\ln (y) \\underset{y \\rightarrow 1}{\\longrightarrow} \\ln (1)=0$

donc par composition, $u_{n}=\\ln \\left(1+e^{-n}\\right) \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 0$.

$u_{n}=1-\\left(\\dfrac{2}{3}\\right)^{n}$

2. F.I. du type $\\dfrac{\\infty}{\\infty}$ : on met en facteur au numérateur et au dénominateur les termes dominants $\\left(\\right.$ ceux en $\\left.n^{2}\\right)$.

$$
\\dfrac{3 n^{2}-n+1}{2 n^{2}-1}=\\dfrac{3 n^{2}\\left(1-\\dfrac{1}{3 n}+\\dfrac{1}{3 n^{2}}\\right)}{2 n^{2}\\left(1-\\dfrac{1}{2 n^{2}}\\right)}=\\dfrac{3}{2} \\dfrac{1-\\dfrac{1}{3 n}+\\dfrac{1}{3 n^{2}}}{1-\\dfrac{1}{2 n^{2}}} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} \\dfrac{3}{2}
$$

5. $u_{n}=n \\ln \\left(1+\\dfrac{1}{n}\\right)$
6. $u_{n}=n-\\sqrt{n^{2}+1}$

Indications pour l'exercice 8

1. composition de limites.
2. F.I. $\\dfrac{\\infty}{\\infty}:$ comment la lever?
3. utiliser $\\lim _{n \\rightarrow+\\infty} q^{n}$.
4. F.I. $\\infty-\\infty$ : comment la lever ?
5. utiliser $\\lim _{x \\rightarrow 0} \\dfrac{\\ln (1+x)}{x}$.
6. méthode de l'expression conjuguée.
7. $\\left.\\dfrac{2}{3} \\in\\right]-1,1\\left[\\operatorname{donc}\\left(\\dfrac{2}{3}\\right)^{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 0\\right.$, et donc $u_{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 1$.
8. F.I. du type $\\infty-\\infty$ : on met en facteur le terme dominant, $3^{n}$.

$$
3^{n}-2^{n}=3^{n}\\left(1-\\left(\\dfrac{2}{3}\\right)^{n}\\right)
$$

Or $3^{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow}+\\infty$ car $3>1$, et $1-\\left(\\dfrac{2}{3}\\right)^{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 1$ d'après la question précédente ; par produit, $u_{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow}+\\infty$

5. On peut écrire $u_{n}=\\dfrac{\\ln \\left(1+\\dfrac{1}{n}\\right)}{\\dfrac{1}{n}}$.

Or $\\dfrac{1}{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 0$ et $\\dfrac{\\ln (1+y)}{y} \\underset{y \\rightarrow 0}{\\longrightarrow} 1$, donc par composition de limites, $u_{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 1$.

6. F.I. du type $\\infty-\\infty$, mais les deux termes en présence sont du même ordre de grandeur : en effet, intuitivement, pour $n$ "grand", on a $\\sqrt{n^{2}+1} \\approx \\sqrt{n^{2}}=n$. Il n'y a donc pas de terme dominant à mettre en facteur ...

Profitons de la présence de la racine carrée pour utiliser la méthode de l'expression conjuguée :

$u_{n}=\\dfrac{\\left(n-\\sqrt{n^{2}+1}\\right)\\left(n+\\sqrt{n^{2}+1}\\right)}{n+\\sqrt{n^{2}+1}}=\\dfrac{n^{2}-\\left(n^{2}+1\\right)}{n+\\sqrt{n^{2}+1}}=\\dfrac{-1}{n+\\sqrt{n^{2}+1}} \\underset{n \\rightarrow+\\infty}{\\rightarrow} 0$

## Exercice 9

Calculer les limites suivantes en utilisant la règle de composition de limites.

1. $\\lim _{x \\rightarrow 0} \\ln (\\cos x)$
2. $\\lim _{x \\rightarrow+\\infty} \\ln \\left(\\dfrac{2 x+1}{x-1}\\right)$
3. $\\lim _{x \\rightarrow 0} \\dfrac{\\ln (1+2 x)}{x}$
4. $\\lim _{x \\rightarrow+\\infty} \\sqrt{1+\\dfrac{1}{x^{2}}}$
5. $\\lim _{x \\rightarrow+\\infty} \\exp \\left(x^{3}-e^{x}\\right)$.
6. $\\lim _{x \\rightarrow+\\infty} x \\sin \\left(\\dfrac{1}{x}\\right)$
7. $\\lim _{x \\rightarrow 0} \\sqrt{1+\\dfrac{1}{x^{2}}}$
8. $\\lim _{x \\rightarrow-\\infty} \\dfrac{\\ln \\left(1+e^{x}\\right)}{e^{x}}$
9. $\\lim _{x \\rightarrow 0} \\dfrac{e^{3 x}-1}{x}$

## Indications pour l'exercice 9

1. $\\cos (0)=$ ?
2.     - 
3. quand $x$ tend vers $0, x^{2}$ tend vers 0 par valeurs positives, donc ...
4. commencer par lever l'indétermination à l'intérieur du ln.
5. commencer par lever l'indétermination à l'intérieur de l'exponentielle.
6. utiliser $\\lim _{x \\rightarrow 0} \\dfrac{\\ln (1+x)}{x}$
7. $\\lim _{x \\rightarrow 0} \\dfrac{\\ln (1+2 x)}{2 x}=$ ?
8. utiliser $\\lim _{x \\rightarrow 0} \\dfrac{\\sin x}{x}$
9. utiliser $\\lim _{x \\rightarrow 0} \\dfrac{e^{x}-1}{x}$

## Correction de l'exercice 9

1. $\\cos (x) \\underset{x \\rightarrow 0}{\\longrightarrow} \\cos (0)=1$ et $\\ln (y) \\underset{y \\rightarrow 1}{\\longrightarrow} \\ln (1)=0$;

par composition de limites, $\\ln (\\cos x) \\underset{x \\rightarrow 0}{\\longrightarrow} 0$.

2. $1+\\dfrac{1}{x^{2}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 1$ et $\\sqrt{y} \\underset{y \\rightarrow 1}{\\longrightarrow} \\sqrt{1}=1$ donc par composition, $\\sqrt{1+\\dfrac{1}{x^{2}}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 1$.
3. Quand $x$ tend vers $0, x^{2}$ tend vers $0 \\underline{\\text { par valeurs positives, }}$, donc $1+\\dfrac{1}{x^{2}} \\underset{x \\rightarrow 0}{\\longrightarrow}+\\infty$. De plus $\\sqrt{y} \\underset{y \\rightarrow+\\infty}{\\longrightarrow}+\\infty$, donc par composition, $\\sqrt{1+\\dfrac{1}{x^{2}}} \\underset{x \\rightarrow 0}{\\longrightarrow}+\\infty$.
4. $\\dfrac{2 x+1}{x-1}$ a même limite en $+\\infty$ que le quotient de ses monômes dominants (ce qui se montre en mettant les termes en $x$ en facteur au numérateur et au dénominateur).

$$
\\lim _{x \\rightarrow+\\infty} \\dfrac{2 x+1}{x-1}=\\lim _{x \\rightarrow+\\infty} \\dfrac{2 x}{x}=\\lim _{x \\rightarrow+\\infty} 2=2
$$

D'autre part, $\\ln (y) \\underset{y \\rightarrow 2}{\\longrightarrow} \\ln (2)$, donc par composition, $\\ln \\left(\\dfrac{2 x+1}{x-1}\\right) \\underset{x \\rightarrow+\\infty}{\\longrightarrow} \\ln (2)$. 5. À l'intérieur de l'exponentielle, on a une forme indéterminée du type $\\infty-\\infty$. On la lève en mettant le terme dominant en facteur :

$$
x^{3}-e^{x}=e^{x}\\left(\\dfrac{x^{3}}{e^{x}}-1\\right)
$$

Or $\\dfrac{e^{x}}{x^{3}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty\\left(\\right.$ croissances comparées), donc $\\dfrac{x^{3}}{e^{x}}=\\dfrac{1}{\\left(\\dfrac{e^{x}}{x^{3}}\\right)} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$

On en déduit que $x^{3}-e^{x}=e^{x}\\left(\\dfrac{x^{3}}{e^{x}}-1\\right) \\underset{x \\rightarrow+\\infty}{\\longrightarrow}-\\infty\\left(\\right.$ car $e^{x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty$, et car la parenthèse tend vers -1 d'après ce qui précède).

Or $\\exp (y) \\underset{y \\rightarrow-\\infty}{\\longrightarrow} 0$, donc par composition de limites, $\\exp \\left(x^{3}-e^{x}\\right) \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$.

6. $e^{x} \\underset{x \\rightarrow-\\infty}{\\longrightarrow} 0$ et $\\dfrac{\\ln (1+y)}{y} \\underset{y \\rightarrow 0}{\\longrightarrow} 1$ donc par composition, $\\dfrac{\\ln \\left(1+e^{x}\\right)}{e^{x}} \\underset{x \\rightarrow-\\infty}{\\longrightarrow} 1$.
7. Un peu plus délicat : pour faire apparaître la composée (et la limite usuelle), il faut introduire artificiellement un facteur "2" en écrivant

$$
\\dfrac{\\ln (1+2 x)}{x}=2 \\dfrac{\\ln (1+2 x)}{2 x}
$$

Or $2 x \\underset{x \\rightarrow 0}{\\longrightarrow} 0$ et $\\dfrac{\\ln (1+y)}{y} \\underset{y \\rightarrow 0}{\\longrightarrow} 1$ donc par composition, $\\dfrac{\\ln (1+2 x)}{2 x} \\underset{x \\rightarrow 0}{\\longrightarrow} 1$.

$\\operatorname{Ainsi} \\dfrac{\\ln (1+2 x)}{x} \\underset{x \\rightarrow 0}{\\longrightarrow} 2$

8. Pour $x>0$, on a $x \\sin \\left(\\dfrac{1}{x}\\right)=\\dfrac{\\sin \\left(\\dfrac{1}{x}\\right)}{\\dfrac{1}{x}}$.

Or $\\dfrac{1}{x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$ et $\\dfrac{\\sin (y)}{y} \\underset{y \\rightarrow 0}{\\longrightarrow} 1$, donc par composition $\\dfrac{\\sin \\left(\\dfrac{1}{x}\\right)}{\\dfrac{1}{x}} \\underset{x \\rightarrow 0}{\\longrightarrow} 1$.

9. On introduit artificiellement un facteur " 3 " pour faire apparaître la limite usuelle :

$$
\\dfrac{e^{3 x}-1}{x}=3 \\dfrac{e^{3 x}-1}{3 x}
$$

Or $3 x \\underset{x \\rightarrow 0}{\\longrightarrow} 0$ et $\\dfrac{e^{y}-1}{y} \\underset{y \\rightarrow 0}{\\longrightarrow} 1$ donc par composition, $\\dfrac{e^{3 x}-1}{3 x} \\underset{x \\rightarrow 0}{\\longrightarrow} 1$.

$\\operatorname{Ainsi} \\dfrac{e^{3 x}-1}{x} \\underset{x \\rightarrow 0}{\\longrightarrow} 3$.

1. $\\lim _{x \\rightarrow 0} \\dfrac{\\ln (x)}{x}$
2. $\\lim _{x \\rightarrow-\\infty} \\dfrac{x}{\\sqrt{x^{2}+1}}$
3. $\\lim _{x \\rightarrow+\\infty} \\dfrac{e^{x}-e^{-x}}{e^{x}+e^{-x}}$
4. $\\lim _{x \\rightarrow 0} \\dfrac{x \\ln x}{x^{2}-1}$
5. $\\lim _{x \\rightarrow 0} \\dfrac{\\sqrt{1+x}-1}{x}$
6. $\\lim _{x \\rightarrow-\\infty} \\dfrac{e^{x}-e^{-x}}{e^{x}+e^{-x}}$
7. $\\lim _{x \\rightarrow-\\infty} 3 e^{2 x}-4 e^{x}$
8. $\\lim _{x \\rightarrow+\\infty} \\sqrt{x^{2}+3 x}-x$
9. $\\lim _{x \\rightarrow+\\infty} \\dfrac{\\sqrt{x^{2}-1}+3 x}{\\sqrt{x^{2}+1}}$
10. $\\lim _{x \\rightarrow+\\infty} 3 e^{2 x}-4 e^{x}$
11. $\\lim _{x \\rightarrow+\\infty} \\dfrac{\\sin x}{x}$
12. $\\lim _{x \\rightarrow+\\infty} \\dfrac{\\ln (x+1)}{\\ln x}$
13. $\\lim _{x \\rightarrow+\\infty} \\dfrac{4 x^{2}-5 \\ln x+x^{3}}{x^{4}-e^{x}+3 x}$

Correction de l'exercice 10

1. $\\ln x \\underset{x \\rightarrow 0^{+}}{\\longrightarrow}-\\infty$ donc par quotient, $\\dfrac{\\ln x}{x} \\underset{x \\rightarrow 0^{+}}{\\longrightarrow}-\\infty$.
2. Par croissances comparées, $x \\ln x \\underset{x \\rightarrow 0}{\\longrightarrow} 0$, et d'autre part $x^{2}-1 \\underset{x \\rightarrow 0}{\\longrightarrow}-1$, donc $\\dfrac{x \\ln x}{x^{2}-1} \\underset{x \\rightarrow 0}{\\longrightarrow} 0$
3. $2 x \\underset{x \\rightarrow-\\infty}{\\longrightarrow}-\\infty$ et $e^{y} \\underset{y \\rightarrow-\\infty}{\\longrightarrow} 0$ donc par composition de limites, $e^{2 x} \\underset{x \\rightarrow-\\infty}{\\longrightarrow} 0$. Ainsi $3 e^{2 x}-4 e^{x} \\underset{x \\rightarrow-\\infty}{\\longrightarrow} 0-0=0$.
4. F.I. du type $\\infty-\\infty$ : on met en facteur le terme dominant $3 e^{2 x}$.

$$
3 e^{2 x}-4 e^{x}=3 e^{2 x}\\left(1-\\dfrac{4}{3 e^{x}}\\right)
$$

Or $e^{x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty$. On en déduit d'une part que $e^{2 x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty$ (par composition, car $2 x \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty)$, et d'autre part que $1-\\dfrac{4}{3 e^{x}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 1$.

Ainsi par produit : $3 e^{2 x}-4 e^{x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty$.

5. F.I. du type $\\dfrac{\\infty}{\\infty}$ : on met en facteur au numérateur et au dénominateur les termes dominants. Ici on veut la limite en $+\\infty$, on va donc supposer que $x>0$; cela permet d'écrire que $\\sqrt{x^{2}}=|x|=x$.

$$
\\dfrac{x}{\\sqrt{x^{2}+1}}=\\dfrac{x}{\\sqrt{x^{2}\\left(1+\\dfrac{1}{x^{2}}\\right)}}=\\dfrac{x}{\\sqrt{x^{2}} \\sqrt{1+\\dfrac{1}{x^{2}}}}=\\dfrac{x}{x \\sqrt{1+\\dfrac{1}{x^{2}}}}=\\dfrac{1}{\\sqrt{1+\\dfrac{1}{x^{2}}}}
$$

Or $\\sqrt{1+\\dfrac{1}{x^{2}}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 1$ (limite de composée, déjà détaillée dans une question d'un autre exercice). On en déduit que $\\dfrac{x}{\\sqrt{x^{2}+1}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} \\dfrac{1}{1}=1$.

6. F.I. du type $\\dfrac{\\infty}{\\infty}$ : on met en facteur au numérateur et au dénominateur les termes dominants. Ici on veut la limite en $-\\infty$, on va donc supposer que $x<0$; cela permet d'écrire que $\\sqrt{x^{2}}=|x|=-x$ (attention au signe!).

$$
\\dfrac{x}{\\sqrt{x^{2}+1}}=\\dfrac{x}{\\sqrt{x^{2}\\left(1+\\dfrac{1}{x^{2}}\\right)}}==\\dfrac{x}{\\sqrt{x^{2}} \\sqrt{1+\\dfrac{1}{x^{2}}}}=\\dfrac{x}{-x \\sqrt{1+\\dfrac{1}{x^{2}}}}=\\dfrac{-1}{\\sqrt{1+\\dfrac{1}{x^{2}}}}
$$

Comme à la question précédente, on en déduit que $\\dfrac{x}{\\sqrt{x^{2}+1}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} \\dfrac{-1}{1}=-1$.

7. Utilisons la méthode de l'expression conjuguée :

$\\dfrac{\\sqrt{x+1}-1}{x}=\\dfrac{(\\sqrt{x+1}-1)(\\sqrt{x+1}+1)}{x(\\sqrt{x+1}+1)}=\\dfrac{(x+1)-1}{x(\\sqrt{x+1}+1)}=\\dfrac{1}{\\sqrt{x+1}+1} \\underset{x \\rightarrow 0}{\\longrightarrow}$. 8. Utilisons la méthode de l'expression conjuguée :

$\\sqrt{x^{2}+3 x}-x=\\dfrac{\\left(\\sqrt{x^{2}+3 x}-x\\right)\\left(\\sqrt{x^{2}+3 x}+x\\right)}{\\sqrt{x^{2}+3 x}+x}=\\dfrac{\\left(x^{2}+3 x\\right)-x^{2}}{\\sqrt{x^{2}+3 x}+x}=\\dfrac{3 x}{\\sqrt{x^{2}+3 x}+x}$

On a encore une indétermination, du type $\\dfrac{\\infty}{\\infty}$, que l'on lève en factorisant numérateur et dénominateur par les termes dominants (ici, $x$ ) :

$$
\\dfrac{3 x}{\\sqrt{x^{2}+3 x}+x}=\\dfrac{3 x}{\\sqrt{x^{2}\\left(1+\\dfrac{3}{x}\\right)}+x}=\\dfrac{3 x}{x \\sqrt{1+\\dfrac{3}{x}}+x}=\\dfrac{3}{\\sqrt{1+\\dfrac{3}{x}}+1} \\rightarrow_{x \\rightarrow+\\infty} \\dfrac{3}{2} .
$$

Ci-dessus, on a supposé que $x>0$ (ce qui n'est pas gênant puisque l'on veut faire tendre $x$ vers $+\\infty)$, ce qui a permis d'écrire que $\\sqrt{x^{2}}=|x|=x$.

9. Puisque l'on veut la limite en $+\\infty$, il n'est pas gênant de supposer $x>0$. Or pour tout $x \\in \\mathbb{R}$, on a $-1 \\leqslant \\sin x \\leqslant 1$; en divisant par $x>0$, on en déduit

$$
\\text { Pour tout } x>0, \\quad \\dfrac{-1}{x} \\leqslant \\dfrac{\\sin x}{x} \\leqslant \\dfrac{1}{x} \\text {. }
$$

Les membres de gauche et de droite de cet encadrement tendent vers 0 quand $x$ tend vers $+\\infty$; on en déduit par théorème d'encadrement que $\\dfrac{\\sin x}{x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$.

10. F.I. du type $\\dfrac{\\infty}{\\infty}$ : on met en facteur les termes dominants au numérateur et au dénominateur.

$$
\\dfrac{4 x^{2}-5 \\ln x+x^{3}}{x^{4}-e^{x}+3 x}=\\dfrac{x^{3}}{e^{x}} \\times \\dfrac{\\dfrac{4}{x}-\\dfrac{5 \\ln x}{x^{3}}+1}{\\dfrac{x^{4}}{e^{x}}-1+\\dfrac{3 x}{e^{x}}}
$$

Or par croissances comparées, $\\dfrac{e^{x}}{x^{3}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty$, donc $\\dfrac{x^{3}}{e^{x}}=\\dfrac{1}{\\left(\\dfrac{e^{x}}{x^{3}}\\right)} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$

Avec des raisonnements similaires, par croissances comparées, on établit que :

- $\\dfrac{4}{x}-\\dfrac{5 \\ln x}{x^{3}}+1 \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 1$ (en utilisant que le fait que $\\dfrac{\\ln x}{x^{3}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$ ) ;
- $\\dfrac{x^{4}}{e^{x}}-1+\\dfrac{3 x}{e^{x}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}-1$

Finalement $: \\dfrac{4 x^{2}-5 \\ln x+x^{3}}{x^{4}-e^{x}+3 x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$.

11. F.I. du type $\\dfrac{\\infty}{\\infty}$ : on met en facteur les termes dominants au numérateur et au dénominateur. Lorsque $x \\rightarrow+\\infty$, le terme dominant est $e^{x}$, d'où :

$$
\\dfrac{e^{x}-e^{-x}}{e^{x}+e^{-x}}=\\dfrac{e^{x}\\left(1-e^{-2 x}\\right)}{e^{x}\\left(1+e^{-2 x}\\right)}=\\dfrac{1-e^{-2 x}}{1+e^{-2 x}}
$$

or $e^{-2 x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$ (par composition de limites, car $-2 x \\underset{x \\rightarrow+\\infty}{\\longrightarrow}-\\infty$ et $e^{y} \\underset{y \\rightarrow-\\infty}{\\longrightarrow} 0$ ), $\\operatorname{donc} \\dfrac{e^{x}-e^{-x}}{e^{x}+e^{-x}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 1$ 12. F.I. du type $\\dfrac{\\infty}{\\infty}$ : on met en facteur les termes dominants au numérateur et au dénominateur. Lorsque $x \\rightarrow-\\infty$, le terme dominant est $e^{-x}$, d'où :

$$
\\dfrac{e^{x}-e^{-x}}{e^{x}+e^{-x}}=\\dfrac{e^{-x}\\left(e^{2 x}-1\\right)}{e^{-x}\\left(e^{2 x}+1\\right)}=\\dfrac{e^{2 x}-1}{e^{2 x}+1}
$$

or $e^{2 x} \\underset{x \\rightarrow-\\infty}{\\longrightarrow} 0$ (par composition de limites, car $2 x \\underset{x \\rightarrow-\\infty}{\\longrightarrow}-\\infty$ et $e^{y} \\underset{y \\rightarrow-\\infty}{\\longrightarrow} 0$ ), $\\operatorname{donc} \\dfrac{e^{x}-e^{-x}}{e^{x}+e^{-x}} \\underset{x \\rightarrow-\\infty}{\\longrightarrow}-1$

13. Quand $x$ tend vers $+\\infty$, les trois quantités en jeu sont de l'ordre de grandeur de $x$ (intuitivement, pour $x$ "grand", on a par exemple $\\sqrt{x^{2}+1} \\approx \\sqrt{x^{2}}=x$ ).

On factorise donc le numérateur et le dénominateur par $x$, en faisant attention au signe de $x$ : puisque l'on veut la limite en $+\\infty$, on va supposer $x>0$, et donc $\\sqrt{x^{2}}=|x|=x$. Ainsi :

$$
\\dfrac{\\sqrt{x^{2}-1}+3 x}{\\sqrt{x^{2}+1}}=\\dfrac{x \\sqrt{1-\\dfrac{1}{x^{2}}}+3 x}{x \\sqrt{1+\\dfrac{1}{x^{2}}}}=\\dfrac{\\sqrt{1-\\dfrac{1}{x^{2}}}+3}{\\sqrt{1+\\dfrac{1}{x^{2}}}}
$$

Il n'y a plus de forme indéterminée : par somme, quotient et composée de limites, $\\dfrac{\\sqrt{x^{2}-1}+3 x}{\\sqrt{x^{2}+1}} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 4$

14. Ici il faut être un peu astucieux, en écrivant :

$$
\\dfrac{\\ln (x+1)}{\\ln x}=\\dfrac{\\ln \\left(x\\left(1+\\dfrac{1}{x}\\right)\\right)}{\\ln x}=\\dfrac{\\ln (x)+\\ln \\left(1+\\dfrac{1}{x}\\right)}{\\ln x}=1+\\dfrac{\\ln \\left(1+\\dfrac{1}{x}\\right)}{\\ln x}
$$

Or $1+\\dfrac{1}{x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 1$ et $\\ln (y) \\underset{y \\rightarrow 1}{\\longrightarrow} \\ln (1)=0$ donc par composition, $\\ln \\left(1+\\dfrac{1}{x}\\right) \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$. D'autre part, $\\ln x \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty$; ainsi par quotient, $\\dfrac{\\ln \\left(1+\\dfrac{1}{x}\\right)}{\\ln x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$, et donc $\\dfrac{\\ln (x+1)}{\\ln x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 1$

## Dérivées, primitives, intégrales

## V.1 Dérivation

On considère $u$ et $v$ deux fonctions dérivables sur un intervalle $I$, ainsi qu'un réel $\\lambda$. Alors :

$(u+v)^{\\prime}=u^{\\prime}+v^{\\prime}, \\quad(\\lambda u)^{\\prime}=\\lambda u^{\\prime}, \\quad(u v)^{\\prime}=u^{\\prime} v+u v^{\\prime}, \\quad\\left(\\dfrac{1}{v}\\right)^{\\prime}=\\dfrac{-v^{\\prime}}{v^{2}}, \\quad\\left(\\dfrac{u}{v}\\right)^{\\prime}=\\dfrac{u^{\\prime} v-u v^{\\prime}}{v^{2}}$.

La formule de dérivation d'un produit se généralise au cas d'un produit de trois fonctions ou plus. Par exemple, pour un produit de trois fonctions :

$$
(u v w)^{\\prime}=u^{\\prime} v w+u v^{\\prime} w+u v w^{\\prime}
$$

## Dérivée d'une composée

On considère deux fonctions $u$ et $v$ telles que :

- $u$ est définie sur un intervalle $I$ et prend ses valeurs dans un intervalle $J$;
- $v$ est définie sur $J$

On peut alors définir la composée de $u$ par $v$, notée $v \\circ u$, de la façon suivante :

$$
\\text { Pour tout } x \\in I, \\quad(v \\circ u)(x)=v(u(x)) .
$$

En d'autres termes, $v \\circ u$ est la fonction qui consiste à appliquer successivement $u$ puis $v$ (dans cet ordre!). On résume parfois cela par le diagramme suivant :

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-22.jpg?height=145&width=832&top_left_y=1218&top_left_x=360)

Si $u$ et $v$ sont dérivables, leur composée $v \\circ u$ l'est aussi, et $(v \\circ u)^{\\prime}=u^{\\prime} \\times\\left(v^{\\prime} \\circ u\\right)$. Autrement dit,

$$
\\text { Pour tout } x \\in I, \\quad(v \\circ u)^{\\prime}(x)=u^{\\prime}(x) \\times v^{\\prime}(u(x))
$$

En appliquant cette formule en prenant pour $v$ une des fonctions usuelles étudiées au lycée, on trouve de nombreuses formules de dérivation. Par exemple, en prenant $v(x)=\\ln (x)$, on a $v^{\\prime}(x)=\\dfrac{1}{x}$, donc la formule ci-dessus entraîne

$$
(\\ln (u))^{\\prime}(x)=u^{\\prime}(x) \\times \\dfrac{1}{u(x)}
$$

ou plus succinctement $:(\\ln (u))^{\\prime}=\\dfrac{u^{\\prime}}{u}$. De la même façon, si $u$ est une fonction dérivable sur un intervalle $I$, on a :

| Fonction | Dérivée | Condition |
| :---: | :---: | :---: |
| $u^{n}(n \\in \\mathbb{N})$ | $n u^{\\prime} u^{n-1}$ | $/$ |
| $u^{n}(n<-1)$ | $n u^{\\prime} u^{n-1}$ | $u(x) \\neq 0$ sur $I$ |
| $\\dfrac{1}{u}$ | $-\\dfrac{u^{\\prime}}{u^{2}}$ | $u(x) \\neq 0$ sur $I$ |
| $\\sqrt{u}$ | $\\dfrac{u^{\\prime}}{2 \\sqrt{u}}$ | $u(x)>0$ sur $I$ |
| $\\ln (u)$ | $\\dfrac{u^{\\prime}}{u}$ | $u(x)>0$ sur $I$ |
| $e^{u}$ | $u^{\\prime} e^{u}$ | $/$ |
| $\\cos (u)$ | $-u^{\\prime} \\sin (u)$ | $/$ |
| $\\sin (u)$ | $u^{\\prime} \\cos (u)$ | $/$ |

Exemple : dériver la fonction $f$ définie par $f(x)=\\cos \\left(\\dfrac{1}{x^{3}}\\right)$. C'est une fonction de la forme $f(x)=\\cos (u(x))$, avec $u(x)=\\dfrac{1}{x^{3}}=x^{-3}$. On a $u^{\\prime}(x)=-3 x^{-4}=-\\dfrac{3}{x^{4}}$, donc d'après la formule de dérivée d'une composée :

$$
f^{\\prime}(x)=-u^{\\prime}(x) \\sin (u(x))=-\\dfrac{3}{x^{4}} \\sin \\left(\\dfrac{1}{x^{3}}\\right)
$$

Remarque : pour dériver une expression de la forme $\\dfrac{1}{x^{n}}$, il vaut mieux éviter de recourir à la formule de dérivation d'un quotient, qui conduit dans ce cas à beaucoup de calculs superflus (essayez pour voir!). À la place, il est bien plus efficace d'écrire cette fraction sous la forme $x^{-n}$, et d'utiliser la formule de dérivation d'une puissance.

Cette formule est en effet vraie même pour les puissances négatives : quelque que soit $\\alpha \\in \\mathbb{Z}$, la fonction $u$ définie $u(x)=x^{\\alpha}$ est dérivable et $u^{\\prime}(x)=\\alpha x^{\\alpha-1}$.

## Exercice 11

Donner une expression aussi simple que possible de $f \\circ g(x)$ et de $g \\circ f(x)$.

1. $f(x)=\\dfrac{2 x+3}{3 x+5}, \\quad g(x)=\\dfrac{x-1}{x+1}$
2. $f(x)=x^{2}+3 x+1, g(x)=\\dfrac{2 x+1}{x-3}$
3. $f(x)=\\dfrac{x-1}{x^{2}+1}, \\quad g(x)=\\dfrac{x+1}{x-1}$

## Correction de l'exercice 11

1. $f \\circ g(x)=f(g(x))=\\dfrac{2 g(x)+3}{3 g(x)+5}=\\dfrac{2\\left(\\dfrac{x-1}{x+1}\\right)+3}{3\\left(\\dfrac{x-1}{x+1}\\right)+5}=\\dfrac{2(x-1)+3(x+1)}{3(x-1)+5(x+1)}=\\dfrac{5 x+1}{8 x+2}$.

Par un calcul du même genre, on obtient $g \\circ f(x)=\\dfrac{-x-2}{5 x+8}$.

2. On a $f \\circ g(x)=f(g(x))=\\dfrac{\\left(\\dfrac{x+1}{x-1}\\right)-1}{\\left(\\dfrac{x+1}{x-1}\\right)^{2}+1}$.

Pour se débarrasser des fractions au numérateur et au dénominateur, on multiplie en haut et en bas par $(x-1)^{2}$ :

$$
f \\circ g(x)=\\dfrac{(x+1)(x-1)-(x-1)^{2}}{(x+1)^{2}+(x-1)^{2}}=\\dfrac{2 x-2}{2 x^{2}+2}=\\dfrac{x-1}{x^{2}+1}
$$

Par un calcul du même genre, on trouve que $g \\circ f(x)=\\dfrac{-x^{2}-x}{x^{2}-x+2}$.

3. On a :

$$
\\begin{aligned}
f \\circ g(x) & =f(g(x))=\\left(\\dfrac{2 x+1}{x-3}\\right)^{2}+3\\left(\\dfrac{2 x+1}{x-3}\\right)+1 \\\\
& =\\dfrac{(2 x+1)^{2}}{(x-3)^{2}}+\\dfrac{3(2 x+1)}{x-3}+1 \\\\
& =\\dfrac{(2 x+1)^{2}}{(x-3)^{2}}+\\dfrac{3(2 x+1)(x-3)}{(x-3)^{2}}+\\dfrac{(x-3)^{2}}{(x-3)^{2}} \\\\
& =\\dfrac{(2 x+1)^{2}+3(2 x+1)(x-3)+(x-3)^{2}}{(x-3)^{2}} \\\\
& =\\dfrac{11 x^{2}-17 x+1}{(x-3)^{2}}
\\end{aligned}
$$

D'autre part, $g \\circ f(x)=\\dfrac{2\\left(x^{2}+3 x+1\\right)+1}{\\left(x^{2}+3 x+1\\right)-3}=\\dfrac{2 x^{2}+6 x+3}{x^{2}+3 x-2}$

2. L'étude rigoureuse du domaine de dérivabilité sera développée davantage en prépa

## Exercice 12

Dériver les fonctions suivantes, en simplifiant et factorisant au maximum $f^{\\prime}(x)$. On précisera à chaque fois sur quel domaine ${ }^{2}$ on peut dériver $f$.

1. $f(x)=x+\\sqrt{4-x^{2}}$
2. $f(x)=x \\ln (x) e^{\\sqrt{x}}$
3. $f(x)=(1-x) \\sqrt{x}$
4. $f(x)=\\dfrac{1}{\\cos x}$
5. $f(x)=\\dfrac{\\sqrt{x}}{x-2}$
6. $f(x)=\\ln (\\sqrt{x}+1)$
7. $f(x)=\\dfrac{2 x+3}{4 x-5}$
8. $f(x)=x^{2} e^{-x}$
9. $f(x)=\\ln \\left(\\dfrac{1-x}{1+x}\\right)$

## Correction de l'exercice 12

1. La fonction racine carrée est dérivable sur $] 0,+\\infty[$. La fonction $f$ est donc dérivable en tout point $x$ tel que $4-x^{2}>0$, c'est-à-dire (après résolution) tel que $x \\in]-2,2[$.

On a $f(x)=x+\\sqrt{u(x)}$, avec $u(x)=4-x^{2}$. Or $u^{\\prime}(x)=-2 x$, donc

Pour tout $x \\in]-2,2\\left[, f^{\\prime}(x)=1+\\dfrac{u^{\\prime}(x)}{2 u(x)}=1-\\dfrac{2 x}{2 \\sqrt{4-x^{2}}}=1-\\dfrac{x}{\\sqrt{4-x^{2}}}=\\dfrac{\\sqrt{4-x^{2}}-x}{\\sqrt{4-x^{2}}}\\right.$.

2. $f$ est dérivable sur $] 0,+\\infty[$ (comme la racine carrée), et pour tout $x>0$ :

$$
f^{\\prime}(x)=-\\sqrt{x}+(1-x) \\dfrac{1}{2 \\sqrt{x}}=-\\dfrac{2(\\sqrt{x})^{2}}{2 \\sqrt{x}}+\\dfrac{1-x}{2 \\sqrt{x}}=-\\dfrac{2 x}{2 \\sqrt{x}}+\\dfrac{1-x}{2 \\sqrt{x}}=\\dfrac{1-3 x}{2 \\sqrt{x}} .
$$

3. $f$ est dérivable en tout $x$ tel que $x>0$ (pour que la racine carrée soit dérivable) et que $x \\neq 2$ (pour que le dénominateur ne s'annule pas). Pour un tel $x$

$$
f^{\\prime}(x)=\\dfrac{\\dfrac{1}{2 \\sqrt{x}}(x-2)-\\sqrt{x}}{(x-2)^{2}}=\\dfrac{x-2-2(\\sqrt{x})^{2}}{2 \\sqrt{x}(x-2)^{2}}=-\\dfrac{x+2}{2 \\sqrt{x}(x-2)^{2}}
$$

4. $f$ est dérivable en tout $x$ tel que $x>0$ et $\\sqrt{x}+1>0$. Cette seconde condition est toujours vérifiée, donc $f$ est dérivable sur $] 0,+\\infty$ [, et pour tout $x>0$ :

$$
f^{\\prime}(x)=\\dfrac{\\dfrac{1}{2 \\sqrt{x}}}{\\sqrt{x}+1}=\\dfrac{1}{2 \\sqrt{x}(\\sqrt{x}+1)}
$$

5. L'exponentielle est dérivable sur $\\mathbb{R}$ donc $f$ l'est aussi, et pour tout $x \\in \\mathbb{R}$, $f^{\\prime}(x)=2 x e^{-x}+x^{2}\\left(-e^{-x}\\right)=x(2-x) e^{-x}$ 6. $f$ est dérivable sur $] 0,+\\infty[$ (comme la racine carrée et $\\ln$ ). On la dérive à l'aide en utilisant la formule de dérivation du produit $(u v w)^{\\prime}=u^{\\prime} v w+u v^{\\prime} w+u v w^{\\prime}$ : pour tout $x>0$,

$f^{\\prime}(x)=\\ln (x) e^{\\sqrt{x}}+x \\dfrac{1}{x} e^{\\sqrt{x}}+x \\ln (x)\\left(\\dfrac{1}{2 \\sqrt{x}} e^{\\sqrt{x}}\\right)=\\left(1+\\left(1+\\dfrac{\\sqrt{x}}{2}\\right) \\ln (x)\\right) e^{\\sqrt{x}}$

où l'on a utilisé le fait que pour $x \\geqslant 0, \\dfrac{x}{\\sqrt{x}}=\\dfrac{(\\sqrt{x})^{2}}{\\sqrt{x}}=\\sqrt{x}$.

7. $f$ est dérivable en tout $x$ tel que $\\cos x \\neq 0$, c'est-à-dire en tout $x$ qui n'est pas de la forme $\\dfrac{\\pi}{2}+k \\pi$ pour un certain $k \\in \\mathbb{Z}$. Pour un tel $x$, on a $f^{\\prime}(x)=\\dfrac{\\sin x}{\\cos ^{2} x}$.
8. $f$ est dérivable en tout $x$ tel que $4 x-5 \\neq 0$, c'est-à-dire tel que $x \\neq \\dfrac{5}{4}$

Pour tout $x \\in \\mathbb{R} \\backslash\\left\\{\\dfrac{5}{4}\\right\\}$, on a $f^{\\prime}(x)=\\dfrac{2(4 x-5)-4(2 x+3)}{(4 x-5)^{2}}=\\dfrac{-22}{(4 x-5)^{2}}$

9. $f$ est dérivable en tout $x$ tel que $\\dfrac{1-x}{1+x}>0$. Un tableau de signe montre que cela se produit si et seulement si $x \\in]-1,1[$.

On a $f(x)=\\ln (u(x))$ avec $u(x)=\\dfrac{1-x}{1+x}$. On voit par calculs que $u^{\\prime}(x)=$ $\\dfrac{-2}{(1+x)^{2}}$, donc pour tout $\\left.x \\in\\right]-1,1[$ :

$$
f^{\\prime}(x)=\\dfrac{u^{\\prime}(x)}{u(x)}=\\dfrac{-2}{(1+x)^{2}} \\times \\dfrac{1+x}{1-x}=\\dfrac{-2}{(1+x)(1-x)}
$$

## Exercice 13

Dériver les fonctions suivantes, en simplifiant et factorisant au maximum $f^{\\prime}(x)$. Le domaine de dérivabilité n'est pas demandé.

1. $f(x)=x \\ln (x)-x$
2. $f(x)=\\dfrac{1}{x^{3}}$
3. $f(x)=\\dfrac{2 x^{2}-5 x+3}{(x-3)^{2}}$
4. $f(x)=\\dfrac{\\cos x}{\\sin x}$ (fonction cotangente)
5. $f(x)=\\dfrac{\\ln x}{x}$
6. $f(x)=\\dfrac{\\sin x+\\cos x}{\\sin x-\\cos x}$
7. $f(x)=\\dfrac{\\sqrt{x}-1}{\\sqrt{x}+1}$
8. $f(x)=\\dfrac{x^{2}-3 x+1}{-3 x^{2}+2 x+1}$
$f(x)=\\dfrac{\\sqrt{x}-1}{\\sqrt{x}+1}$
9. $f(x)=\\dfrac{\\mathrm{e}^{x}+\\mathrm{e}^{-x}}{\\mathrm{e}^{x}-\\mathrm{e}^{-x}}$
10. $f(x)=\\dfrac{\\sin x}{\\cos x}$ (fonction tangente)
11. $f(x)=\\dfrac{1-\\ln x}{1+\\ln x}$

Correction de l'exercice 13

1. $f^{\\prime}(x)=\\ln x$.
2. $\\dfrac{1}{x^{3}}=x^{-3}$, donc $f^{\\prime}(x)=-3 x^{-4}=-\\dfrac{3}{x^{4}}$
3. Dérivation d'un quotient : $f^{\\prime}(x)=\\dfrac{(4 x-5) \\cdot(x-3)^{2}-\\left(2 x^{2}-5 x+3\\right) \\cdot 2(x-3)}{(x-3)^{4}}$.

Après simplification par $(x-3)$ et calculs, on obtient $f^{\\prime}(x)=\\dfrac{-7 x+9}{(x-3)^{3}}$.

4. $f^{\\prime}(x)=\\dfrac{1-\\ln x}{x^{2}}$.
5. $f^{\\prime}(x)=\\dfrac{\\dfrac{1}{2 \\sqrt{x}} \\cdot(\\sqrt{x}+1)-(\\sqrt{x}-1) \\cdot \\dfrac{1}{2 \\sqrt{x}}}{(\\sqrt{x}+1)^{2}}=\\dfrac{1}{\\sqrt{x}(\\sqrt{x}+1)^{2}}$.
6. $f^{\\prime}(x)=\\dfrac{\\cos ^{2} x+\\sin ^{2} x}{\\cos ^{2} x}=\\dfrac{1}{\\cos ^{2} x}$.
7. $f^{\\prime}(x)=-\\dfrac{1}{\\sin ^{2} x}$.
8. $f^{\\prime}(x)=\\dfrac{-(\\cos x-\\sin x)^{2}-(\\sin x+\\cos x)^{2}}{(\\sin x-\\cos x)^{2}}=[\\ldots]=\\dfrac{-2}{(\\sin x-\\cos x)^{2}}$
9. $f^{\\prime}(x)=\\dfrac{(2 x-3)\\left(-3 x^{2}+2 x+1\\right)-(-6 x+2)\\left(x^{2}-3 x+1\\right)}{\\left(-3 x^{2}+2 x+1\\right)^{2}}=\\dfrac{-7 x^{2}+8 x-5}{\\left(-3 x^{2}+2 x+1\\right)^{2}}$
10. $f^{\\prime}(x)=\\dfrac{\\left(e^{x}-e^{-x}\\right)\\left(e^{x}-e^{-x}\\right)-\\left(e^{x}+e^{-x}\\right)\\left(e^{x}+e^{-x}\\right)}{\\left(e^{x}-e^{-x}\\right)^{2}}=\\dfrac{-4}{\\left(e^{x}-e^{-x}\\right)^{2}}$.
11. $f^{\\prime}(x)=\\dfrac{-\\dfrac{1}{x}(1+\\ln x)-(1-\\ln x) \\dfrac{1}{x}}{(1+\\ln x)^{2}}=\\dfrac{-2}{x(1+\\ln x)^{2}}$.

## Exercice 14

Dériver les fonctions suivantes, en simplifiant et factorisant au maximum $f^{\\prime}(x)$.

Le domaine de dérivabilité n'est pas demandé.

1. $f(x)=\\sin \\left(x^{2}\\right)$
2. $f(x)=\\cos (\\sqrt{x})$
3. $f(x)=\\sqrt{\\dfrac{1+x}{1-x}}$
4. $f(x)=x^{2} \\sin \\left(\\dfrac{1}{x}\\right)$
5. $f(x)=e^{x^{2}-3 x+4}$
6. $f(x)=\\dfrac{\\cos (\\ln x)}{x}$
7. $f(x)=\\left(x-\\dfrac{1}{x}\\right)^{3}$
8. $f(x)=\\sin ^{5} x$
9. $f(x)=\\cos ^{4} x$
10. $f(x)=\\dfrac{1}{\\left(1+x^{3}\\right)^{4}}$
11. $f(x)=\\dfrac{\\sin ^{2} x}{1+\\cos ^{2} x}$
12. $f(x)=\\mathrm{e}^{\\mathrm{e}^{x}}$
13. $f(x)=\\sqrt{x^{4}-2 x+3}$
14. $f(x)=\\ln \\left(1+\\dfrac{1}{x^{2}}\\right)$

## Correction de l'exercice 14

1. $f(x)=\\sin (u(x))$ avec $u(x)=x^{2}$, donc $f^{\\prime}(x)=2 x \\cos \\left(x^{2}\\right)$.
2. De même qu'au 1., $f^{\\prime}(x)=-\\dfrac{1}{2 \\sqrt{x}} \\sin (\\sqrt{x})$.
3. Dérivée d'un produit, où l'on utilise que $(\\sin (u))^{\\prime}=u^{\\prime} \\cos (u)$ pour le second facteur :

$$
f^{\\prime}(x)=2 x \\sin \\left(\\dfrac{1}{x}\\right)+x^{2}\\left(-\\dfrac{1}{x^{2}} \\cos \\left(\\dfrac{1}{x}\\right)\\right)=2 x \\sin \\left(\\dfrac{1}{x}\\right)-\\cos \\left(\\dfrac{1}{x}\\right)
$$

4. En considérant $f(x)$ comme un quotient dont le numérateur est une fonction composée, on obtient :

$$
f^{\\prime}(x)=\\dfrac{-\\dfrac{1}{x} \\sin (\\ln x) \\cdot x-\\cos (\\ln x) \\cdot 1}{x^{2}}=-\\dfrac{\\sin (\\ln x)+\\cos (\\ln x)}{x^{2}} .
$$

5. $f(x)=u(x)^{5}$ avec $u(x)=\\sin x$, donc $f^{\\prime}(x)=5 u^{\\prime}(x)(u(x))^{4}=5 \\cos x \\sin ^{4} x$.
6. $f^{\\prime}(x)=-4 \\sin x \\cos ^{3} x$ (de même).
7. En considérant $f(x)$ comme un quotient dont le numérateur et le dénominateur sont des fonctions composées, on obtient :

$$
\\begin{aligned}
f^{\\prime}(x) & =\\dfrac{(2 \\cos x \\sin x)\\left(1+\\cos ^{2} x\\right)-\\left(\\sin ^{2} x\\right)(-2 \\sin x \\cos x)}{\\left(1+\\cos ^{2} x\\right)^{2}} \\\\
& =\\dfrac{2 \\sin x \\cos x\\left(1+\\cos ^{2} x+\\sin ^{2} x\\right)}{\\left(1+\\cos ^{2} x\\right)^{2}}=\\dfrac{4 \\sin x \\cos x}{\\left(1+\\cos ^{2} x\\right)^{2}} .
\\end{aligned}
$$

8. $f(x)=\\sqrt{u(x)}$ avec $u(x)=x^{4}-2 x+3$, donc

$$
f^{\\prime}(x)=\\dfrac{u^{\\prime}(x)}{2 \\sqrt{u(x)}}=\\dfrac{4 x^{3}-2}{2 \\sqrt{x^{4}-2 x+3}}=\\dfrac{2 x^{3}-1}{\\sqrt{x^{4}-2 x+3}}
$$

9. Comme à la question précédente, avec $u(x)=\\dfrac{1+x}{1-x}$. Après calculs, $u^{\\prime}(x)=$ $\\dfrac{2}{(1-x)^{2}}$, donc :

$$
f^{\\prime}(x)=\\dfrac{u^{\\prime}(x)}{2 \\sqrt{u(x)}}=\\dfrac{2}{(1-x)^{2}} \\times \\dfrac{1}{2} \\sqrt{\\dfrac{1-x}{1+x}}=\\dfrac{1}{(1-x)^{2}} \\sqrt{\\dfrac{1-x}{1+x}}
$$

10. $f^{\\prime}(x)=(2 x-3) e^{x^{2}-3 x+4}$
11. $f(x)=(u(x))^{3}$ avec $u(x)=x-\\dfrac{1}{x}$. On a $u^{\\prime}(x)=1+\\dfrac{1}{x^{2}}$, donc

$$
\\begin{aligned}
f^{\\prime}(x)=3 u^{\\prime}(x)(u(x))^{2} & =3\\left(1+\\dfrac{1}{x^{2}}\\right)\\left(x-\\dfrac{1}{x}\\right)^{2} \\\\
& =3\\left(\\dfrac{x^{2}+1}{x^{2}}\\right)\\left(\\dfrac{x^{2}-1}{x}\\right)^{2} \\\\
& =\\dfrac{3\\left(x^{2}+1\\right)\\left(x^{2}-1\\right)^{2}}{x^{4}}
\\end{aligned}
$$

12. Le plus simple pour dériver cette fonction, c'est de la considérer non pas comme une fraction, mais comme une puissance négative : $f(x)=\\left(1+x^{3}\\right)^{-4}$, d'où

$$
f^{\\prime}(x)=-4\\left(3 x^{2}\\right)\\left(1+x^{3}\\right)^{-5}=\\dfrac{-12 x^{2}}{\\left(1+x^{3}\\right)^{5}}
$$

13. $f^{\\prime}(x)=e^{u(x)}$ avec $u(x)=e^{x}$. Puisque $u^{\\prime}(x)=e^{x}$, on a

$$
f^{\\prime}(x)=u^{\\prime}(x) e^{u(x)}=e^{x} e^{e^{x}}=e^{1+e^{x}}
$$

14. $f(x)=\\ln (u(x))$ avec $u(x)=1+\\dfrac{1}{x^{2}}$. On dérive $u$ en l'écrivant plutôt à l'aide de puissances négatives : on a $u(x)=1+x^{-2}$, donc $u^{\\prime}(x)=-2 x^{-3}=-\\dfrac{2}{x^{3}}$, et ainsi

$$
f^{\\prime}(x)=\\dfrac{u^{\\prime}(x)}{u(x)}=-\\dfrac{2}{x^{3}\\left(1+\\dfrac{1}{x^{2}}\\right)}=-\\dfrac{2}{x^{3}+x}=-\\dfrac{2}{x\\left(x^{2}+1\\right)} .
$$

Exercice 15

Soit $f(x)$ la fonction définie sur $\\mathbb{R}^{*}$ par $f(x)=x e^{\\dfrac{1}{x}}$

Calculer $f^{\\prime}(x), f^{\\prime \\prime}(x)$ et $f^{(3)}(x)$.

On rappelle que $f^{\\prime \\prime}$ désigne la dérivée seconde de $f$, obtenue en dérivant $f$ deux fois de suite; et $f^{(3)}$ désigne la dérivée troisième de $f$, obtenue en dérivant $f$ trois fois de suite.

## Correction de l'exercice 15

Soit $x \\in] 0,+\\infty[$. On dérive trois fois de suite l'expression de $f$, en utilisant les formules de dérivation d'un produit et d'une composée

$$
\\begin{gathered}
f^{\\prime}(x)=e^{\\dfrac{1}{x}}-x \\dfrac{1}{x^{2}} e^{\\dfrac{1}{x}}=\\left(1-\\dfrac{1}{x}\\right) e^{\\dfrac{1}{x}} \\\\
f^{\\prime \\prime}(x)=\\dfrac{1}{x^{2}} e^{\\dfrac{1}{x}}-\\left(1-\\dfrac{1}{x}\\right) \\dfrac{1}{x^{2}} e^{\\dfrac{1}{x}}=\\dfrac{1}{x^{3}} e^{\\dfrac{1}{x}} \\\\
f^{(3)}(x)=-\\dfrac{3}{x^{4}} e^{\\dfrac{1}{x}}-\\dfrac{1}{x^{3}} \\dfrac{1}{x^{2}} e^{\\dfrac{1}{x}}=-\\left(\\dfrac{3}{x^{4}}+\\dfrac{1}{x^{5}}\\right) e^{\\dfrac{1}{x}}=-\\dfrac{3 x+1}{x^{5}} e^{\\dfrac{1}{x}} .
\\end{gathered}
$$

Exercice 16 Dérivées de composées ... mais plus difficiles.

Déterminer les dérivées des fonctions suivantes. Le domaine de dérivabilité n'est pas demandé.

1. $f(x)=e^{\\sqrt{x^{2}-x+1}}$
2. $f(x)=\\dfrac{1}{2 \\sqrt{1+x^{2}}+1}$
3. $f(x)=\\ln \\left(x+\\sqrt{x^{2}+1}\\right)$
4. $f(x)=\\sin \\left(\\sqrt{1-x^{2}}\\right)$
5. $f(x)=\\sqrt{\\sqrt{1+x^{2}}-x}$
6. $f(x)=\\sqrt{\\ln \\left(1+3 x^{2}\\right)}$

## Correction de l'exercice 16

1. $f(x)=e^{u(x)}$ avec $u(x)=\\sqrt{x^{2}-x+1}$. Ici $u$ est elle-même une fonction composée ! Il faut donc appliquer à deux reprises la formule de dérivation d'une composée. Une première application de cette formule donne $u^{\\prime}(x)=\\dfrac{2 x-1}{2 \\sqrt{x^{2}-x+1}}$

On en déduit, par une seconde application :

$$
f^{\\prime}(x)=u^{\\prime}(x) e^{u(x)}=\\dfrac{2 x-1}{2 \\sqrt{x^{2}-x+1}} e^{\\sqrt{x^{2}-x+1}}
$$

2. $f(x)=\\ln (u(x))$ avec $u(x)=x+\\sqrt{x^{2}+1}$. Comme à la question précédente, la fonction $u$ est elle-même formée à partir d'une fonction composée, il faut donc la dériver à part

$$
u^{\\prime}(x)=1+\\dfrac{2 x}{2 \\sqrt{x^{2}+1}}=1+\\dfrac{x}{\\sqrt{x^{2}+1}}=\\dfrac{\\sqrt{x^{2}+1}+x}{\\sqrt{x^{2}+1}}
$$

d'où $f^{\\prime}(x)=\\dfrac{u^{\\prime}(x)}{u(x)}=\\dfrac{\\sqrt{x^{2}+1}+x}{\\sqrt{x^{2}+1}} \\cdot \\dfrac{1}{x+\\sqrt{x^{2}+1}}=\\dfrac{1}{\\sqrt{x^{2}+1}}$.

3. $f(x)=\\sqrt{u(x)}$ avec $u(x)=\\sqrt{1+x^{2}}-x$. On a

$$
u^{\\prime}(x)=\\dfrac{2 x}{2 \\sqrt{1+x^{2}}}-1=\\dfrac{x-\\sqrt{1+x^{2}}}{\\sqrt{1+x^{2}}}
$$

donc

$$
f^{\\prime}(x)=\\dfrac{u^{\\prime}(x)}{2 \\sqrt{u(x)}}=\\dfrac{x-\\sqrt{1+x^{2}}}{2 \\sqrt{1+x^{2}} \\sqrt{\\sqrt{1+x^{2}}-x}}
$$

4. $f(x)=\\dfrac{1}{u(x)}$ avec $u(x)=2 \\sqrt{1+x^{2}}+1$. On a

$$
u^{\\prime}(x)=2 \\times \\dfrac{2 x}{2 \\sqrt{1+x^{2}}}=\\dfrac{2 x}{\\sqrt{1+x^{2}}}
$$

donc

$$
f^{\\prime}(x)=-\\dfrac{u^{\\prime}(x)}{(u(x))^{2}}=-\\dfrac{2 x}{\\sqrt{1+x^{2}}\\left(2 \\sqrt{1+x^{2}}+1\\right)^{2}}
$$

5. $f(x)=\\sin (u(x))$ avec $u(x)=\\sqrt{1-x^{2}}$. On a

$$
u^{\\prime}(x)=\\dfrac{-2 x}{2 \\sqrt{1-x^{2}}}=\\dfrac{-x}{\\sqrt{1-x^{2}}}
$$

donc

$$
f^{\\prime}(x)=u^{\\prime}(x) \\cos (u(x))=\\dfrac{-x \\cos \\left(\\sqrt{1-x^{2}}\\right)}{\\sqrt{1-x^{2}}}
$$

6. $f(x)=\\sqrt{u(x)}$ avec $u(x)=\\ln \\left(1+3 x^{2}\\right)$. On a $u^{\\prime}(x)=\\dfrac{6 x}{1+3 x^{2}}$, donc

$$
f^{\\prime}(x)=\\dfrac{u^{\\prime}(x)}{2 \\sqrt{u(x)}}=\\dfrac{6 x}{2\\left(1+3 x^{2}\\right) \\sqrt{\\ln \\left(1+3 x^{2}\\right)}}=\\dfrac{3 x}{\\left(1+3 x^{2}\\right) \\sqrt{\\ln \\left(1+3 x^{2}\\right)}}
$$

## V.2 Primitives et intégrales

Soit $f$ une fonction continue sur un intervalle $I$. Une primitive de $f$ sur $I$ est une fonction $F$ dérivable sur $I$ telle que $F^{\\prime}=f$.

Une fonction $f$ continue sur un intervalle $I$ y admet toujours une primitive. De plus, les différentes primitives de $f$ diffèrent les unes des autres par des constantes : si $F$ est une primitive de $f$ sur $I$, alors toute autre primitive de $f$ sur $I$ est de la forme $G(x)=F(x)+C$, où $C \\in \\mathbb{R}$.

Pour calculer une intégrale, on utilise presque toujours le théorème suivant (dit "Théorème fondamental de l'Analyse", rien que ça !) :
![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-27.jpg?height=404&width=1292&top_left_y=651&top_left_x=120)

## Primitives classiques

| Fonction | Une primitive | Intervalle |
| :---: | :---: | :---: |
| $x^{n} \\quad(n \\in \\mathbb{N})$ | $\\dfrac{x^{n+1}}{n+1}$ | $\\mathbb{R}$ |
| $x^{n} \\quad(n<-1)$ | $\\dfrac{x^{n+1}}{n+1}$ | ]$-\\infty, 0[$ ou $] 0,+\\infty$ |
| $\\dfrac{1}{x}$ | $\\ln (x)$ | ] $0,+\\infty[$ |
| $e^{\\alpha x} \\quad(\\alpha \\neq 0)$ | $\\dfrac{e^{\\alpha x}}{\\alpha}$ | $\\mathbb{R}$ |
| $\\ln x$ | $x \\ln x-x$ | ] $0,+\\infty[$ |
| $\\sin (\\omega x) \\quad(\\omega \\neq 0)$ | $-\\dfrac{\\cos (\\omega x)}{\\omega}$ | $\\mathbb{R}$ |
| $\\cos (\\omega x) \\quad(\\omega \\neq 0)$ | $\\dfrac{\\sin (\\omega x)}{\\omega}$ | $\\mathbb{R}$ |

Opérations usuelles sur les primitives Si $F$ est une primitive de $f$ et $G$ est une primitive de $g$ sur l'intervalle $I$, alors :

- $F+G$ est une primitive de $f+g$ sur $I$
- Pour toute constante $\\lambda \\in \\mathbb{R}, \\lambda F$ est une primitive de $\\lambda f$.

En plus de ces deux règles, il est crucial de pouvoir reconnaître rapidement une dérivée de composée. La méthode est la suivante : on sait que $(v \\circ u)^{\\prime}=u^{\\prime} \\times\\left(v^{\\prime} \\circ u\\right)$. $\\mathrm{Si}$ on reconnaît, dans une expression donnée, une fonction et sa dérivée (la paire $u$ et $u^{\\prime}$ ), cela peut indiquer que l'on est face à une dérivée de composée. On utilise alors le tableau sur les dérivées de composées donné plus haut dans la section Dérivation, en le lisant de la droite vers la gauche, pour en déterminer une primitive.

Il est donc indispensable de connaître par cœur le tableau en question, en l'apprenant aussi bien de la gauche vers la droite (sens "dérivation") que de la droite vers la gauche (sens "primitivation").

En plus de ce tableau, il est utile de retenir le fait suivant : on a vu dans la section Dérivation que, si $u$ est une fonction dérivable et $n \\in \\mathbb{N}$, alors la dérivée de $u^{n+1}$ est égale à $(n+1) u^{\\prime} u^{n}$. On en déduit, en divisant par $n+1$, que

$$
\\text { Une primitive de } u^{\\prime} u^{n} \\text { est } \\dfrac{u^{n+1}}{n+1}
$$

Exemple : calculer $\\int_{1}^{4} \\dfrac{e^{\\sqrt{x}}}{\\sqrt{x}} \\mathrm{~d} x$.

On sait que, si $u(x)=\\sqrt{x}$, alors $u^{\\prime}(x)=\\dfrac{1}{2 \\sqrt{x}}$. On remarque alors que, dans l'intégrale ci-dessus, ces deux termes sont tous les deux présents (à peu de choses près) : c'est le signe que l'on est face à une dérivée de composée!

On fait alors appel à la formule appropriée, que l'on pioche parmi les formules recensées dans le tableau vu à la section Dérivation : la dérivée de $e^{u}$ est $u^{\\prime} e^{u}$.

Il reste à transformer l'expression dans l'intégrale pour la mettre sous la forme $u^{\\prime} e^{u}$ :

$$
\\dfrac{e^{\\sqrt{x}}}{\\sqrt{x}}=2 \\underbrace{\\left(\\dfrac{1}{2 \\sqrt{x}}\\right)}_{u^{\\prime}(x)} \\underbrace{e^{\\sqrt{x}}}_{e^{u(x)}}
$$

(il peut être nécessaire, comme ici, de faire apparaître "artificiellement" une constante!). On en déduit ainsi qu'une primitive de l'expression ci-dessus est la fonction $F(x)=2 e^{u(x)}=2 e^{\\sqrt{x}}$.

On conclut en appliquant le théorème fondamental de l'Analyse :

$$
\\int_{1}^{4} \\dfrac{e^{\\sqrt{x}}}{\\sqrt{x}} \\mathrm{~d} x=\\left[2 e^{\\sqrt{x}}\\right]_{1}^{4}=2 e^{\\sqrt{4}}-2 e^{\\sqrt{1}}=2\\left(e^{2}-e^{1}\\right) .
$$

## Exercice 17

Calculer les intégrales suivantes :

1. $I=\\int_{0}^{2}\\left(x^{3}-2 x+1\\right) \\mathrm{d} x$
2. $I=\\int_{0}^{\\pi / 2} \\cos x \\sin ^{5} x \\mathrm{~d} x$
3. $I=\\int_{0}^{1} e^{-3 x} \\mathrm{~d} x$
4. $I=\\int_{0}^{1} \\dfrac{1}{(2 x+1)^{2}} \\mathrm{~d} x$
5. $I=\\int_{0}^{1} x e^{x^{2}} \\mathrm{~d} x$
6. $I=\\int_{0}^{2} \\dfrac{2 x-1}{x^{2}-x+1} \\mathrm{~d} x$
7. $I=\\int_{0}^{1} x^{2}\\left(x^{3}+1\\right) \\mathrm{d} x$
8. $I=\\int_{0}^{1} \\dfrac{x}{\\left(x^{2}+1\\right)^{2}} \\mathrm{~d} x$
9. $I=\\int_{1}^{3} \\dfrac{\\ln (x)}{x} \\mathrm{~d} x$
10. $I=\\int_{-1}^{1} \\dfrac{e^{x}}{1+e^{x}} \\mathrm{~d} x$

Indications pour l'exercice $\\mathbf{1 7}$

Reconnaitre (ou faire apparaître) une forme du type ...

1. quelle est la primitive de 3. $u^{\\prime} e^{u}$
2. $-\\dfrac{u^{\\prime}}{u^{2}}$
3. $-\\dfrac{u^{\\prime}}{u^{2}}$
$x \\longmapsto x^{n}$ ?
4. $u^{\\prime} u$
5. $\\dfrac{u^{\\prime}}{u}$
6. $\\dfrac{u^{\\prime}}{u}$

## Correction de l'exercice 17

1. $I=\\left[\\dfrac{x^{4}}{4}-x^{2}+x\\right]_{0}^{2}=2$
2. $I=\\left[\\dfrac{1}{6} \\sin ^{6} x\\right]_{0}^{\\dfrac{\\pi}{2}}=\\dfrac{1}{6}$
3. $I=\\left[-\\dfrac{1}{3} e^{-3 x}\\right]_{0}^{1}=\\dfrac{1-e^{-3}}{3}$
4. $I=\\left[\\dfrac{1}{2} e^{x^{2}}\\right]_{0}^{1}=\\dfrac{e-1}{2}$
5. $I=\\left[-\\dfrac{1}{2}\\left(\\dfrac{1}{2 x+1}\\right)\\right]_{0}^{1}=\\dfrac{1}{3}$
6. $I=\\left[\\dfrac{1}{6}\\left(x^{3}+1\\right)^{2}\\right]_{0}^{1}=\\dfrac{1}{2}$
7. $I=\\left[\\dfrac{1}{2}(\\ln (x))^{2}\\right]_{1}^{3}=\\dfrac{1}{2}(\\ln 3)^{2}$
8. $I=\\left[\\ln \\left(x^{2}-x+1\\right)\\right]_{0}^{2}=\\ln 3$
9. $I=\\left[-\\dfrac{1}{2}\\left(\\dfrac{1}{x^{2}+1}\\right)\\right]_{0}^{1}=\\dfrac{1}{4}$
10. $I=\\left[\\ln \\left(1+e^{x}\\right)\\right]_{-1}^{1}=1$ VI Trigonométrie

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-29.jpg?height=1010&width=1026&top_left_y=144&top_left_x=345)

Valeurs remarquables :

| $\\theta$ | 0 | $\\pi / 6$ | $\\pi / 4$ | $\\pi / 3$ | $\\pi / 2$ | $\\pi$ |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| $\\cos (\\theta)$ | 1 | $\\dfrac{\\sqrt{3}}{2}$ | $\\dfrac{\\sqrt{2}}{2}$ | $\\dfrac{1}{2}$ | 0 | -1 |
| $\\sin (\\theta)$ | 0 | $\\dfrac{1}{2}$ | $\\dfrac{\\sqrt{2}}{2}$ | $\\dfrac{\\sqrt{3}}{2}$ | 1 | 0 |

## Propriétés

- $\\cos ^{2} \\theta+\\sin ^{2} \\theta=1$
- Périodicité $: \\cos (\\theta+2 \\pi)=\\cos (\\theta)$ et $\\sin (\\theta+2 \\pi)=\\sin (\\theta)$
- Parité : $\\cos (-\\theta)=\\cos (\\theta)$ et $\\sin (-\\theta)=-\\sin (\\theta)$
- Formules d'addition : $\\left\\{\\begin{array}{l}\\cos (a+b)=\\cos a \\cos b-\\sin a \\sin b \\\\ \\sin (a+b)=\\sin a \\cos b+\\sin b \\cos a\\end{array}\\right.$
- $\\cos (2 a)=2 \\cos ^{2}(a)-1=1-2 \\sin ^{2}(a)=\\cos ^{2}(a)-\\sin ^{2}(a)$
- $\\sin (2 a)=2 \\sin (a) \\cos (a)$


## Exercice 18

Pour chaque question, une seule réponse est correcte.

1. Résoudre $\\cos (x)=\\dfrac{1}{2} \\operatorname{sur}[0,2 \\pi]$ :
(A) $\\left\\{\\dfrac{\\pi}{3}, \\dfrac{2 \\pi}{3}\\right\\}$
(B) $\\left\\{\\dfrac{\\pi}{3}, \\dfrac{4 \\pi}{3}\\right\\}$
(C) $\\left\\{\\dfrac{\\pi}{3}, \\dfrac{5 \\pi}{3}\\right\\}$
2. Résoudre $\\sin (x)=\\sqrt{3}$ sur $[0,2 \\pi]$ :
(A) $\\left\\{\\dfrac{2 \\pi}{3}, \\dfrac{4 \\pi}{3}\\right\\}$
(B) $\\emptyset$
(C) $\\left\\{\\dfrac{\\pi}{6}, \\dfrac{\\pi}{3}\\right\\}$
3. Résoudre $\\cos (x)=0$ sur $[-\\pi, \\pi]$ :
(A) $\\left\\{\\dfrac{\\pi}{2}, \\pi\\right\\}$
(B) $\\left\\{0, \\dfrac{\\pi}{2}\\right\\}$
(C) $\\left\\{-\\dfrac{\\pi}{2}, \\dfrac{\\pi}{2}\\right\\}$
4. Résoudre $\\sin (x)=-\\dfrac{\\sqrt{2}}{2} \\operatorname{sur}[-\\pi, \\pi]$ :
(A) $\\left\\{\\dfrac{3 \\pi}{4}, \\dfrac{5 \\pi}{4}\\right\\}$
(B) $\\left\\{-\\dfrac{3 \\pi}{4},-\\dfrac{\\pi}{4}\\right\\}$
(C) $\\left\\{-\\dfrac{\\pi}{4}, \\dfrac{3 \\pi}{4}\\right\\}$
5. Résoudre $\\cos (x)<\\dfrac{\\sqrt{3}}{2} \\operatorname{sur}[-\\pi, \\pi]$ :
(A) $]-\\dfrac{\\pi}{6}, \\dfrac{\\pi}{6}[$
(B) $\\left[-\\pi,-\\dfrac{\\pi}{6}[\\cup] \\dfrac{\\pi}{6}, \\pi\\right]$
(C) $] \\dfrac{\\pi}{6}, \\dfrac{5 \\pi}{6}[$
6. Résoudre $\\sin (x) \\geqslant \\dfrac{1}{2} \\operatorname{sur}[0,2 \\pi]$ :
(A) $\\left[\\dfrac{\\pi}{6}, \\dfrac{5 \\pi}{6}\\right]$
(B) $\\left[\\dfrac{\\pi}{3}, \\dfrac{5 \\pi}{3}\\right]$
(C) $\\left[\\dfrac{\\pi}{6}, \\dfrac{2 \\pi}{6}\\right] \\cup\\left[\\dfrac{4 \\pi}{6}, \\dfrac{5 \\pi}{6}\\right]$
7. $\\cos (a-b)=$
(A) $-\\cos a \\cos b-\\sin a \\sin b$
(B) $\\cos a \\cos b+\\sin a \\sin b$
(C) $\\cos a \\sin b-\\sin a \\cos b$
8. $\\sin (a-b)=$
(A) $\\sin b \\cos a-\\sin a \\cos b$
(B) $\\sin a-\\sin b$
(C) $\\sin a \\cos b-\\cos a \\sin b$
9. $\\dfrac{1}{2} \\cos x+\\dfrac{\\sqrt{3}}{2} \\sin x=$
(A) $\\cos \\left(x+\\dfrac{\\pi}{3}\\right)$
(B) $\\cos \\left(x+\\dfrac{\\pi}{6}\\right)$
(C) $\\cos \\left(x-\\dfrac{\\pi}{3}\\right)$

## Suites arithmétiques, suites géométriques

## VII.1 Suites arithmétiques

Une suite $\\left(u_{n}\\right)$ est arithmétique de raison $r$ si :

$$
\\text { Pour tout } n \\in N, \\quad u_{n+1}=u_{n}+r \\text {. }
$$

On a alors, pour tout $n \\in \\mathbb{N}, u_{n}=u_{0}+n r$, et plus généralement $u_{n}=u_{p}+(n-p) r$.

## VII.2 Suites géométriques

Une suite $\\left(u_{n}\\right)$ est géométrique de raison $q$ si :

Pour tout $n \\in N, \\quad u_{n+1}=q u_{n}$.

On a alors, pour tout $n \\in \\mathbb{N}, u_{n}=u_{0} q^{n}$, et plus généralement $u_{n}=u_{p} q^{n-p}$.

On peut calculer la somme de termes successifs d'une suite géométrique à l'aide de la formule suivante (pour $q \\neq 1$ ) :

$$
q^{p}+\\ldots+q^{n}=q^{p} \\times \\dfrac{1-q^{n-p+1}}{1-q}=\\text { premier terme } \\times \\dfrac{1-q^{\\mathrm{nb} \\text { de termes }}}{1-q} .
$$

N.B. Le nombre d'entiers entre $p$ et $n$ ( $p$ et $n$ compris) est égal à $n-p+1$.

Limite et sens de variation

- Si $q \\geq 1$ :
- Si $u_{0}>0$, alors $\\left(u_{n}\\right)$ est strictement croissante et tend vers $+\\infty$.
- Si $u_{0}<0$, alors $\\left(u_{n}\\right)$ est strictement décroissante et tend vers $-\\infty$

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-30.jpg?height=350&width=1316&top_left_y=1533&top_left_x=88)

- $\\underset{--\\underline{T}_{--}}{\\underline{\\text { S }}}$, , alors $\\left(u_{n}\\right)$ est constante. - Si $-1<q<1$, alors $u_{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 0$. De plus :
- Si $0<q \\leq 1,\\left(u_{n}\\right)$ est strictement monotone.

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-30.jpg?height=356&width=1341&top_left_y=292&top_left_x=1489)

- Si $-1 \\leq q \\leq 0,\\left(u_{n}\\right)$ n'est pas monotone (elle oscille).

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-30.jpg?height=515&width=988&top_left_y=787&top_left_x=1729)

- Si $\\underline{q} \\leq=-1$, alors $\\left(u_{n}\\right)$ n'a pas de limite et n'est pas monotone (elle oscille).

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-30.jpg?height=515&width=989&top_left_y=1458&top_left_x=1691)

Exercice 19

Pour chaque question, une seule réponse est correcte.

1. $\\left(u_{n}\\right)$ est une suite arithmétique de raison $r=4$ et de terme initial $u_{0}=2$, alors $u_{10}=$

(A) 42

(B) 24

(C) 12

2. $\\left(u_{n}\\right)$ est une suite arithmétique de raison $r=-2$. Si $u_{1}=5$, alors $u_{8}=$
(A) -11
(B) -9
(C) 19
3. $\\left(u_{n}\\right)$ est une suite arithmétique avec $u_{7}=79$ et $u_{8}=82$. Alors sa raison $r=$
(A) 3
(B) 1,5
(C) 81,5
4. $\\left(u_{n}\\right)$ est une suite arithmétique avec $u_{10}=4$ et $u_{35}=54$. Alors sa raison $r=$
(A) 50
(B) 2
(C) 25
5. $\\left(u_{n}\\right)$ est une suite géométrique de raison $q=2$ et de terme initial $u_{0}=\\dfrac{1}{8}$, alors $u_{10}=$
(A) 1024
(B) 128
(C) 20,125
6. $\\left(u_{n}\\right)$ est une suite géométrique de premier terme $u_{0}=\\dfrac{1}{2}$ avec $u_{1}=4$, alors sa raison $q=$
(A) 2
(B) 8
(C) 3,5
7. $\\left(u_{n}\\right)$ est une suite géométrique de premier terme $u_{0}=1$ avec $u_{2}=9$, alors sa raison $q=$
(A) 9
(B) 3
(C) 3 ou -3
8. $\\left(u_{n}\\right)$ est une suite géométrique de terme initial $u_{0}=0,5$ et de raison $q=2$, alors $u_{0}+u_{1}+u_{2}+\\ldots+u_{10}=$
(A) 1023,5
(B) 511,5
(C) 2818,75
9. $\\left(u_{n}\\right)$ est une suite géométrique avec $u_{1}=128$ et de raison $q=0,5$, alors $u_{1}+u_{2}+\\ldots+u_{10}=$
(A) 255,875
(B) 704,6875
(C) 255,75
10. $1-10+100-1000+\\ldots+100000000=$
(A) 111111111
(B) -9090909
(C) 90909091
11. Que vaut la somme des 8 premiers termes de la suite géométrique de premier terme 2 et de raison 2 ?
(A) 255
(B) 500
(C) 510
12. $\\left(u_{n}\\right)$ est une suite géométrique de premier terme $u_{1}=6$ et de raison 3 . Alors $u_{n}=$
(A) $6 \\times 3^{n}$
(B) $2 \\times 3^{n}$
(C) $2 \\times 3^{n-1}$
13. On pose $u_{n}=3-2 n$. Ce terme correspond à une suite :
(A) arithmétique
(B) géométrique
(C) ni arithmétique, ni géométrique
14. On pose $u_{n}=3-n^{2}$. Ce terme correspond à une suite :
(A) arithmétique
(B) géométrique
(C) ni arithmétique, ni géométrique
15. On pose $u_{n}=3\\left(\\dfrac{1}{2}\\right)^{n}$. Ce terme correspond à une suite :
(A) arithmétique
(B) géométrique
(C) ni arithmétique, ni géométrique
16. On pose $u_{n}=\\dfrac{3}{2^{n+1}}$. Ce terme correspond à une suite :
(A) arithmétique
(B) géométrique
(C) ni arithmétique, ni géométrique
17. On pose $u_{n}=\\dfrac{n^{2}-1}{n+1}$. Ce terme correspond à une suite :
(A) arithmétique
(B) géométrique
(C) ni arithmétique, ni géométrique
18. $\\left(u_{n}\\right)$ est une suite géométrique de terme initial $u_{0}=1$ et de raison $q=2$, alors $u_{2}+u_{3}+\\ldots+u_{10}=$
(A) 1023
(B) 2047
(C) 2044
19. $\\left(u_{n}\\right)$ est une suite géométrique de terme initial $u_{0}=1$ et de raison $q=\\dfrac{1}{2}$, alors $u_{2}+u_{3}+\\ldots+u_{10}=$
(A) $2\\left(1-\\left(\\dfrac{1}{2}\\right)^{9}\\right)$
(B) $\\dfrac{1}{2}\\left(1-\\left(\\dfrac{1}{2}\\right)^{9}\\right)$
(C) $1-\\left(\\dfrac{1}{2}\\right)^{10}$

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-31.jpg?height=49&width=981&top_left_y=1609&top_left_x=1494)
10. $\\mathrm{C}$ (somme de termes d'une suite géométrique de raison -10 ) 11. C 12. B 13. A 14. $\\mathrm{C}$ 15. B

16. $\\mathrm{B}$, car $u_{n}=\\dfrac{3}{2}\\left(\\dfrac{1}{2}\\right)^{n}$
17. $\\mathrm{A}$, car $u_{n}=\\dfrac{(n-1)(n+1)}{n+1}=n-1$.

On reconnaît une suite arithmétique de raison 1

18. C 19. B

## Exercice 20

On considère la suite $\\left(u_{n}\\right)$ définie par $u_{0}=-2$ et, pour tout $n \\in \\mathbb{N}, u_{n+1}=\\dfrac{1}{2} u_{n}+3$. On pose, pour tout $n \\in \\mathbb{N}, v_{n}=u_{n}-6$.

1. Montrer que $\\left(v_{n}\\right)$ est une suite géométrique dont on précisera la raison.
2. Pour tout $n \\in \\mathbb{N}$, exprimer $v_{n}$ en fonction de $n$.
3. En déduire une expression de $u_{n}$ en fonction de $n$, puis la limite $\\left(u_{n}\\right)$.

## Correction de l'exercice 20

1. Pour tout $n \\in \\mathbb{N}$,

$$
v_{n+1}=u_{n+1}-6=\\left(\\dfrac{1}{2} u_{n}+3\\right)-6=\\dfrac{1}{2} u_{n}-3=\\dfrac{1}{2}\\left(u_{n}-6\\right)=\\dfrac{1}{2} v_{n}
$$

donc $\\left(v_{n}\\right)$ est une suite géométrique de raison $\\dfrac{1}{2}$.

2. $\\left(v_{n}\\right)$ est une suite géométrique de raison $\\dfrac{1}{2}$ et de premier terme $v_{0}=u_{0}-6=-8$, donc pour tout $n \\in N, v_{n}=-8\\left(\\dfrac{1}{2}\\right)^{n}$
3. Pour tout $n \\in \\mathbb{N}$, on a $u_{n}=v_{n}+6=6-8\\left(\\dfrac{1}{2}\\right)^{n}$.

Or $-1<\\dfrac{1}{2}<1$ donc $\\left(\\dfrac{1}{2}\\right)^{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 0$, d'où $u_{n} \\underset{n \\rightarrow+\\infty}{\\longrightarrow} 6$.

## Exercice 21

Soit $\\left(u_{n}\\right)$ et $\\left(v_{n}\\right)$ deux suites définies par $u_{0}=-1, v_{0}=1$ et, pour tout $n \\in \\mathbb{N}$,

$$
u_{n+1}=\\dfrac{2 u_{n}+v_{n}}{3} \\quad \\text { et } \\quad v_{n+1}=\\dfrac{u_{n}+3 v_{n}}{4}
$$

1. Pour tout $n \\in \\mathbb{N}$, exprimer $v_{n+1}-u_{n+1}=\\dfrac{5}{12}\\left(v_{n}-u_{n}\\right)$.
2. En déduire l'expression de $v_{n}-u_{n}$ en fonction de $n$
3. Calculer $3 u_{n+1}+4 v_{n+1}$. En déduire l'expression de $3 u_{n}+4 v_{n}$
4. Déduire des deux questions précédentes les expressions de $u_{n}$ et $v_{n}$ en fonction de $n$. Correction de l'exercice 21
5. Pour tout $n \\in \\mathbb{N}$,

$v_{n+1}-u_{n+1}=\\dfrac{u_{n}+3 v_{n}}{4}-\\dfrac{2 u_{n}+v_{n}}{3}=\\dfrac{3 u_{n}+9 v_{n}}{12}-\\dfrac{8 u_{n}+4 v_{n}}{12}=\\dfrac{5}{12}\\left(v_{n}-u_{n}\\right)$

2. Posons, pour tout $n \\in \\mathbb{N}, w_{n}=v_{n}-u_{n}$.

D'après la question précédente, $\\left(w_{n}\\right)$ est une suite géométrique de raison $\\dfrac{5}{12}$ et de premier terme $w_{0}=v_{0}-u_{0}=2$, donc pour tout $n \\in \\mathbb{N}, w_{n}=2\\left(\\dfrac{5}{12}\\right)^{n}$.

En d'autres termes, pour tout $n \\in \\mathbb{N}$,

$$
v_{n}-u_{n}=2\\left(\\dfrac{5}{12}\\right)^{n}
$$

3. Pour tout $n \\in \\mathbb{N}$,

$$
3 u_{n+1}+4 v_{n+1}=\\left(2 u_{n}+v_{n}\\right)+\\left(u_{n}+3 v_{n}\\right)=3 u_{n}+4 v_{n}
$$

Ainsi la suite de terme général $3 u_{n}+4 v_{n}$ est constante. On en déduit que pour tout $n \\in \\mathbb{N}$

$$
3 u_{n}+4 v_{n}=3 u_{0}+4 v_{0}=1
$$

4. On a montré aux deux précédentes questions que pour tout $n \\in \\mathbb{N}$,

$$
\\left\\{\\begin{aligned}
3 u_{n}+4 v_{n} & =1 \\\\
v_{n}-u_{n} & =2\\left(\\dfrac{5}{12}\\right)^{n}
\\end{aligned}\\right.
$$

Pour récupérer les expressions de $u_{n}$ et de $v_{n}$, il suffit de résoudre le système! Pour minimiser les calculs, il vaut mieux éviter de faire la résolution par substitution : procédons plutôt par combinaison, en faisant des opérations sur les lignes afin d'isoler $u_{n}$ ou $v_{n}$

En notant $L_{1}$ et $L_{2}$ les deux lignes de ce système :

- En faisant $L_{1}+3 L_{2}$, on trouve $7 v_{n}=1+6\\left(\\dfrac{5}{12}\\right)^{n}$, donc $v_{n}=\\dfrac{1}{7}\\left(1+6\\left(\\dfrac{5}{12}\\right)^{n}\\right)$.
- En faisant $L_{1}-4 L_{2}$, on trouve $7 u_{n}=1-8\\left(\\dfrac{5}{12}\\right)^{n}$, donc $u_{n}=\\dfrac{1}{7}\\left(1-8\\left(\\dfrac{5}{12}\\right)^{n}\\right)$.


## Géométrie dans le plan

## VIII.1 Repérage dans le plan

- Une base $(\\vec{i}, \\vec{j})$ du plan est la donnée de deux vecteurs $\\vec{i}$ et $\\vec{j}$ non colinéaires.

Alors, pour tout vecteur $\\vec{u}$, il existe un unique couple de réels $(x, y)$ tel que $\\vec{u}=x \\vec{i}+y \\vec{j}$. On dit que $\\vec{u}$ a pour coordonnées $(x, y)$ dans la base $(\\vec{i}, \\vec{j})$, et on le note ainsi : $\\vec{u}(x, y)$.

- Un repère $(O ; \\vec{i}, \\vec{j})$ du plan est la donnée d'un point $O$ et de deux vecteurs $\\vec{i}$ et $\\vec{j}$ non colinéaires.

Alors, pour tout point $M$, il existe un unique couple de réels $(x, y)$ tel que $\\overrightarrow{O M}=x \\vec{i}+y \\vec{j}$. On dit que $M$ a pour coordonnées $(x, y)$ dans le repère $(O ; \\vec{i}, \\vec{j})$, et on le note ainsi : $M(x, y)$.

- Une base ou un repère est $\\operatorname{dit}(\\mathrm{e})$ orthonormé(e) si les deux vecteurs $\\vec{i}$ et $\\vec{j}$ sont orthogonaux et de norme 1 .


## VIII.2 Produit scalaire et distances

On se place dans un repère orthonormé $(O ; \\vec{i}, \\vec{j})$ du plan.

Soient $\\vec{u}(x, y)$ et $\\vec{v}\\left(x^{\\prime}, y^{\\prime}\\right)$ deux vecteurs du plan.

- $\\vec{u}$ et $\\vec{v}$ sont colinéaires si et seulement si $x y^{\\prime}-x^{\\prime} y=0$.
- Le produit scalaire de $\\vec{u}$ et $\\vec{v}$ est défini par $\\vec{u} \\cdot \\vec{v}=x x^{\\prime}+y y^{\\prime}$.

Deux vecteurs sont orthogonaux si et seulement si leur produit scalaire est nul :

$$
\\vec{u}(x, y) \\text { est orthogonal à } \\vec{v}\\left(x^{\\prime}, y^{\\prime}\\right) \\quad \\Longleftrightarrow \\quad x x^{\\prime}+y y^{\\prime}=0 .
$$

- Distance entre 2 points $A\\left(x_{A}, y_{A}\\right)$ et $B\\left(x_{B}, y_{B}\\right)$ :

$$
A B=\\|\\overrightarrow{A B}\\|=\\sqrt{\\left(x_{B}-x_{A}\\right)^{2}+\\left(y_{B}-y_{A}\\right)^{2}}
$$

## VIII.3 Droites dans le plan

- Toute droite $\\mathcal{D}$ du plan admet une équation cartésienne de la forme $a x+b y+c=0$, où $a, b, c$ sont des réels tels que $(a, b) \\neq(0,0)$. Cela signifie que, pour tout point $M(x, y)$ du plan

$$
M \\in \\mathcal{D} \\Longleftrightarrow a x+b y+c=0 .
$$

Alors $\\vec{u}(-b, a)$ est un vecteur directeur de $\\mathcal{D}$, et $\\vec{n}(a, b)$ est un vecteur normal à $\\mathcal{D}$.

- Toute droite $\\mathcal{D}$ non verticale du plan admet une équation cartésienne de la forme $y=m x+p$
- $\\mathcal{D}$ a alors pour vecteur directeur $\\vec{u}(1, m)$.
- $m$ est le coefficient directeur de $\\mathcal{D}$. Si $\\mathcal{D}$ passe par deux points $A\\left(x_{A}, y_{A}\\right)$ et $B\\left(x_{B}, y_{B}\\right)$, alors $m$ est donné par

$$
m=\\dfrac{y_{B}-y_{A}}{x_{B}-x_{A}}
$$

- $p$ est l'ordonnée à l'origine de $\\mathcal{D}$. C'est l'ordonnée du point en lequel $\\mathcal{D}$ coupe l'axe des ordonnées.

Déterminer une équation cartésienne d'une droite $\\mathcal{D}$

- Si on en connaît un point $A$ et un point vecteur directeur $\\vec{u}$ :

Pour tout $M(x, y), \\quad M \\in \\mathcal{D} \\Leftrightarrow \\overrightarrow{A M}$ et $\\vec{n}$ sont colinéaires

et on applique le critère de colinéarité.

- Si on en connaît un point $A$ et un point vecteur normal $\\vec{n}$ :

$$
\\text { Pour tout } \\begin{aligned}
M(x, y), \\quad M \\in \\mathcal{D} & \\Leftrightarrow \\overrightarrow{A M} \\text { est orthogonal à } \\vec{n} \\\\
& \\Leftrightarrow \\overrightarrow{A M} \\cdot \\vec{n}=0 \\Leftrightarrow \\ldots
\\end{aligned}
$$

## Exercice 22

Pour chacune des droites suivantes, déterminer un vecteur directeur et un vecteur normal.

1. La droite $D$ d'équation : $y=\\dfrac{3}{2} x+1$
2. La droite $D$ d'équation : $5 x+2 y+3=0$.
3. La droite $D$ d'équation : $x=5$.
4. La droite $D$ d'équation : $y=5$.

## Correction de l'exercice 22

1. vecteur directeur de $D: \\vec{u}\\left(1, \\dfrac{3}{2}\\right)$. - vecteur normal de $D$ : on cherche un vecteur $\\vec{n}$ tel que $\\vec{n} \\cdot \\vec{u}=0$ On peut prendre, par exemple, $\\vec{n}\\left(-\\dfrac{3}{2}, 1\\right)$.

Remarque : Étant donné un vecteur $\\vec{u}(x, y)$, le vecteur $\\vec{n}(-y, x)$ est toujours orthogonal à $\\vec{u}$. C'est une façon pratique de trouver un vecteur normal quand on connait un vecteur directeur (et réciproquement)!

2. vecteur normal de $D: \\vec{n}(5,2)$, vecteur directeur de $D: \\vec{u}(-2,5)$.
3. vecteur directeur de $D: \\vec{u}(0,1)$, vecteur normal de $D: \\vec{n}(1,0)$.
4. vecteur directeur de $D: \\vec{u}(1,0)$, vecteur normal de $D: \\vec{n}(0,1)$.

## Exercice 23

On considère les points $A(3,1), B(-1,-1), C(1,3)$.

1. On note $D_{1}$ la droite passant par $A$ et $B$.

a) Déterminer un vecteur directeur de $D_{1}$.

b) Déterminer un vecteur normal de $D_{1}$.

c) En déduire une équation de $D_{1}$ de la forme : $a x+b y+c=0$.

2. On note $D_{2}$ la droite passant par $A$ et $C$.

a) Déterminer le coefficient directeur de $D_{2}$.

b) En déduire une équation de $D_{2}$ de la forme : $y=m x+p$

Correction de l'exercice 23

1. a) vecteur directeur de $D_{1}: \\overrightarrow{u_{1}}=\\overrightarrow{A B}(-4,-2)$.

b) vecteur normal de $D_{1}$ : on cherche un vecteur $\\overrightarrow{n_{1}}$ tel que $\\overrightarrow{n_{1}} \\cdot \\overrightarrow{u_{1}}=0$.

On peut prendre, par exemple, $\\overrightarrow{n_{1}}(2,-4)$.

Remarque : Étant donné un vecteur $\\vec{u}(x, y)$, le vecteur $\\vec{n}(-y, x)$ est toujours orthogonal à $\\vec{u}$. C'est une façon pratique de trouver un vecteur normal quand on connait un vecteur directeur (et réciproquement)!

c) On suit la méthode donnée en rappel :

Pour tout $M(x, y), \\quad M \\in D_{1} \\Leftrightarrow \\overrightarrow{A M}$ est orthogonal à $\\overrightarrow{n_{1}}$

$$
\\begin{aligned}
& \\Leftrightarrow \\overrightarrow{A M} \\cdot \\overrightarrow{n_{1}}=0 \\\\
& \\Leftrightarrow(x-3) \\times 2+(y-1) \\times(-4)=0 \\\\
& \\Leftrightarrow x-2 y-1=0 .
\\end{aligned}
$$

2. a) $m=\\dfrac{y_{C}-y_{A}}{x_{C}-x_{A}}=-1$.

b) Une équation de $D_{2}$ est de la forme : $y=-x+p$.

En remplaçant par les coordonnées de $A$, on obtient $3=-1+p$ d'où $p=4$ D'où l'équation : $y=-x+4$.

## Bilan : études complètes de fonctions

## Exercice 24

Le but de cet exercice est d'étudier la fonction $f$ définie sur $] 0,+\\infty[$ par :

$$
\\text { pour tout } x>0, \\quad f(x)=\\dfrac{(\\ln (x))^{2}}{x}
$$

1. Déterminer $\\lim _{x \\rightarrow 0^{+}} f(x)$.
2. Pour tout $x>0$, on pose $g(x)=\\dfrac{\\ln (x)}{x}$.

a) Montrer que, pour tout $x>0, f(x)=4(g(\\sqrt{x}))^{2}$.

b) En déduire $\\lim _{x \\rightarrow+\\infty} f(x)$.

3. Étudier les variations de $f$.
4. Tracer (sans l'aide de la calculatrice!) l'allure de la courbe représentative de $f$. Donnée numérique : $e^{2} \\approx 7.4$.
5. Soit $a>0$. Calculer $\\int_{a}^{1} f(x) \\mathrm{d} x$.
6. Quelle est la limite de l'intégrale précédente quand $a$ tend vers 0 ? Interpréter graphiquement ce résultat.

## Indications pour l'exercice 24

1. Il n'y a pas de forme indéterminée!
2. a) Se souvenir que $\\ln (\\sqrt{x})=\\dfrac{1}{2} \\ln (x)$.

b) Composition de limites (que vaut $\\lim _{y \\rightarrow+\\infty} g(y)$ ?).

3. On trouve $f^{\\prime}(x)=\\dfrac{(2-\\ln (x)) \\ln (x)}{x^{2}}$ : étudier son signe à l'aide d'un tableau de signe.
4. Faute de calculatrice pour déterminer des valeurs approchées de certaines images, on se contente de chercher à représenter l'allure de la courbe. Quelques petites imprécisions sont donc complètement pardonnables : le principal est de faire figurer correctement toutes les données à disposition (minima ou maxima, limites, variations ...).
5. Pour trouver une primitive de $f$, reconnaître que $f$ est de la forme $u^{\\prime} u^{2}$.
6. La limite de l'intégrale quand $a$ tend vers 0 mesure l'aire d'un certain domaine : lequel? (cela aide de s'appuyer sur un dessin !)

## Correction de l'exercice 24

1. Il n'y a pas de F.I. : en effet, $(\\ln (x))^{2} \\underset{x \\rightarrow 0^{+}}{\\longrightarrow}+\\infty$, donc $f(x) \\underset{x \\rightarrow 0^{+}}{\\longrightarrow}+\\infty$.
2. a Pour montrer une égalité, il est souvent judicieux de partir du membre le plus compliqué (et de le simplifier pour chercher à faire apparaître l'autre membre). On part donc de $4(g(\\sqrt{x}))^{2}$, en écrivant :

$$
4(g(\\sqrt{x}))^{2}=4\\left(\\dfrac{\\ln (\\sqrt{x})}{\\sqrt{x}}\\right)^{2}=4\\left(\\dfrac{\\dfrac{1}{2} \\ln (x)}{\\sqrt{x}}\\right)^{2}=\\dfrac{(\\ln (x))^{2}}{x}=f(x)
$$

b) On a $\\sqrt{x} \\underset{x \\rightarrow+\\infty}{\\longrightarrow}+\\infty$, et $g(y) \\underset{y \\rightarrow+\\infty}{\\longrightarrow} 0$ par croissances comparées.

Ainsi, par composition de limites, $g(\\sqrt{x}) \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$. On en déduit :

$$
f(x)=4(g(\\sqrt{x}))^{2} \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0
$$

3. $f$ est dérivable sur $] 0,+\\infty[$ (comme $\\ln )$, et pour tout $x>0, f(x)=\\dfrac{u(x)}{x}$ avec $u(x)=(\\ln (x))^{2}$. On a

$$
u^{\\prime}(x)=2 \\ln ^{\\prime}(x) \\ln (x)=\\dfrac{2}{x} \\ln (x)
$$

donc

$$
f^{\\prime}(x)=\\dfrac{u^{\\prime}(x) x-u(x)}{x^{2}}=\\dfrac{2 \\ln (x)-(\\ln (x))^{2}}{x^{2}}=\\dfrac{(2-\\ln (x)) \\ln (x)}{x^{2}}
$$

Le dénominateur de cette fraction étant toujours positif, le signe de $f^{\\prime}(x)$ est égal à celui de $(2-\\ln (x)) \\ln (x)$. Or, en utilisant le fait que exp est strictement croissante sur $\\mathbb{R}$

$$
2-\\ln (x)>0 \\Leftrightarrow \\ln (x)<2 \\Leftrightarrow e^{\\ln (x)}<e^{2} \\Leftrightarrow x<e^{2}
$$

On en déduit, le signe de $f^{\\prime}(x)$ puis les variations de $f$ :

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-35.jpg?height=489&width=1130&top_left_y=1486&top_left_x=241)

4. On déduit des questions précédentes l'allure de $f$ (le domaine hachuré sera expliqué à la dernière question) :

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-35.jpg?height=746&width=1209&top_left_y=235&top_left_x=1570)

5. Il faut chercher une primitive de $f$. On se souvient pour cela que, pour tout $x>0$, $\\ln ^{\\prime}(x)=\\dfrac{1}{x}$. Ainsi :

$$
f(x)=\\dfrac{(\\ln (x))^{2}}{x}=\\ln ^{\\prime}(x)(\\ln (x))^{2}
$$

On reconnaît une dérivée de composée de la forme $u^{\\prime} u^{2}$ ! Ainsi :

$$
\\int_{a}^{1} f(x) \\mathrm{d} x=\\left[\\dfrac{1}{3}(\\ln (x))^{3}\\right]_{a}^{1}=\\dfrac{1}{3}(\\ln (1))^{3}-\\dfrac{1}{3}(\\ln (a))^{3}=-\\dfrac{1}{3}(\\ln (a))^{3}
$$

6. $\\ln (a) \\underset{a \\rightarrow 0^{+}}{\\longrightarrow}-\\infty$ donc

$$
\\int_{a}^{1} f(x) \\mathrm{d} x=-\\dfrac{1}{3}(\\ln (a))^{3} \\underset{a \\rightarrow 0^{+}}{\\longrightarrow}+\\infty
$$

Graphiquement, cette limite mesure l'aire du domaine du plan délimité par l'axe des abscisses, l'axe des ordonnées, la courbe de $f$ et la droite d'équation $x=1$ (domaine hachuré sur le dessin). Ce domaine est donc d'aire infinie! Exercice 25 d'après un exercice de bac 2018

Soit $f$ et $g$ les fonctions définies sur $\\mathbb{R}$ par

$$
f(x)=e^{-x}(-\\cos (x)+\\sin (x)+1) \\text { et } g(x)=-e^{-x} \\cos (x) .
$$

1. Déterminer $\\lim _{x \\rightarrow+\\infty} f(x)$.
2. Étudier les variations de $f$ sur $[-\\pi, \\pi]$.
3. Étudier la position relative des courbes de $f$ et de $g$ sur $\\mathbb{R}$ : en quels points ces courbes se rencontrent-elles? Hors de ces points d'intersection, quelle courbe se situe au-dessus de l'autre?

## Indications pour l'exercice 25

1. Les fonctions cos et sin n'ont pas de limite en $+\\infty$, donc la limite de $f$ n'est pas calculable directement. Mais en encadrant cos et sin entre -1 et 1 , il doit y avoir moyen de contourner le problème ...
2. On trouve après calculs que $f^{\\prime}(x)=e^{-x}(2 \\cos (x)-1)$.

Pour trouver le signe de $f^{\\prime}(x)$, il reste à faire un peu de trigonométrie (ne pas hésiter à s'appuyer sur un cercle trigonométrique!).

3. Étudier le signe de $f(x)-g(x)$.

Correction de l'exercice 25

1. Pour tout $x \\in \\mathbb{R},-1 \\leqslant \\cos (x) \\leqslant 1$, donc $-1 \\leqslant-\\cos (x) \\leqslant 1$.

On a aussi $-1 \\leqslant \\sin (x) \\leqslant 1$; en additionnant ces deux inégalités :

$$
-2 \\leqslant-\\cos (x)+\\sin (x) \\leqslant 2
$$

donc

$$
-1 \\leqslant-\\cos (x)+\\sin (x)+1 \\leqslant 3
$$

En multipliant par $e^{-x}$, qui est positif :

$$
-e^{-x} \\leqslant f(x) \\leqslant 3 e^{-x}
$$

Les membres de gauche et de droite tendent vers 0 quand $x$ tend vers $+\\infty$ (car $\\left.e^{y} \\underset{y \\rightarrow-\\infty}{\\longrightarrow} 0\\right)$. Par encadrement, on en déduit que $f(x) \\underset{x \\rightarrow+\\infty}{\\longrightarrow} 0$

2. $f$ est dérivable sur $\\mathbb{R}$ (car l'exponentielle, le cosinus et le sinus le sont), et pour tout $x \\in \\mathbb{R}$

$$
\\begin{aligned}
f^{\\prime}(x) & =-e^{-x}(-\\cos (x)+\\sin (x)+1)+e^{-x}(\\sin (x)+\\cos (x)) \\\\
& =e^{-x}(2 \\cos (x)-1) .
\\end{aligned}
$$

L'exponentielle étant toujours strictement positive, pour tout $x \\in \\mathbb{R}$,

$$
f^{\\prime}(x)>0 \\Leftrightarrow 2 \\cos (x)-1>0 \\Leftrightarrow \\cos (x)>\\dfrac{1}{2}
$$

En faisant un dessin sur le cercle trigonométrique, on voit que, sur $[-\\pi, \\pi]$, cette condition est vérifiée pour $x \\in]-\\dfrac{\\pi}{3}, \\dfrac{\\pi}{3}[$.

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-36.jpg?height=328&width=1119&top_left_y=90&top_left_x=1607)

3. Étudier la position relative des courbes de $f$ et de $g$ revient à déterminer le signe de $f(x)-g(x)$. Or, pour tout $x \\in \\mathbb{R}$,

$$
f(x)-g(x)=\\underbrace{e^{-x}}_{\\geqslant 0}(\\overbrace{\\geqslant 0}^{\\underbrace{\\geqslant-1}_{\\sin (x)}+1}) \\geqslant 0 .
$$

Ainsi, pour tout $x \\in \\mathbb{R}, f(x) \\geqslant g(x)$ : la courbe de $f$ est toujours située au-dessus de celle de $g$. Déterminons les points où ces deux courbes se rencontrent :

$$
\\begin{aligned}
f(x)=g(x) & \\Leftrightarrow f(x)-g(x)=0 \\\\
& \\Leftrightarrow e^{-x}(\\sin (x)+1)=0 \\\\
& \\Leftrightarrow \\sin (x)+1=0 \\quad\\left(\\operatorname{car} e^{-x} \\neq 0\\right) \\\\
& \\Leftrightarrow \\sin (x)=-1 \\\\
& \\Leftrightarrow \\text { il existe } k \\in \\mathbb{Z} \\text { tel que } x=-\\dfrac{\\pi}{2}+2 k \\pi .
\\end{aligned}
$$

![](https://cdn.mathpix.com/cropped/2023_06_03_6a40cc8b57791b5aa622g-36.jpg?height=810&width=1205&top_left_y=1169&top_left_x=1568)

