{{ chapitre(3, "Puissances", "Rappel N°")}}



## Calculs 
![Placeholder](./IMG/gaston.png){ align=right width=300}


!!! note "Petits rappels"

	Étant donné  un réel  $x \in \mathbb{R}$  et un entier  non nul  $n \in
	\mathbb{N}$, on pose :
	
	$$
	x^{n}=\underbrace{x \times \ldots \times x}_{n \text { fois }}
	$$
	
	et, par convention, $x^{0}=1$. 
	
	Enfin,  si $x  \neq 0$, on  définit les
	puissances entières négatives de $x$ en posant:
	
	$x^{-n}=\frac{1}{x^{n}}\left(\right.$           en          particulier
	$x^{-1}=\frac{1}{x}$ ).
	
	Alors, pour tous entiers $n, p \in \mathbb{Z}$, on a :
	
	$(x y)^{n}=x^{n} y^{n}, \quad\left(\frac{x}{y}\right)^{n}=\frac{x^{n}}{y^{n}}, \quad x^{n} x^{p}=x^{n+p}, \quad\left(x^{n}\right)^{p}=x^{n p}, \quad \frac{x^{p}}{x^{n}}=x^{p-n}$



!!! {{ exercice()}}

    === "Énoncé"
		
		Dans chaque cas,  donner le résultat sous la  forme d'une puissance
		de 10.
		
	    1. $10^{5} \cdot 10^{3}$
		1. $\dfrac{10^{5}}{10^{3}}$
		1. $\dfrac{10^{-5}}{10^{-3}}$
		1. $\left(10^{5}\right)^{3}$
		1.   $\dfrac{\left(10^{5}  \cdot   10^{-3}\right)^{5}}{\left(10^{-5}
		   \cdot 10^{3}\right)^{-3}}$ 
	    1. $\dfrac{\left(10^{3}\right)^{-5} \cdot 10^{5}}{10^{3} \cdot 10^{-5}}$

	=== "Corrigé"

         oooohhhh


!!! {{ exercice()}}

    === "Énoncé"

        Dans chaque cas,  donner le résultat sous la  forme d'une puissance
        de 10.
		
		1. $0,001 $
		1. $\dfrac{0,01^{2}}{0,1^{5}}$
		1. $\dfrac{1000 \cdot 0,01^{3}}{0,1^{3} \cdot 0,01^{2}}$
		1. $10^{3} \cdot 0,01^{3} $
		1. $0,001^{-2} \cdot 1000^{2}$
		1. $\dfrac{\left(0,01^{3}\right)^{-2}}{0,1^{-3} \cdot\left(100^{-2}\right)^{-3}}$

	=== "Corrigé"

         1. oooohhhh
		 1.         $\quad          10^{3}         \cdot         0,01^{3}=10^{3}
		    \cdot\left(10^{-2}\right)^{3}=10^{3} \cdot 10^{-6}=10^{-3}$
			
		 1. $\quad \frac{0,01^{2}}{0,1^{5}}=\frac{10^{-4}}{10^{-5}}=10^{1}$
		 1.  $\quad  0,001^{-2} \cdot  1000^{2}=\left(10^{-3}\right)^{-2}  \cdot
		    10^{6}=10^{12}$
		
		 1. $\quad \frac{1000 \cdot 0,01^{3}}{0,1^{3} \cdot
		 0,01^{2}}=\frac{10^{3}  \cdot  10^{-6}}{10^{-3} \cdot  10^{-4}}=10^{-3}
		 \cdot 10^{7}=10^{4}$
		 
		 1. $\frac{\left(0,01^{3}\right)^{-2}}{0,1^{-3} \cdot\left(100^{-2}\right)^{-3}}=\frac{\left(10^{-6}\right)^{-2}}{10^{3} \cdot\left(10^{-4}\right)^{-3}}=\frac{1}{10^{3}}=10^{-3}$



!!! {{ exercice()}}

    === "Énoncé"

        Dans chaque cas, donner le résultat sous la forme sous la forme
        $a^{n}$ avec $a$ et $n$ deux entiers relatifs. 
		
		1. $3^{4} \cdot 5^{4}$
		1. $\dfrac{2^{5}}{2^{-2}}$
		1. $\dfrac{6^{5}}{2^{5}}$
		1. $\left(5^{3}\right)^{-2}$
		1. $(-7)^{3} \cdot(-7)^{-5}$
		1. $\dfrac{\left(30^{4}\right)^{7}}{2^{28} \cdot 5^{28}}$

	=== "Corrigé"

         Quand même....


!!! {{ exercice()}}

    === "Énoncé"

        Dans  chaque cas,  donner le  résultat sous  la forme  $2^{n} \cdot
        3^{p}$, où $n$ et $p$ sont deux entiers relatifs. 
		
		1. $\dfrac{2^{3} \cdot 3^{2}}{3^{4} \cdot 2^{8} \cdot 6^{-1}}$
		1. $\dfrac{3^{22}+3^{21}}{3^{22}-3^{21}}$
		1. $2^{21}+2^{22}$
		1.    $\dfrac{\left(3^{2}   \cdot(-2)^{4}\right)^{8}}{\left((-3)^{5}
		   \cdot 2^{3}\right)^{-2}}$ 

	=== "Corrigé"

         1.   $\quad   \frac{2^{3}   \cdot  3^{2}}{3^{4}   \cdot   2^{8}   \cdot
            6^{-1}}=\frac{2^{3}  \cdot  3^{2}}{3^{4}  \cdot 2^{8}  \cdot  2^{-1}
            \cdot     3^{-1}}=\frac{2^{3}     \cdot     3^{2}}{3^{4-1}     \cdot
            2^{8-1}}=\frac{2^{3} \cdot  3^{2}}{3^{3} \cdot  2^{7}}=2^{3-7} \cdot
            3^{2-3}=2^{-4} \cdot 3^{-1}$.
		 1.   On  factorise   $:   2^{21}+2^{22}=2^{21}+2^{21}  \cdot   2=2^{21}
		    \cdot(1+2)=2^{21} \cdot 3$.
		 1.    On    factorise   au    numérateur   et   au    dénominateur   $:
		     \frac{3^{22}+3^{21}}{3^{22}-3^{21}}=\frac{(3+1) \cdot 3^{21}}{(3-1)
		     \cdot 3^{21}}=\frac{4}{2}=2$.
		 1. On simplifie en appliquant les règles habituelles de calcul avec les
		    puissances, et  en exploitant  le fait que  $(-a)^{n}=a^{n}$ lorsque
		    $n$          est          pair         $:          \frac{\left(3^{2}
		    \cdot(-2)^{4}\right)^{8}}{\left((-3)^{5}                       \cdot
		    2^{3}\right)^{-2}}=\frac{3^{16}    \cdot    2^{32}}{3^{-10}    \cdot
		    2^{-6}}=2^{38} \cdot 3^{26}$.
			

!!! {{ exercice()}}

    === "Énoncé"

        Dans chaque cas, simplifier au maximum.
		
		1. $\dfrac{8^{17} \cdot 6^{-6}}{9^{-3} \cdot 2^{42}}$
		1. $\dfrac{12^{-2} \cdot 15^{4}}{25^{2} \cdot 18^{-4}}$
		1. $\dfrac{55^{2} \cdot 121^{-2} \cdot 125^{2}}{275 \cdot 605^{-2} \cdot 25^{4}}$
		1. $\dfrac{36^{3} \cdot 70^{5} \cdot 10^{2}}{14^{3} \cdot 28^{2} \cdot 15^{6}}$ 

	=== "Corrigé"

         1. On fait apparaitre les facteurs premiers 2 et $3: \frac{8^{17} \cdot
            6^{-6}}{9^{-3} \cdot 2^{42}}=\frac{2^{3 \cdot 17} \cdot 2^{-6} \cdot
            3^{-6}}{3^{2    \cdot(-3)}   \cdot    2^{42}}=\frac{2^{51-6}   \cdot
            3^{-6}}{3^{-6} \cdot 2^{42}}=2^{45-42}=2^{3}=8$.
		 1. Avec  les facteurs  premiers 5 et  $11: \frac{55^{2}  \cdot 121^{-2}
		    \cdot  125^{2}}{275  \cdot  605^{-2}  \cdot  25^{4}}=\frac{(5  \cdot
		    11)^{2}                                \cdot\left(11^{2}\right)^{-2}
		    \cdot\left(5^{3}\right)^{2}}{5^{2} \cdot  11 \cdot\left(11^{2} \cdot
		    5\right)^{-2}     \cdot\left(5^{2}\right)^{4}}=\frac{5^{8}     \cdot
		    11^{-2}}{5^{8} \cdot 11^{-3}}=11$.
		 1. On  fait apparaitre les  facteurs premiers 2,3 et  $5: \frac{12^{-2}
		    \cdot  15^{4}}{25^{2}  \cdot  18^{-4}}=\frac{\left(2^{2}\right)^{-2}
		    \cdot 3^{-2}  \cdot 3^{4} \cdot  5^{4}}{\left(5^{2}\right)^{2} \cdot
		    2^{-4} \cdot\left(3^{2}\right)^{-4}}=\frac{2^{-4}  \cdot 3^{2} \cdot
		    5^{4}}{2^{-4} \cdot 3^{-8} \cdot 5^{4}}=3^{10}$
		 1.  Même  méthode que précédemment  : $\frac{36^{3} \cdot  70^{5} \cdot
		     10^{2}}{14^{3} \cdot  28^{2} \cdot 15^{6}}=\frac{2^{6}  \cdot 3^{6}
		     \cdot 2^{5} \cdot 5^{5} \cdot  7^{5} \cdot 2^{2} \cdot 5^{2}}{2^{3}
		     \cdot   7^{3}   \cdot  2^{4}   \cdot   7^{2}   \cdot  3^{6}   \cdot
		     5^{6}}=\frac{2^{13}  \cdot  3^{6}  \cdot 5^{7}  \cdot  7^{5}}{2^{7}
		     \cdot 3^{6} \cdot 5^{6} \cdot 7^{5}}=2^{6} \cdot 5$.
			 

!!! {{ exercice()}}

    === "Énoncé"

        Dans chaque cas, simplifier au  maximum l'expression en fonction du
        réel $x$.
		
		1. $\dfrac{x}{x-1}-\dfrac{2}{x+1}-\dfrac{2}{x^{2}-1}$.
		1. $\dfrac{x^{2}}{x^{2}-x}+\dfrac{x^{3}}{x^{3}+x^{2}}-\dfrac{2 x^{2}}{x^{3}-x}$
		1. $\dfrac{2}{x+2}-\dfrac{1}{x-2}+\dfrac{8}{x^{2}-4}$
		1. $\dfrac{1}{x}+\dfrac{x+2}{x^{2}-4}+\dfrac{2}{x^{2}-2 x}$

	=== "Corrigé"

         1.  On   met  au  même   dénominateur  les  deux   premières  écritures
            fractionnaires   :  $\frac{x}{x-1}-\frac{2}{x+1}-\frac{2}{x^{2}-1}=$
            $\frac{x(x+1)-2(x-1)}{(x-1)(x+1)}-\frac{2}{x^{2}-1}=\frac{x^{2}+x-2
            x+2}{(x+1)(x-1)}-\frac{2}{(x+1)(x-1)}=\frac{x^{2}-x}{(x+1)(x-1)}=\frac{x}{x+1}$
		 1.                   Même                   méthode                  $:
		     \frac{2}{x+2}-\frac{1}{x-2}+\frac{8}{x^{2}-4}=\frac{2(x-2)-(x+2)}{(x+2)(x-2)}+\frac{8}{(x+2)(x-2)}=\frac{2
		     x-4}{(x+2)(x-2)}=\frac{1}{x-2}$
		 1.  On commence par simplifier les puissances superflues, puis c'est le
		     même          principe          que         précédemment          :
		     $\frac{x^{2}}{x^{2}-x}+\frac{x^{3}}{x^{3}+x^{2}}-\frac{2
		     x^{2}}{x^{3}-x}=\frac{x}{x-1}+\frac{x}{x+1}-\frac{2
		     x}{x^{2}-1}=\frac{x(x+1+x-1)}{(x-1)(x+1)}-\frac{2
		     x}{(x+1)(x-1)}=\frac{2 x^{2}}{(x+1)(x-1)}=\frac{2 x}{x+1}$
		 1.                    $\frac{1}{x}+\frac{x+2}{x^{2}-4}+\frac{2}{x^{2}-2
		    x}=\frac{1}{x}+\frac{x+2}{(x+2)(x-2)}+\frac{2}{x(x-2)}=\frac{1}{x}+\frac{1}{x-2}+\frac{2}{x(x-2)}=\frac{x-2}{x(x-2)}+\frac{2}{x(x-2)}=\frac{2}{x-2}$ 


	
## Réponses mélangées

$${\small
\begin{aligned}
& 3^{28} \quad 11 \quad 10^2 \quad 8 \quad \dfrac{2x}{x+1} \quad 15^4 \quad
(-7)^{-2} \quad \dfrac{x}{x+1}\\
& 2^{38}×3^{26}\quad  2^7 \quad  2^6×5
\quad 2  \quad 2^{-4}×3^{-1}  \quad 10^{-8}\quad  10^{15} \quad  10^4 \quad
\dfrac{2}{x-2} \\
&3^{10} \quad  5^{-6} \quad  3^5 \quad  2^{21}×3 \quad
10^{-2} \quad \dfrac{1}{x-2} \quad 10^8\\
\end{aligned}
}$$











## QCM

!!! {{ exercice()}}

    === "QCM"
	      
	     $a, b, x, C$ représentent des réels, $n$ et $k$ représentent des entiers.
	     Cliquez sur vos propositions à toutes les questions puis validez.
		 
		 {{ multi_qcm(
		 ["$ a^{5} a^{3}=$",["$a^{5^{3}} $", "$a^{15}$", "$a^{8}$"],[3]],
		 ["$a^{2} b^{3}= $",["$(a b)^{2} b $", " $(a b)^{5}$", "$(a b)^{6}$"],[1]],
		 ["$ \\left(a^{2}\\right)^{n}=$",["$ a^{2 n}$", " $a^{2+n}$", "$a^{2^{n}}$"],[1]],
		 ["$\\left(3^{n}\\right)^{2}= $",["$3^{n^{2}} $", "$6^{n}$", "$9^{n}$"],[3]],
		 ["$\\left(a^{n^{2}}\\right)^{3}=$",[" $a^{3 n^{2}}$", "$a^{n^{8}}$", "$a^{n^{6}}$"],[1]],
		 ["$\\dfrac{a^{n^{2}}}{a^{n}}=$",["$a^{n}$", "$a^{n^{2}-n}$", " $a^{2 n}$"],[2]],
		 [" $a^{3 n}\\left(a^{n}\\right)^{2}=$",["$a^{5 n}$", "$a^{6 n}$", "$a^{3 n^{3}}$"],[1]],
		 ["$2^{-2 k} \\times 3^{k}=$",["$\\left(\\dfrac{3}{4}\\right)^{k}$", "$\\left(\\dfrac{3}{2}\\right)^{k}$", "$\\left(\\dfrac{3}{2}\\right)^{2 k}$"],[1]],
		 ["$3^{2 k+1} \\times 2^{-k}=$",["$3\\left(\\dfrac{9}{2}\\right)^{-k}$", "$9\\left(\\dfrac{3}{2}\\right)^{k}$", "$3\\left(\\dfrac{9}{2}\\right)^{k}$"],[3]],
		 ["$2\\left(2 \\times 3^{n}-3 \\times 2^{n}\\right)=$",["$\\left(4 \\times 12^{n}-6 \\times 4^{n}\\right)$", "$\\left(4 \\times 3^{n}-3 \\times 4^{n}\\right)$", "$\\left(4 \\times 3^{n}-3 \\times 2^{n+1}\\right)$"],[3]],
		 [" $2^{n}+2^{n}=$",["$4^{n}$", "$2^{n+1}$", "$2^{2 n}$"],[2]],
		 ["$(-1)^{n+2}=$",["$(-1)^{n+1}$", "$-(-1)^{n}$", "$(-1)^{n}$"],[3]],
		 ["$\\dfrac{1}{(-1)^{n}}=$",["$(-1)^{n+1}$", "$-(-1)^{n}$", "$(-1)^{n}$"],[3]],
		 ["$(-1)^{n+1}+(-1)^{n}=$",["0", "1", "2"],[1]],
		 ["$(-2)^{2 n+1}=$",["$2^{2 n+1}$", "$(-4)^{n+1}$", "$-2 \\times 4^{n}$"],[3]]
	     )}}

    === "Réponses"
	
	     Cliquez et vous saurez tout de suite si vous avez vu juste :

		 1. $a^{5} a^{3}= $
		    {{ qcm(["$a^{5^{3}} $", "$a^{15}$ ", "$a^{8}$ "],[3 ], shuffle=True)}}
		 1. $a^{2} b^{3}= $
		    {{ qcm(["$(a b)^{2} b $", " $(a b)^{5}$", "$(a b)^{6}$" ],[1 ], shuffle=True)}}
		 1. $\left(a^{2}\right)^{n}= $
		    {{ qcm(["$ a^{2 n}$", " $a^{2+n}$", "$a^{2^{n}}$" ],[1 ], shuffle=True)}}
		 1. $\left(3^{n}\right)^{2}= $
		    {{ qcm(["$3^{n^{2}} $", "$6^{n}$", "$9^{n}$" ],[3 ], shuffle=True)}}
		 1. $\left(a^{n^{2}}\right)^{3}=$
		    {{ qcm([" $a^{3 n^{2}}$", "$a^{n^{8}}$", "$a^{n^{6}}$"],[1] , shuffle=True)}}
		 1. $\dfrac{a^{n^{2}}}{a^{n}}=$
		    {{ qcm(["$a^{n}$", "$a^{n^{2}-n}$", " $a^{2 n}$"],[2] , shuffle=True)}}
		 1. $a^{3 n}\left(a^{n}\right)^{2}=$
		    {{ qcm( ["$a^{5 n}$", "$a^{6 n}$", "$a^{3 n^{3}}$"],[1], shuffle=True)}}
		 1. $2^{-2 k} \times 3^{k}=$
		    {{ qcm(["$\\left(\\dfrac{3}{4}\\right)^{k}$", "$\\left(\\dfrac{3}{2}\\right)^{k}$", "$\\left(\\dfrac{3}{2}\\right)^{2 k}$"],[1] , shuffle=True)}}
		 1. $3^{2 k+1} \times 2^{-k}=$
		    {{ qcm(["$3\\left(\\dfrac{9}{2}\\right)^{-k}$", "$9\\left(\\dfrac{3}{2}\\right)^{k}$", "$3\\left(\\dfrac{9}{2}\\right)^{k}$"],[3] , shuffle=True)}}
		 1. $2\left(2 \times 3^{n}-3 \times 2^{n}\right)=$
		    {{ qcm( ["$\\left(4 \\times 12^{n}-6 \\times 4^{n}\\right)$", "$\\left(4 \\times 3^{n}-3 \\times 4^{n}\\right)$", "$\\left(4 \\times 3^{n}-3 \\times 2^{n+1}\\right)$"],[3], shuffle=True)}}
		 1.  $2^{n}+2^{n}=$
		    {{ qcm( ["$4^{n}$", "$2^{n+1}$", "$2^{2 n}$"],[2], shuffle=True)}}
		 1. $(-1)^{n+2}=$
		    {{ qcm(["$(-1)^{n+1}$", "$-(-1)^{n}$", "$(-1)^{n}$"],[3] , shuffle=True)}}
		 1. $\dfrac{1}{(-1)^{n}}=$
		    {{ qcm(["0", "1", "2"],[1] , shuffle=True)}}
		 1. $(-1)^{n+1}+(-1)^{n}=$
		    {{ qcm( ["0", "1", "2"],[1], shuffle=True)}}
		 1. $(-2)^{2 n+1}=$
		    {{ qcm(["$2^{2 n+1}$", "$(-4)^{n+1}$", "$-2 \\times 4^{n}$"],[3] , shuffle=True)}}
		 

	=== "Réponses et commentaires"

         1. 
		 2. 
		 3. 
		 4. 
		 5. 
		 6. 
		 7. 
		 8. A, $\operatorname{car}  2^{-2 k} \times 3^{k}=\frac{3^{k}}{2^{2
		    k}}=\frac{3^{k}}{\left(2^{2}\right)^{k}}=\left(\frac{3}{4}\right)^{k}$.
		 9.    $\mathrm{C},    \operatorname{car}    3^{2    k+1}    \times
		    2^{-k}=\frac{3^{2                   k}                   \times
		    3}{2^{k}}=\frac{\left(3^{2}\right)^{k}                   \times
		    3}{2^{k}}=3\left(\frac{9}{2}\right)^{k}$ 
         10. C, car $2 \times 2^{n}=2^{n+1}$.
         11. $\mathrm{B}$, car $2^{n}+2^{n}=2 \times 2^{n}=2^{n+1}$
         12. C, car $(-1)^{n+2}=(-1)^{2} \times(-1)^{n}=1 \times(-1)^{n}=(-1)^{n}$. 
         13. $\mathrm{C}: \frac{1}{(-1)^{n}}=\frac{1 \times(-1)^{n}}{(-1)^{n} \times(-1)^{n}}=\frac{(-1)^{n}}{(-1)^{2 n}}=(-1)^{n} \cdot\left(2 n\right.$ est pair donc $\left.(-1)^{2 n}=1\right)$ Alternativement, on peut utiliser le fait que $(-1)^{n}$ vaut 1 si $n$ est pair et -1 si $n$ est impair ; par conséquent, $\frac{1}{(-1)^{n}}$ vaut $\frac{1}{1}=1$ si $n$ est pair et $\frac{1}{-1}=-1$ si $n$ est impair. On en déduit que $\frac{1}{(-1)^{n}}=(-1)^{n}$, puisque ces deux expressions prennent toujours la même valeur quelle que soit la parité de $n$.
         14. $\mathrm{A}$, $\operatorname{car}(-1)^{n+1}+(-1)^{n}=(-1) \times(-1)^{n}+(-1)^{n}=-(-1)^{n}+(-1)^{n}=0$.
         15. C, car $(-2)^{2 n+1}=(-2) \times(-2)^{2 n}=-2 \times\left((-2)^{2}\right)^{n}=-2 \times 4^{n}$.
		 

