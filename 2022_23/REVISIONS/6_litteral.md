{{ chapitre(4, "Calcul littéral", "Rappel N°")}}


Dans  cette section,  on  tâchera  de mener  les  calculs  avec le  minimum
d'étapes. La variable $x$ représente un nombre réel.


## Développer, réduire et ordonner
![Placeholder](./IMG/tintin5.png){ align=right width=300}


!!! note "Petits rappels"

	Pour tous réels $a$ et $b$ :

	$$
	(a+b)^{2}=a^{2}+2 a b+b^{2}, \quad(a-b)^{2}=a^{2}-2 a b+b^{2}, \quad a^{2}-b^{2}=(a+b)(a-b) .
	$$

	Remarques :

	- La formule pour $(a+b)^{2}$ se généralise au cas d'une somme de trois termes ou plus; il faut juste tenir compte de tous les doubles produits possibles :

	$$
	(a+b+c)^{2}=a^{2}+b^{2}+c^{2}+2 a b+2 b c+2 a c
	$$

	- On voit en première année de classe préparatoire la formule du binôme de Newton, qui donne une expression générale pour $(a+b)^{n}$ :

	$$
	(a+b)^{n}=\sum_{k=0}^{n}\left(\begin{array}{l}
	n \\
	k
	\end{array}\right) a^{k} b^{n-k}
	$$

	Vous pouvez déjà retenir ce que donne cette formule dans le cas $n=3$ :

	$$
	(a+b)^{3}=a^{3}+3 a^{2} b+3 a b^{2}+b^{3}
	$$




!!! {{ exercice()}}

    === "Énoncé"
		
		Développer, réduire et ordonner les expressions suivantes selon les
		puissances décroissantes de $x$. 
		
		1. $\left(2 x-\dfrac{1}{2}\right)^{3}$
		1. $(x+1)^{2}(x-1)\left(x^{2}+x+1\right)$
		1. $(x-1)^{3}\left(x^{2}+x+1\right)$
		1. $(x-1)^{2}(x+1)\left(x^{2}+x+1\right)$
		1. $(x+1)^{2}(x-1)\left(x^{2}-x+1\right)$
		1. $\left(x^{2}+x+1\right)\left(x^{2}-x+1\right)$

	=== "Corrigé"

         1.   On utilise  directement l'identité  remarquable $(a+b)^{3}=a^{3}+3
             a^{2} b+3 a b^{2}+b^{3}$.
		 2.  On  peut  écrire:  $(x-1)^{3}\left(x^{2}+x+1\right)=\left(x^{3}-3
		    x^{2}+3 x-1\right)\left(x^{2}+x+1\right)$
			
			$=x^{5}-2 x^{4}+x^{3}-x^{2}+2
		    x-1$. Pour être  "efficace", il suffit de  rechercher directement le
		    coefficient du  terme d'un  degré donné (sachant  que $\left.\left(a
		    x^{n}\right)\left(b x^{p}\right)=a  b x^{n+p}\right)$.  Par exemple,
		    dans l'expression  finale et en utilisant  l'étape intermédiaire, le
		    coefficient  du terme  de degré  2 est  donné par  $(-3) \times  1+3
		    \times 1+(-1)  \times 1=-1$. Ici, l'étape  intermédiaire n'étant pas
		    compliquée (à effectuer  et à retenir), on  peut (éventuellement) se
		    passer de l'écrire.
		 3.  Connaissant  les  identités  remarquables  $(x-1)(x+1)=x^{2}-1$  et
		    $(x+1)\left(x^{2}-x+1\right)=x^{3}+1$,    on    a    facilement    :
		    $(x+1)^{2}(x-1)\left(x^{2}-x+1\right)=[(x+1)(x-1)]\left[(x+1)\left(x^{2}-x+1\right)\right]$
			
			$=\left(x^{2}-1\right)\left(x^{3}+1\right)=x^{5}-x^{3}+x^{2}-1.$
		    Que pensez-vous de la nécessité d'écrire les étapes intermédiaires? 
		 4.   On calcule $(x+1)^{2}(x-1)\left(x^{2}+x+1\right)=\left(x^{2}+2
		     x+1\right)\left(x^{3}-1\right)$
			 
			 $=x^{5}+2 x^{4}+x^{3}-x^{2}-2 x-1$.
		 5.                              On                              calcule
		    $(x-1)^{2}(x+1)\left(x^{2}+x+1\right)=\left(x^{2}-1\right)\left(x^{3}-1\right)$
			
			$=x^{5}-x^{3}-x^{2}+1$.
			


!!! {{ exercice()}}

    === "Énoncé"

        Développer,  réduire  et   ordonner  les  expressions  polynomiales
        suivantes selon les puissances croissantes de $x$. 

        1. $(x-2)^{2}\left(-x^{2}+3 x-1\right)-(2 x-1)\left(x^{3}+2\right)$

		1. $(2 x+3)(5 x-8)-(2 x-4)(5 x-1)$

		1. $\left((x+1)^{2}(x-1)\left(x^{2}-x+1\right)+1\right) x-x^{6}-x^{5}+2$

		1. $(x+1)(x-1)^{2}-2\left(x^{2}+x+1\right)$

		1. $\left(x^{2}+\sqrt{2} x+1\right)\left(1-\sqrt{2} x+x^{2}\right)$

		1. $\left(x^{2}+x+1\right)^{2}$

	=== "Corrigé"

         RAS



## Factoriser




!!! {{ exercice("Petite mise en jambe.")}}

    === "Énoncé"

        Factoriser les expressions polynomiales de la variable réelle $x$ suivantes. 

		1. $-(6 x+7)(6 x-1)+36 x^{2}-49$
		1. $25-(10 x+3)^{2}$
		1. $(6 x-8)(4 x-5)+36 x^{2}-64$
		1. $(-9 x-8)(8 x+8)+64 x^{2}-64$ 

	=== "Corrigé"

         1.  Une  identité remarquable  fait  apparaître  le facteur  commun  $6
            x+7$.  On  calcule alors  $-(6  x+7)(6  x-1)+36 x^{2}-49=-(6  x+7)(6
            x-1)+(6 x)^{2}-7^{2}=(6 x+7)[-(6 x-1)+6 x-7]=-6(6 x+7)$
			
		 2.  On calcule $25-(10 x+3)^{2}=5^{2}-(10 x+3)^{2}=(10 x+8)(-10 x+2)=4(5 x+4)(-5 x+1)$.


!!! {{ exercice("À l'aide de la forme canonique.")}}

    === "Énoncé"

        Donner la forme canonique puis factoriser les polynômes de degré deux suivants.
        
		On  rappelle  que  la  forme  canonique  de  $a  x^{2}+b  x+c$  est
        $a\left[\left(x+\dfrac{b}{2      a}\right)^{2}-\dfrac{b^{2}-4     a
        c}{4a^{2}}\right]$ (où $\left.a \neq 0\right)$.
		
		1. $x^{2}-2 x+1$
		1. $3 x^{2}+7 x+1$
		1. $x^{2}+4 x+4$
		1. $2 x^{2}+3 x-28$
		1. $x^{2}+3 x+2$
		1. $-5 x^{2}+6 x-1$

	=== "Corrigé"

         1. RAS
		 2. RAS
		 3.  La forme canonique est $\left(x+\frac{3}{2}\right)^{2}-\frac{1}{4}$. On en déduit la factorisation à l'aide de l'identité remarquable $a^{2}-b^{2}=$

!!! {{ exercice("Avec plusieurs variables.")}}

    === "Énoncé"

        Factoriser sur $\mathbb{R}$  les expressions polynomiales suivantes
        dont les variables représentent des nombres réels. 
		
		1. $(x+y)^{2}-z^{2}$
		1. $x y-x-y+1$
		1. $x^{2}+6 x y+9 y^{2}-169 x^{2}$
		1. $x^{3}+x^{2} y+2 x^{2}+2 x y+x+y$
		1. $x y+x+y+1$
		1. $y^{2}\left(a^{2}+b^{2}\right)+16 x^{4}\left(-a^{2}-b^{2}\right)$.

	=== "Corrigé"

         1. RAS
		 2.   On   calcule  $x^{2}+6  x  y+9   y^{2}-169  x^{2}=(x+3  y)^{2}-(13
		     x)^{2}=(14 x+3 y)(-12 x+3 y)$.
		 3. RAS
		 4. RAS
		 5.   On calcule  $x^{3}+x^{2}  y+2  x^{2}+2 x  y+x+y=(x+y)\left(x^{2}+2
		     x+1\right)=(x+y)(x+1)^{2}$.
		 
		 

!!! {{ exercice("On passe au niveau supérieur.")}}

    === "Énoncé"

        Factoriser sur $\mathbb{R}$ les expressions polynomiales suivantes
        dont les variables représentent des nombres réels. 


	    1. $\left(-9 x^{2}+24\right)\left(8 x^{2}+8\right)+64 x^{4}-64$
        1. $x^{4}+x^{2}+1$
	    1. $(a c+b d)^{2}+(a d-b c)^{2}$
	    1. $(a p+b q+c r+d s)^{2}+(a q-b p-c s+d r)^{2}+(a r+b s-c p-d q)^{2}+(a s-b r+c q-d p)^{2}$ 

	=== "Corrigé"

         1.                               On                             calcule
             $x^{4}-1=\left(x^{2}-1\right)\left(x^{2}+1\right)=(x-1)(x+1)\left(x^{2}+1\right)$.
		 1.  On   calcule  $\left(-9   x^{2}+24\right)\left(8  x^{2}+8\right)+64
		    x^{4}-64=-8\left(x^{2}+1\right)\left[9
		    x^{2}-24-8\left(x^{2}-1\right)\right]=-8\left(x^{2}+1\right)(x-4)(x+4)$.
		 1.             On            calcule             $x^{4}+x^{2}+1=x^{4}+2
		    x^{2}+1-x^{2}=\left(x^{2}+1\right)-x^{2}=\left(x^{2}+x+1\right)\left(x^{2}-x+1\right)$. La
		    factorisation est  alors terminée sur $\mathbb{R}$  puisque les deux
		    équations, $x^{2}+x+1=0$  et $x^{2}-x+1=0$,  n'ont pas  de solutions
		    réelles.
		 1. Une fois n'est pas coutume  : on peut commencer par développer avant
		    de  factoriser. Ce  qui  donne $(a  c+b  d)^{2}+(a d-b  c)^{2}=a^{2}
		    c^{2}+b^{2}                  d^{2}+a^{2}                 d^{2}+b^{2}
		    c^{2}=\left(a^{2}+b^{2}\right)\left(c^{2}+d^{2}\right) .$
			Remarque :  signalons tout de  même qu'une autre voie  (sans calcul)
			consiste  à interpréter  en termes  de module  d'un produit  de deux
			nombres  complexes...mais il  faudra attendre  de les  avoir étudiés
			pour les maths comps.


	
## Réponses mélangées


$${\small
\begin{aligned}
& x^{5}+2 x^{4}+x^{3}-x^{2}-2 x-1 \quad 2+x^{3}-x^{4}-x^{5} \quad 8 x^{3}-6 x^{2}+\frac{3}{2} x-\frac{1}{8} \\
& x^{5}-2 x^{4}+x^{3}-x^{2}+2 x-1 \quad-1-3 x-3 x^{2}+x^{3} \quad(x+y-z)(x+y+z) \\
& -5\left[\left(x-\frac{3}{5}\right)^{2}-\frac{4}{25}\right] \text { et }-5(x-1)\left(x-\frac{1}{5}\right) \quad-2+12 x-17 x^{2}+8 x^{3}-3 x^{4} \\
& x^{5}-x^{3}+x^{2}-1 \quad x^{5}-x^{3}-x^{2}+1 \quad(x-1)^{2} \quad(x-1)(x+1)\left(x^{2}+1\right) \\
& 3\left[\left(x+\frac{7}{6}\right)^{2}-\frac{37}{36}\right] \text { et } 3\left(x+\frac{7-\sqrt{37}}{6}\right)\left(x+\frac{7+\sqrt{37}}{6}\right) \\
& \left(x^{2}+x+1\right)\left(x^{2}-x+1\right) \quad(x+1)(y+1) \quad-8(x+1)(x+16) \quad(x+2)^{2} \\
& 2\left[\left(x+\frac{3}{4}\right)^{2}-\frac{233}{16}\right] \text { et } 2\left(x+\frac{3-\sqrt{233}}{4}\right)\left(x+\frac{3+\sqrt{233}}{4}\right) \quad\left(a^{2}+b^{2}+c^{2}+d^{2}\right)\left(p^{2}+q^{2}+r^{2}+s^{2}\right) \\
& 1+2 x+3 x^{2}+2 x^{3}+x^{4} \quad\left(x+\frac{3}{2}\right)^{2}-\frac{1}{4} \text { et }(x+1)(x+2) \quad 1+x^{4} \\
& -6(6 x+7) \quad 2(3 x-4)(10 x+3) \quad(14 x+3 y)(-12 x+3 y) \quad 4(5 x+4)(-5 x+1) \\
& \left(a^{2}+b^{2}\right)\left(y-4 x^{2}\right)\left(y+4 x^{2}\right) \quad(x+y)(x+1)^{2} \quad\left(a^{2}+b^{2}\right)\left(c^{2}+d^{2}\right) \\
& -8\left(x^{2}+1\right)(x-4)(x+4) \quad-28+21 x \quad(x-1)(y-1) \quad x^{4}+x^{2}+1
\end{aligned}
}$$



## QCM

!!! {{ exercice()}}

    === "QCM"
	      
	     Pour chaque  expression, une seule égalité  est correcte. $a, b,  x, t$
	     représentent des réels.
		 
	     Cliquez sur vos propositions à toutes les questions puis validez.
		 
		 {{ multi_qcm(
		 ["$\\frac{4 x^{2}}{\\dfrac{2}{x^{2}}}=$",["$2 x^{4}$", "2", "8"],[1]],
		 ["$\\frac{1}{2}-\\frac{x+1}{x}=$",[" $-\\left(\\frac{x+2}{2 x}\\right)$", "$\\frac{x-2}{2 x}$", "$\\frac{-x+2}{2 x}$"],[1]],
		 ["$\\frac{1}{x(x+1)}-\\frac{2}{x}=$",["$\\frac{-2 x-1}{x^{2}(x+1)}$", "$\\frac{-2 x-1}{x(x+1)}$", "$\\frac{-2 x+3}{x(x+1)}$"],[2]],
		 [" $\\frac{1-x^{2}}{(x-1)^{4}}=$",["$-\\frac{1+x}{(1-x)^{3}}$", "$\\frac{1+x}{(x-1)^{3}}$", "$\\frac{1+x}{(1-x)^{3}}$"],[3]],
		 ["$\\frac{e^{t}}{1+e^{t}}-1=$",["$\\frac{-1}{1+e^{t}}$", "$\\frac{2 e^{t}-1}{1+e^{t}}$", "$\\frac{e^{t}-1}{1+e^{t}}$"],[1]],
		 ["$\\frac{1}{\\frac{1}{a}+\\frac{1}{b}}=$",["$a+b$", "$\\frac{1}{a+b}$", "$\\frac{a b}{a+b}$"],[3]],
		 ["$2-\\frac{2 x+1}{x+2}=$",["$\\frac{3}{x+2}$", "$\\frac{5}{x+2}$", "$\\frac{4 x+3}{x+2}$"],[1]],
		 ["$2-\\frac{(x+1)^{2}}{x}=$",["$\\frac{-x^{2}+1}{x}$", "$\\frac{-x^{2}-1}{x}$", "$-\\frac{x^{2}-1}{x}$"],[2]],
		 ["$\\frac{1}{x-1}-\\frac{1}{x+1}=$",[" $\\frac{2}{(x-1)}$", "$\\frac{2}{x^{2}-1}$", "$\\frac{2 x}{(x-1)(x+1)}$ "],[2]],
		 ["$\\frac{1}{x^{2}-1}-\\frac{1}{x-1}=$",["$\\frac{x-2}{x^{2}-1}$", "$\\frac{x}{x^{2}-1}$", "$\\frac{-x}{x^{2}-1}$"],[3]],
		 ["$\\left(\\frac{1-\\sqrt{5}}{2}\\right)^{2}=$",["$\\frac{3+\\sqrt{5}}{2}$", "$\\frac{3-\\sqrt{5}}{2}$", " $\\frac{-2+\\sqrt{5}}{2}$"],[2]],
		 ["$\\frac{1}{\\sqrt{2}-1}=$",["$\\frac{-1}{\\sqrt{2}+1}$", "$\\sqrt{2}+1$", " $\\frac{1}{\\sqrt{2}}-1$"],[2]],
		 ["$\\left(1-x^{2}\\right)(1-x)=$",["$(1-x)^{2}(1+x)$", "$(1-x)(1+x)^{2}$", "$1-x^{3}$"],[1]],
		 [" $2 x^{2}-5 x-3=$",["$(2 x+1)(x-3)$", "$(2 x-1)(x+3)$", "$\\left(x+\\frac{1}{2}\\right)(x-3)$"],[1]],
		 ["$5-2(x-1)^{2}=$",["$-2 x^{2}+4 x+7$", "$-2 x^{2}+4 x+3$", "$-2 x^{2}-4 x+7$"],[2]],
		 ["$3(x+2)^{2}-8=$",["$3 x^{2}+4 x-4$", " $3 x^{2}+12 x+4$", "$3 x^{2}+6 x+4$"],[2]],
		 ["$(3 x-2)^{2}-(3 x-2)(x-1)=$",["$(3 x-2)(2 x-1)$", "$(3 x-2)(2 x-3)$", "$(3 x-2)(2-x)$"],[1]]
	     )}}

    === "Réponses"
	
	     Cliquez et vous saurez tout de suite si vous avez vu juste :

		 1. $\dfrac{4 x^{2}}{\dfrac{2}{x^{2}}}=$
		    {{ qcm(["$2 x^{4}$", "2", "8"],[1], shuffle=True)}}
     	 2. $\dfrac{1}{2}-\dfrac{x+1}{x}=$
		    {{ qcm([" $-\\left(\\frac{x+2}{2 x}\\right)$", "$\\frac{x-2}{2 x}$", "$\\frac{-x+2}{2 x}$"],[1], shuffle=True)}}
		 3. $\dfrac{1}{x(x+1)}-\dfrac{2}{x}=$
		    {{ qcm(["$\\frac{-2 x-1}{x^{2}(x+1)}$", "$\\frac{-2 x-1}{x(x+1)}$", "$\\frac{-2 x+3}{x(x+1)}$"],[2], shuffle=True)}}
		 4.  $\dfrac{1-x^{2}}{(x-1)^{4}}=$
		    {{ qcm(["$-\\frac{1+x}{(1-x)^{3}}$", "$\\frac{1+x}{(x-1)^{3}}$", "$\\frac{1+x}{(1-x)^{3}}$"],[3], shuffle=True)}}
		 5. $\dfrac{e^{t}}{1+e^{t}}-1=$
		    {{ qcm(["$\\frac{-1}{1+e^{t}}$", "$\\frac{2 e^{t}-1}{1+e^{t}}$", "$\\frac{e^{t}-1}{1+e^{t}}$"],[1], shuffle=True)}}
		 6. $\dfrac{1}{\dfrac{1}{a}+\dfrac{1}{b}}=$
		    {{ qcm(["$a+b$", "$\\frac{1}{a+b}$", "$\\frac{a b}{a+b}$"],[3], shuffle=True)}}
		 7. $2-\dfrac{2 x+1}{x+2}=$
		    {{ qcm(["$\\frac{3}{x+2}$", "$\\frac{5}{x+2}$", "$\\frac{4 x+3}{x+2}$"],[1], shuffle=True)}}
		 8. $2-\dfrac{(x+1)^{2}}{x}=$
		    {{ qcm(["$\\frac{-x^{2}+1}{x}$", "$\\frac{-x^{2}-1}{x}$", "$-\\frac{x^{2}-1}{x}$"],[2], shuffle=True)}}
		 9. $\dfrac{1}{x-1}-\dfrac{1}{x+1}=$
		    {{ qcm([" $\\frac{2}{(x-1)}$", "$\\frac{2}{x^{2}-1}$", "$\\frac{2 x}{(x-1)(x+1)}$ "],[2], shuffle=True)}}
		 10. $\dfrac{1}{x^{2}-1}-\dfrac{1}{x-1}=$
		    {{ qcm(["$\\frac{x-2}{x^{2}-1}$", "$\\frac{x}{x^{2}-1}$", "$\\frac{-x}{x^{2}-1}$"],[3], shuffle=True)}}
		 11. $\left(\dfrac{1-\sqrt{5}}{2}\right)^{2}=$
		    {{ qcm(["$\\frac{3+\\sqrt{5}}{2}$", "$\\frac{3-\\sqrt{5}}{2}$", " $\\frac{-2+\\sqrt{5}}{2}$"],[2], shuffle=True)}}
		 12. $\dfrac{1}{\sqrt{2}-1}=$
		    {{ qcm(["$\\frac{-1}{\\sqrt{2}+1}$", "$\\sqrt{2}+1$", " $\\frac{1}{\\sqrt{2}}-1$"],[2], shuffle=True)}}
		 13. $\left(1-x^{2}\right)(1-x)=$
		    {{ qcm(["$(1-x)^{2}(1+x)$", "$(1-x)(1+x)^{2}$", "$1-x^{3}$"],[1], shuffle=True)}}
		 14.  $2 x^{2}-5 x-3=$
		    {{ qcm(["$(2 x+1)(x-3)$", "$(2 x-1)(x+3)$", "$\\left(x+\\frac{1}{2}\\right)(x-3)$"],[1], shuffle=True)}}
		 15. $5-2(x-1)^{2}=$
		    {{ qcm(["$-2 x^{2}+4 x+7$", "$-2 x^{2}+4 x+3$", "$-2 x^{2}-4 x+7$"],[2], shuffle=True)}}
		 16. $3(x+2)^{2}-8=$
		    {{ qcm(["$3 x^{2}+4 x-4$", " $3 x^{2}+12 x+4$", "$3 x^{2}+6 x+4$"],[2], shuffle=True)}}
		 17. $(3 x-2)^{2}-(3 x-2)(x-1)=$
		    {{ qcm(["$(3 x-2)(2 x-1)$", "$(3 x-2)(2 x-3)$", "$(3 x-2)(2-x)$"],[1], shuffle=True)}}

	=== "Réponses et commentaires"

		 1. $\mathrm{A}$;
		 2. A (mise au même dénominateur)
		 3. $\mathrm{B} \operatorname{car} \frac{1}{x(x+1)}-\frac{2}{x}=\frac{1}{x(x+1)}-\frac{2(x+1)}{x(x+1)}=\frac{-2 x-1}{x(x+1)}$.
		 4. $\mathrm{C} \operatorname{car} \frac{1-x^{2}}{(x-1)^{4}}=\frac{(1-x)(1+x)}{(x-1)^{4}}=\frac{(1-x)(1+x)}{(1-x)^{4}}=\frac{1+x}{(1-x)^{3}}$.

		 Ici, on a utilisé le fait que $(x-1)^{4}=(-(1-x))^{4}=(1-x)^{4}$ puisque 4 est pair.

		 5. A (mise au même dénominateur)
		 6. $\mathrm{C}$ (multiplier en haut et en bas par $a b)$
		 7. A (mise au même dénominateur)
		 8. B (mise au même dénominateur)
		 9. B (mise au même dénominateur, en utilisant que $\left.(x-1)(x+1)=x^{2}-1\right)$
		 10. C. En effet (en essayant de minimiser les calculs) :

		     $$
			 \begin{aligned}
			 \frac{1}{x^{2}-1}-\frac{1}{x-1} & =\frac{1}{(x-1)(x+1)}-\frac{1}{x-1} \\
			 & =\frac{1}{(x-1)(x+1)}-\frac{x+1}{(x-1)(x+1)}=\frac{-x}{(x-1)(x+1)}=\frac{-x}{x^{2}-1} .
			 \end{aligned}
			 $$

		 11. $\mathrm{B}$ (on développe en utilisant $\left.(a-b)^{2}=a^{2}-2 a b+b^{2}\right)$.
		 12. B. Pour obtenir cela, on multiplie en haut et en bas par $\sqrt{2}+1$ (ce que l'on appelle la "quantité conjuguée" à $\sqrt{2}-1$ ).

	         $$
			 \frac{1}{\sqrt{2}-1}=\frac{\sqrt{2}+1}{(\sqrt{2}-1)(\sqrt{2}+1)}=\frac{\sqrt{2}+1}{(\sqrt{2})^{2}-1^{2}}=\frac{\sqrt{2}+1}{2-1}=\sqrt{2}+1 .
			 $$

		 13. $\mathrm{A}$, car $\left(1-x^{2}\right)(1-x)=(1-x)(1+x)(1-x)=(1-x)^{2}(1+x)$.
		 14. $\mathrm{A}$;
		 15. $\mathrm{B}$
		 16. $\mathrm{B}$;
		 17. A (factoriser par $3 x-2$ ).

