{{ chapitre(5, "Racines carrées", "Rappel N°")}}



## Premiers calculs
![Placeholder](./IMG/gaston3.png){ align=right width=300}

!!! note "Petits rappels"

	Soit $x$ un réel positif. La racine carrée de $x$ est l'unique réel positif $a$ tel que $a^{2}=x$

	Alors, pour tous réels positifs $x, y \in \mathbb{R}^{+}$:

	$$
	(\sqrt{x})^{2}=x, \quad \sqrt{x y}=\sqrt{x} \sqrt{y}, \quad \sqrt{\frac{x}{y}}=\frac{\sqrt{x}}{\sqrt{y}} .
	$$

	Par ailleurs, pour tout $x \in \mathbb{R}$, on a $\sqrt{x^{2}}=|x|=\left\{\begin{aligned} x & \text { si } x \geqslant 0 \\ -x & \text { si } x \leqslant 0 .\end{aligned}\right.$

!!! {{ exercice("Définition de la racine carrée")}}

    === "Énoncé"
		
		Simplifier les expressions suivantes.

		1. $\sqrt{(-5)^{2}}$
		1. $\sqrt{(2-\sqrt{7})^{2}}$
		1. $\sqrt{(\sqrt{3}-1)^{2}}$
		1. $\sqrt{(3-\pi)^{2}}$
		1. $\sqrt{(\sqrt{3}-2)^{2}}$
		1. $\sqrt{(3-a)^{2}}$

	=== "Corrigé"

         1. Quand $a$ est un réel positif, $\sqrt{a}$ est le nombre positif dont
            le carré vaut $a$ donc $\sqrt{\left(-5^{2}\right)}=5$.
		 1. idem
	 	 1. idem
		 1. idem
		 1. idem
		 1. On trouve $|3-a|$, c'est-à-dire $3-a$ si $a \leqslant 3$ et $a-3$ si
		    $a \geqslant 3$.


!!! {{ exercice("Transformation d'écriture.")}}

    === "Énoncé"

        Écrire aussi simplement que possible les expressions suivantes.

		1. $(2 \sqrt{5})^{2}$
		1. $(3+\sqrt{7})^{2}-(3-\sqrt{7})^{2}$
		1. $(2+\sqrt{5})^{2}$
		1. $(\sqrt{2 \sqrt{3}})^{4}$
		1. $\sqrt{4+2 \sqrt{3}}$
		1. $\left(\dfrac{5-\sqrt{2}}{\sqrt{3}}\right)^{2}$
		1. $\sqrt{11+6 \sqrt{2}}$
		1. $(\sqrt{2}+\sqrt{3})^{2}+(\sqrt{2}-\sqrt{3})^{2}$


	=== "Corrigé"

         1. RAS
		 1. RAS
		 1. On essaie  de reconnaître une identité remarquable dans  la racine :
		    $\sqrt{4+2                                       \sqrt{3}}=\sqrt{1+2
		    \sqrt{3}+3}=\sqrt{(1+\sqrt{3})^{2}}=1+\sqrt{3}$ 
			




## Avec la méthode de la quantité conjuguée




!!! {{ exercice()}}

    === "Énoncé"

        Rendre rationnels les dénominateurs des expressions suivantes.

		1. $\dfrac{2-\sqrt{3}}{2+\sqrt{2}}$
		1. $\dfrac{\sqrt{2}-1}{\sqrt{2}+1}$
		1. $\dfrac{\sqrt{2}+\sqrt{3}+\sqrt{5}}{\sqrt{2}+\sqrt{3}}$
		1. $\dfrac{\sqrt{5}-\sqrt{2}}{\sqrt{3}-\sqrt{2}}$
		1. $\dfrac{1}{\sqrt{2}-\sqrt{3}}$
		1. $\dfrac{\sqrt{2}+\sqrt{3}}{1-\sqrt{3}}$
		1. $\dfrac{5+2 \sqrt{6}}{\sqrt{2}+\sqrt{3}}+\dfrac{5-2 \sqrt{6}}{\sqrt{2}-\sqrt{3}}$
		1. $\left(\dfrac{5 \sqrt{2}}{\sqrt{3}+1}\right)^{2}$


	=== "Corrigé"

	    1. $ \begin{aligned}\frac{2-\sqrt{3}}{2+\sqrt{2}} & =\frac{2-\sqrt{3}}{2+\sqrt{2}} \times \frac{2-\sqrt{2}}{2-\sqrt{2}}=\frac{(2-\sqrt{3})(2-\sqrt{2})}{(2+\sqrt{2})(2-\sqrt{2})} \\& =\frac{(2-\sqrt{3})(2-\sqrt{2})}{2^{2}-2}=\frac{4-2 \sqrt{2}-2 \sqrt{3}+\sqrt{6}}{2} \\& =2-\sqrt{2}-\sqrt{3}+\frac{1}{2} \sqrt{6} .\end{aligned}$
			
		 


!!! {{ exercice()}}

    === "Énoncé"

        Exprimer la quantité suivante sans racine carrée au dénominateur.

	    $$
		\dfrac{1}{1+\sqrt{2}+\sqrt{3}}
		$$


	=== "Corrigé"

         On     pose    $A:=\frac{1}{1+\sqrt{2}+\sqrt{3}}     \cdot$    On     a
         $A=\frac{1}{1+(\sqrt{2}+\sqrt{3})}=\frac{1-(\sqrt{2}+\sqrt{3})}{(1+(\sqrt{2}+\sqrt{3}))(1-(\sqrt{2}+\sqrt{3}))}=\frac{1-(\sqrt{2}+\sqrt{3})}{1-(\sqrt{2}+\sqrt{3})^{2}}=\frac{\sqrt{2}+\sqrt{3}-1}{4+2
         \sqrt{6}}.$
		 
		 Ainsi, la technique  de la « quantité conjuguée »  n'est pas suffisante
		 ici;      mais     on      peut      la      réappliquer.     On      a
		 $A=\frac{(\sqrt{2}+\sqrt{3}-1)(4-2     \sqrt{6})}{(4+2    \sqrt{6})(4-2
		 \sqrt{6})}=\frac{4   \sqrt{2}-4   \sqrt{3}+4  \sqrt{3}-6   \sqrt{2}-4+2
		 \sqrt{6}}{16-24}=\frac{2                                   \sqrt{2}+4-2
		 \sqrt{6}}{8}=\frac{\sqrt{2}+2-\sqrt{6}}{4}$
		 
		 Ainsi,                               on                               a
		 $\frac{1}{1+\sqrt{2}+\sqrt{3}}=\frac{\sqrt{2}+2-\sqrt{6}}{4}$    :   ce
		 qu'on cherchait.
		 
		 Remarque : on pouvait aussi faire un autre type de quantité conjuguée :
		 $\frac{1}{1+\sqrt{2}+\sqrt{3}}=\frac{1+\sqrt{2}-\sqrt{3}}{(1+\sqrt{2}+\sqrt{3})(1+\sqrt{2}-\sqrt{3})}=\frac{1+\sqrt{2}-\sqrt{3}}{2
		 \sqrt{2}}=\frac{\sqrt{2}+2-\sqrt{6}}{4} .$ 
		 
		 
		 
## Calculs variés

!!! {{ exercice("Avec une variable.")}}

    === "Énoncé"

         On   considère    la   fonction   $f$   qui    à   $x>1$   associe
         $f(x)=\sqrt{x-1}$.  Pour tout  $x>1$, calculer  et simplifier  les
         expressions suivantes. 

		1. $f(x)+\dfrac{1}{f(x)}$
		1. $\dfrac{f^{\prime}(x)}{f(x)}$
		1. $\dfrac{f(x+2)-f(x)}{f(x+2)+f(x)}$
		1. $f(x)+4 f^{\prime \prime}(x) \ldots \ldots \ldots \ldots \ldots \ldots \ldots$
		1. $\sqrt{x+2 f(x)}$
		1. $\dfrac{f(x)}{f^{\prime \prime}(x)}$


	=== "Corrigé"

         1. RAS
		 1. RAS
		 1. On essaie  de reconnaître une identité remarquable dans  la racine :
		    $\sqrt{x+2     f(x)}=\sqrt{x+2    \sqrt{x-1}}=\sqrt{\sqrt{x-1}^{2}+2
		    \sqrt{x-1}+1}=\sqrt{(\sqrt{x-1}+1)^{2}}=\sqrt{x-1}+1$
		 1. RAS
		 1.     Le     calcul    donne     $f^{\prime    \prime}(x)=-\frac{1}{4}
		     \frac{1}{(x-1)^{3   /    2}}$   d'où    :   $    f(x)+4   f^{\prime
		     \prime}(x)=\sqrt{x-1}-\frac{1}{(x-1)     \sqrt{x-1}}=\frac{1}{(x-1)
		     \sqrt{x-1}}\left((x-1)^{2}-1\right)=\frac{x(x-2)}{(x-1)
		     \sqrt{x-1}}$ 


!!! {{ exercice("Mettre au carré.")}}

    === "Énoncé"

         Élever  les  quantités  suivantes  au carré  pour  en  donner  une
         expression simplifiée. 

		1. $\sqrt{3+\sqrt{5}}-\sqrt{3-\sqrt{5}}$
		1. $\sqrt{3-2 \sqrt{2}}+\sqrt{3+2 \sqrt{2}}$

	=== "Corrigé"

         1.              $(\sqrt{3+\sqrt{5}}-\sqrt{3-\sqrt{5}})^{2}=3+\sqrt{5}-2
             \sqrt{3+\sqrt{5}}  \sqrt{3-\sqrt{5}}+3-\sqrt{5}=6-2  \sqrt{9-5}=6-2
             \sqrt{4}=6-4=2 $
			 
			 De  plus, $\sqrt{3+\sqrt{5}}-\sqrt{3-\sqrt{5}}  \geqslant 0$,  donc
			 $\sqrt{3+\sqrt{5}}-\sqrt{3-\sqrt{5}}=\sqrt{2}$ 


!!! {{ exercice("Méli-mélo")}}

    === "Énoncé"

        Donner une écriture simplifiée des réels suivants.

		1. $\dfrac{3-\sqrt{5}}{2+\sqrt{5}}$
		1. $3 \mathrm{e}^{-\dfrac{1}{2} \ln 3}$
		1. $\sqrt{3+2 \sqrt{2}}$
		1. $2 \sqrt{\dfrac{3+\sqrt{5}}{2}}$
		1. $\sqrt{\dfrac{2+\sqrt{2}}{2-\sqrt{2}}}$
		1. $\dfrac{1}{2} \ln \dfrac{\sqrt{2}+1}{\sqrt{2}-1}$

	=== "Corrigé"

         1. RAS
		 1.  On  calcule $3+2 \sqrt{2}=2+2 \sqrt{2}+1=(\sqrt{2})^{2}+2  \times 1
		     \times   \sqrt{2}+1^{2}=(1+\sqrt{2})^{2}$   et   on   trouve   donc
		     $\sqrt{3+2 \sqrt{2}}=1+\sqrt{2}$ 
		 1. RAS
		 1. RAS
		 1. $ 2 \sqrt{\frac{3+\sqrt{5}}{2}}=\sqrt{6+2 \sqrt{5}}=\sqrt{1+2 \sqrt{5}+\sqrt{5}^{2}}=\sqrt{(1+\sqrt{5})^{2}}=1+\sqrt{5}$. 





	
## Réponses mélangées

$$
{\small
\begin{array}{ccccccccc}
-4(x-1)^{2} & 12 \sqrt{7} & 9+4 \sqrt{5} & 1+\sqrt{2} & 50-25 \sqrt{3} & -11+5 \sqrt{5} & 1-\sqrt{10}+\sqrt{15} \\
9-\dfrac{10}{3} \sqrt{2} & 12 & -\sqrt{3}+2 & 1+\sqrt{5} & \dfrac{1}{2} \dfrac{1}{x-1} & -(\sqrt{2}+\sqrt{3}) & 1+\sqrt{2} & 2 \sqrt{2} \\
\sqrt{15}+\sqrt{10}-\sqrt{6}-2 & \dfrac{x(x-2)}{(x-1) \sqrt{x-1}} & 10 & 3+\sqrt{2} & 5 & \sqrt{7}-2 & 1+\sqrt{x-1} \\
\dfrac{x}{\sqrt{x-1}} & \sqrt{3} & \dfrac{\sqrt{2}+2-\sqrt{6}}{4} & 2-\sqrt{2}-\sqrt{3}+\dfrac{1}{2} \sqrt{6} & x-\sqrt{x^{2}-1} & 2 \sqrt{2} & \pi-3 \\
1+\sqrt{3} & \sqrt{3}-1 & \sqrt{2} & 20 & 3-2 \sqrt{2} & \ln (1+\sqrt{2}) & |3-a| & -\dfrac{3+\sqrt{2}+\sqrt{3}+\sqrt{6}}{2}
\end{array}
}
$$





## QCM

!!! {{ exercice()}}

    === "QCM"
	      
	     Pour chaque  expression, une seule égalité  est correcte. $a, b,  x, C$
	     représentent des réels, $n$ et $k$ représentent des entiers.
		 
	     Cliquez sur vos propositions à toutes les questions puis validez.
		 
		 {{ multi_qcm(
		 ["$\\frac{a^{2}}{\\sqrt{a}}=$",["$\\sqrt{a}$", "$a \\sqrt{a}$", "$\\frac{1}{\\sqrt{a}}$"],[2]],
		 ["$\\frac{\\sqrt{a^{2}+3 a^{2}}}{\\sqrt{2 a}}=$",["$2 \\sqrt{a}$", "$\\sqrt{2 a}$", "$\\sqrt{\\frac{a}{2}}$"],[2]],
		 ["Pour $x \\geqslant 0, \\frac{1}{x} \\sqrt{x^{3}+2 x^{2}+3}=$",["$\\sqrt{x+2+\\frac{3}{x^{2}}}$", " $\\sqrt{x^{2}+2 x+\\frac{3}{x}}$", "$\\sqrt{\\frac{1}{x}+x^{3}+2 x^{2}+3}$"],[1]],
		 ["$2 \\times\\left(\\left(\\frac{\\sqrt{2}}{2}\\right)^{n}\\right)^{2}=$",["$2^{n}$", "$2^{n+1}$", "$\\frac{1}{2^{n-1}}$"],[3]]
	     )}}

    === "Réponses"
	
	     Cliquez et vous saurez tout de suite si vous avez vu juste :

		 1. $\dfrac{a^{2}}{\sqrt{a}}=$
		    {{ qcm(["$\\sqrt{a}$", "$a \\sqrt{a}$", "$\\frac{1}{\\sqrt{a}}$"],[2], shuffle=True)}}
		 2. $\dfrac{\sqrt{a^{2}+3 a^{2}}}{\sqrt{2 a}}=$
		    {{ qcm(["$2 \\sqrt{a}$", "$\\sqrt{2 a}$", "$\\sqrt{\\frac{a}{2}}$"],[2], shuffle=True)}}
		 3. Pour $x \geqslant 0, \dfrac{1}{x} \sqrt{x^{3}+2 x^{2}+3}=$
		    {{ qcm(["$\\sqrt{x+2+\\frac{3}{x^{2}}}$", " $\\sqrt{x^{2}+2 x+\\frac{3}{x}}$", "$\\sqrt{\\frac{1}{x}+x^{3}+2 x^{2}+3}$"],[1], shuffle=True)}}
		 4. $2 \times\left(\left(\dfrac{\sqrt{2}}{2}\right)^{n}\right)^{2}=$
		    {{ qcm(["$2^{n}$", "$2^{n+1}$", "$\\frac{1}{2^{n-1}}$"],[3], shuffle=True)}}
     	 

	=== "Réponses et commentaires"

		 1. $\mathrm{B}, \operatorname{car} \frac{a^{2}}{\sqrt{a}}=\frac{a \times a}{\sqrt{a}}=\frac{a \times(\sqrt{a})^{2}}{\sqrt{a}}=a \sqrt{a}$.
		 2. $\mathrm{B}, \operatorname{car} \frac{\sqrt{a^{2}+3 a^{2}}}{\sqrt{2 a}}=\frac{\sqrt{4 a^{2}}}{\sqrt{2 a}}=\frac{\sqrt{(2 a)^{2}}}{\sqrt{2 a}}=\frac{\sqrt{2 a} \times \sqrt{2 a}}{\sqrt{2 a}}=\sqrt{2 a}$.
		 
		 3. A. En effet, pour $x>0$, on a $\sqrt{x^{2}}=|x|=x$, donc

		    $$
		     \frac{1}{x} \sqrt{x^{3}+2 x^{2}+3}=\frac{1}{\sqrt{x^{2}}} \sqrt{x^{3}+2 x^{2}+3}=\sqrt{\frac{1}{x^{2}}\left(x^{3}+2 x^{2}+3\right)}=\sqrt{x+2+\frac{3}{x^{2}}} .
		    $$

		 4. C. En effet,

		    $$
			2       \times\left(\left(\frac{\sqrt{2}}{2}\right)^{n}\right)^{2}=2
			\times\left(\frac{\sqrt{2}}{2}\right)^{2                        n}=2
			\times\left(\left(\frac{\sqrt{2}}{2}\right)^{2}\right)^{n}=2
			\times\left(\frac{2}{4}\right)^{n}=2
			\times\left(\frac{1}{2}\right)^{n}=\frac{2}{2^{n}}=\frac{1}{2^{n-1}}
			$$

