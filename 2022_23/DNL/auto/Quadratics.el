(TeX-add-style-hook
 "Quadratics"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-environments
    '("demonstration" LaTeX-env-args ["argument"] 0)
    '("nliste" LaTeX-env-args ["argument"] 1)
    '("Sliste" LaTeX-env-args ["argument"] 0)
    '("DeuxCols" LaTeX-env-args ["argument"] 0)
    '("Zigoui" LaTeX-env-args ["argument"] 0)
    '("arrayl" LaTeX-env-args ["argument"] 0)
    '("rubric" LaTeX-env-args ["argument"] 1)
    '("Question" LaTeX-env-args ["argument"] 1)
    '("systeme" LaTeX-env-args ["argument"] 0)))
 :latex)

