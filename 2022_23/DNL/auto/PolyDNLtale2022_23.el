(TeX-add-style-hook
 "PolyDNLtale2022_23"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("trmbookDNL" "10pt" "a4paper" "svgnames" "arabe" "english" "utf8" "euler")))
   (TeX-run-style-hooks
    "latex2e"
    "patch-tkz-graph"
    "Quadratics"
    "sequences"
    "ProbasDNL"
    "trmbookDNL"
    "trmbookDNL10"
    "etex"
    "preambuleTrm"
    "yfonts"
    "alterqcm"
    "lcd"
    "caption"
    "soul"
    "multido"
    "enumitem"
    "picins"
    "numprint"
    "tikz"
    "tkz-graph"
    "tikz-qtree"
    "tkz-linknodes"
    "cclicenses"
    "cclicence"
    "textcomp"
    ""
    "mathtools"
    "xlop"
    "longtable"
    "algo"
    "casio"
    "ti83font"
    "bold-extra"
    "algorithm2e")
   (TeX-add-symbols
    '("entet" 3)
    "myunit"
    "triangleup"
    "nor"
    "nand"
    "cacheb"
    "cacheg"
    "soustitre"
    "Python"))
 :latex)

