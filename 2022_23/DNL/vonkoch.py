from turtle import forward, left, right, speed, pencolor, penup, pendown, backward

taille = 810
couleurs = ["red", "orange", "blue"]

def flocon(taille, niveau):
    if niveau == 0:
        forward(taille)
    else:
        taille = taille / 3
        flocon(taille, niveau - 1)
        left(60)
        flocon(taille, niveau - 1)
        right(120)
        flocon(taille, niveau - 1)
        left(60)
        flocon(taille, niveau - 1)

speed('fastest')
penup()
backward(taille/2)
left(90)
forward(taille/3)
right(90)
pendown()

for i in range(3):
    pencolor(couleurs[i])
    flocon(taille, 4)
    right(120)

