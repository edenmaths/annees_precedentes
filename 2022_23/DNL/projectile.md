{{ chapitre(1, "Projectiles")}}


The  trajectory of  a projectile  is the  path that  it follows  during its
flight. In  the absence  of air  resistance, the  path of  the flight  of a
projectile will trace out the shape of a parabola as we will now prove it.

Say an object of  mass $m$ es thrown with uniform  velocity $v_0$ making an
angle $θ$ with the horizontal axis. The only force acting upon a projectile
is   gravity,    which   imparts    a   downward   acceleration    to   the
projectile. Because of  the object's initial inertia, an  external force is
unnecessary to maintain the horizontal velocity of the object.

The elementary equations  of ballistics only take into  account the initial
velocity  of   the  projectile   and  an  assumed   constant  gravitational
acceleration.  Practical  applications of  ballistics  have  to consider  a
variety of  other factors like  air resistance, crosswinds,  target motion,
the varying  acceleration due  to gravity,  and in the  case the  target is
significantly far away, even the rotation of the earth. 


![parabola](parabola.png "parabola")


Let's apply the Newton's second law, assuming that the only force applied
to  the projectile  is the  downward force  of gravity.  Let's compute  the
horizontal and vertical components of the accelaration.

$a_x = ...$

$a_y=....$

Keeping in mind that  $g$, $θ$ and $v_0$ are constant,  you should then get
the velocities along both axis and eventually state that :

- $x=v_0×t×\cos(θ) \qquad (1)$
- $y=v_0×t×\sin(θ)-\frac{1}{2}×g×t^2\qquad (2)$

Replacing $t$ in equation (2) with  the expression of $t$ from equation (3)
you should then get  an expression of $y$ depending on  $x$ and explain the
particular shape of the trajecory of the projectile.


You could now be able to work out :

- the total time of flight for a projectile
- the maximum height reached by a projectile
- the total time of flight
- the time to reach the maximum height
- the horizontal range
- the value(s) of $θ$ giving the maximum range.




