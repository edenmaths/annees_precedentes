\documentclass[10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[version=4]{mhchem}
\usepackage{stmaryrd}
\usepackage{graphicx}
\usepackage[export]{adjustbox}
\graphicspath{ {./images/} }

\title{mathcentre }

\author{}
\date{}


\begin{document}
\maketitle
\section{Finding areas by integration}
mc-TY-areas-2009-1

Integration can be used to calculate areas. In simple cases, the area is given by a single definite integral. But sometimes the integral gives a negative answer which is minus the area, and in more complicated cases the correct answer can be obtained only by splitting the area into several parts and adding or subtracting the appropriate integrals.

In order to master the techniques explained here it is vital that you undertake plenty of practice exercises so that they become second nature.

After reading this text, and/or viewing the video tutorial on this topic, you should be able to:

\begin{itemize}
  \item find the area beween a curve, the $x$-axis, and two given ordinates;

  \item find the area between a curve and the $x$-axis, where the ordinates are given by the points where the curve crosses the axis;

  \item find the area between two curves.

\end{itemize}

\section{Contents}
\begin{enumerate}
  \item Introduction 2

  \item The area between a curve and the $x$-axis 2

  \item Some examples 4

  \item The area between two curves $\quad 7$

  \item Another way of finding the area between two curves 9

\end{enumerate}

\section{Introduction}
We can obtain the area between a curve, the $x$-axis, and specific ordinates (that is, values of $x$ ), by using integration. We know this from the units on Integration as Summation, and on Integration as the Reverse of Differentiation. In this unit we are going to look at how to apply this idea in a number of more complicated situations.

\section{The area between a curve and the $x$-axis}
Let us begin by exploring the following question: 'Calculate the areas of the segments contained between the $x$-axis and the curve $y=x(x-1)(x-2)$.'

In order to answer this question, it seems reasonable for us to draw a sketch of the curve, as there is no mention of the ordinates, or values, of $x$. To make the sketch, we see first that the curve crosses the $x$-axis when $y=0$, in other words when $x=0, x=1$, and $x=2$. Next, when $x$ is large and positive, we see that $y$ is also large and positive. Finally, when $x$ is large and negative, we see that $y$ is also large and negative. So if we join up these features that we have found on the graph, we can see that the curve looks like this.

\begin{center}
\includegraphics[max width=\textwidth]{2023_03_05_6da03f2c48825e8a78c9g-02}
\end{center}

Now the areas required are obviously the area $A$ between $x=0$ and $x=1$, and the area $B$ between $x=1$ and $x=2$. But there is a marked difference between these two areas in terms of their position. The area $A$ is above the $x$-axis, whereas the area $B$ is below it. In previous units we have talked only about calculating areas using integration when the curve, and thus the area, is above the $x$-axis. Does the position of the curve make any difference to the area?

In this example, we shall play safe and calculate each area separately. We know that the area $A$ is given by the integral from $x=0$ to $x=1$ of the curve $y=x(x-1)(x-2)=x^{3}-3 x^{2}+2 x$; thus

$$
\begin{aligned}
A & =\int_{0}^{1} y \mathrm{~d} x \\
& =\int_{0}^{1}\left(x^{3}-3 x^{2}+2 x\right) \mathrm{d} x \\
& =\left[\frac{x^{4}}{4}-\frac{3 x^{3}}{3}+\frac{2 x^{2}}{2}\right]_{0}^{1} \\
& =\left[\frac{x^{4}}{4}-x^{3}+x^{2}\right]_{0}^{1} \\
& =\left[\frac{1}{4}-1+1\right]-\left[\frac{0}{4}-0+0\right] \\
& =\frac{1}{4} .
\end{aligned}
$$

Area B should be given by a similar integral, except that now the limits of integration are from $x=1$ to $x=2$ :

$$
\begin{aligned}
B & =\int_{1}^{2} y \mathrm{~d} x \\
& =\int_{1}^{2}\left(x^{3}-3 x^{2}+2 x\right) \mathrm{d} x \\
& =\left[\frac{x^{4}}{4}-\frac{3 x^{3}}{3}+\frac{2 x^{2}}{2}\right]_{1}^{2} \\
& =\left[\frac{x^{4}}{4}-x^{3}+x^{2}\right]_{1}^{2} \\
& =\left[\frac{16}{4}-8+4\right]-\left[\frac{1}{4}-1+1\right] \\
& =0-\frac{1}{4} \\
& =-\frac{1}{4}
\end{aligned}
$$

Now the two integrals have the same magnitude, but area $A$ is above the $x$-axis and area $B$ is below the $x$-axis; and, as we see, the sign of the value $B$ is negative. The actual value of the area is $+\frac{1}{4}$, so why does our calculation give a negative answer?

In the unit on Integration as Summation, when we summed the small portions of area we were evaluating $\sum y \delta x$. Now $\delta x$ was defined as a small positive increment in $x$, but $y$ was simply the $y$-value at the ordinate $x$. Clearly this $y$-value will be negative if the curve is below the $x$-axis, so in this case the quantity $y \delta x$ will be minus the value of the area. So in our example, we were adding up a lot of values equal to minus the area, as the curve is wholly below the $x$-axis between $x=1$ and $x=2$. For this reason, the calculation gives a negative answer which is minus the value of the area.

Now suppose for a moment that the question had asked us instead to find the total area enclosed by the curve and the $x$-axis. We know the answer to this question. The total area is the area of $A, \frac{1}{4}$ of a unit of area, added to actual value of the area $B$, which is another $\frac{1}{4}$ of a unit of area. Thus the total area enclosed by the curve and the $x$-axis is $\frac{1}{2}$ of a unit of area. But suppose we had decided to work this out by finding the value of the integral between $x=0$ and $x=2$, without drawing a sketch. What answer would we get? We would find

$$
\begin{aligned}
\text { 'Area' } & =\int_{0}^{2} y \mathrm{~d} x \\
& =\int_{0}^{2}\left(x^{3}-3 x^{2}+2 x\right) \mathrm{d} x \\
& =\left[\frac{x^{4}}{4}-\frac{3 x^{3}}{3}+\frac{2 x^{2}}{2}\right]_{0}^{2} \\
& =\left[\frac{x^{4}}{4}-x^{3}+x^{2}\right]_{0}^{2} \\
& =\left[\frac{16}{4}-8+4\right]-\left[\frac{0}{4}-0+0\right] \\
& =0-0 \\
& =0 .
\end{aligned}
$$

So according to this calculation, the area enclosed by the curve and the $x$-axis is zero. But we know that this is not the case, because we have a sketch to prove it. Clearly what has happened is that the 'signed' values of the two areas have been added together in the process of integration, and they have cancelled each other out.

Thus the value of the integral evaluated between two ordinates is not necessarily the value of the area between the curve, the $x$-axis and the two ordinates. So we must be very careful when calculating areas to avoid this particular trap. The best way is always to draw a sketch of the curve over the required range of values of $x$.

\section{Key Point}
When calculating the area between a curve and the $x$-axis, you should carry out separate calculations for the parts of the curve above the axis, and the parts of the curve below the axis. The integral for a part of the curve below the axis gives minus the area for that part. You may find it helpful to draw a sketch of the curve for the required range of $x$-values, in order to see how many separate calculations will be needed.

\section{Some examples}
\section{Example}
Find the area between the curve $y=x(x-3)$ and the ordinates $x=0$ and $x=5$.

\section{Solution}
If we set $y=0$ we see that $x(x-3)=0$, and so $x=0$ or $x=3$. Thus the curve cuts the $x$-axis at $x=0$ and at $x=3$. The $x^{2}$ term is positive, and so we know that the curve forms a $U$-shape as shown below.

\begin{center}
\includegraphics[max width=\textwidth]{2023_03_05_6da03f2c48825e8a78c9g-04}
\end{center}

From the graph, we can see that we need to calculate the area $A$ between the curve, the $x$-axis and the ordinates $x=0$ and $x=3$ first, and that we should expect this integral to give a negative answer because the area is wholly below the $x$-axis:

$$
\begin{aligned}
A & =\int_{0}^{3} y \mathrm{~d} x \\
& =\int_{0}^{3}\left(x^{2}-3 x\right) \mathrm{d} x \\
& =\left[\frac{x^{3}}{3}-\frac{3 x^{2}}{2}\right]_{0}^{3} \\
& =\left[\frac{27}{3}-\frac{3 \times 9}{2}\right]-\left[\frac{0}{3}-\frac{3 \times 0}{2}\right] \\
& =\left[9-\frac{27}{2}\right]-[0] \\
& =-4 \frac{1}{2} .
\end{aligned}
$$

Next, we need to calculate the area $B$ between the curve, the $x$-axis, and the ordinates $x=3$ and $x=5$ :

$$
\begin{aligned}
B & =\int_{3}^{5} y \mathrm{~d} x \\
& =\int_{3}^{5}\left(x^{2}-3 x\right) \mathrm{d} x \\
& =\left[\frac{x^{3}}{3}-\frac{3 x^{2}}{2}\right]_{3}^{5} \\
& =\left[\frac{125}{3}-\frac{3 \times 25}{2}\right]-\left[\frac{27}{3}-\frac{3 \times 9}{2}\right] \\
& =41 \frac{2}{3}-37 \frac{1}{2}-9+13 \frac{1}{2} \\
& =8 \frac{2}{3} .
\end{aligned}
$$

So the total actual area is $4 \frac{1}{2}+8 \frac{2}{3}=13 \frac{1}{6}$ units of area.

\section{Example}
Find the area bounded by the curve $y=x^{2}+x+4$, the $x$-axis and the ordinates $x=1$ and $x=3$.

\section{Solution}
If we set $y=0$ we obtain the quadratic equation $x^{2}+x+4=0$, and for this quadratic $b^{2}-4 a c=1-16=-15$ so that there are no real roots. This means that the curve does not cross the $x$-axis. Furthermore, the coefficient of $x^{2}$ is positive and so the curve is $U$-shaped. When $x=0, y=4$ and so the curve looks like this.

\begin{center}
\includegraphics[max width=\textwidth]{2023_03_05_6da03f2c48825e8a78c9g-05}
\end{center}

The required area $A$ is entirely above the $x$-axis and so we can simply evaluate the integral between the required limits:

$$
\begin{aligned}
A & =\int_{1}^{3} y \mathrm{~d} x \\
& =\int_{1}^{3}\left(x^{2}+x+4\right) \mathrm{d} x \\
& =\left[\frac{x^{3}}{3}+\frac{x^{2}}{2}+4 x\right]_{1}^{3} \\
& =\left[\frac{27}{3}+\frac{9}{2}+12\right]-\left[\frac{1}{3}+\frac{1}{2}+4\right] \\
& =25 \frac{1}{2}-4 \frac{5}{6}
\end{aligned}
$$

which equals $20 \frac{2}{3}$ units of area.

\section{Exercises}
\begin{enumerate}
  \item Find the area enclosed by the given curve, the $x$-axis, and the given ordinates.
(a) The curve $y=x$, from $x=1$ to $x=3$.
(b) The curve $y=x^{2}+3 x$, from $x=1$ to $x=3$
(c) The curve $y=x^{2}-4$ from $x=-2$ to $x=2$
(d) The curve $y=x-x^{2}$ from $x=0$ to $x=2$

  \item Find the area contained by the curve $y=x(x-1)(x+1)$ and the $x$-axis.

  \item Calculate the value of

\end{enumerate}

$$
\int_{-1}^{1} x(x-1)(x+1) \mathrm{d} x .
$$

Compare your answer with that obtained in question 3, and explain what has happened.

\begin{enumerate}
  \setcounter{enumi}{3}
  \item Calculate the value of
\end{enumerate}

$$
\int_{0}^{6}\left(4 x-x^{2}\right) \mathrm{d} x
$$

Explain your answer.

\section{The area between two curves}
A similar technique to the one we have just used can also be employed to find the areas sandwiched between curves.

\section{Example}
Calculate the area of the segment cut from the curve $y=x(3-x)$ by the line $y=x$.

\section{Solution}
Sketching both curves on the same axes, we can see by setting $y=0$ that the curve $y=x(3-x)$ cuts the $x$-axis at $x=0$ and $x=3$. Furthermore, the coefficient of $x^{2}$ is negative and so we have an inverted $U$-shape curve. The line $y=x$ goes through the origin and meets the curve $y=x(3-x)$ at the point $P$. It is this point that we need to find first of all.

\begin{center}
\includegraphics[max width=\textwidth]{2023_03_05_6da03f2c48825e8a78c9g-07}
\end{center}

At $P$ the $y$ co-ordinates of both curves are equal. Hence:

$$
\begin{aligned}
x(3-x) & =x \\
3 x-x^{2} & =x \\
2 x-x^{2} & =0 \\
x(2-x) & =0
\end{aligned}
$$

so that either $x=0$, the origin, or else $x=2$, the $x$ co-ordinate of the point $P$.

We now need to find the shaded area in the diagram. To do this we need the area under the upper curve, the graph of $y=x(3-x)$, between the $x$-axis and the ordinates $x=0$ and $x=2$. Then we need to subtract from this the area under the lower curve, the line $y=x$, and between the $x$-axis and the ordinates $x=0$ and $x=2$.

The area under the curve is

$$
\begin{aligned}
\int_{0}^{2} y \mathrm{~d} x & =\int_{0}^{2} x(3-x) \mathrm{d} x \\
& =\int_{0}^{2}\left(3 x-x^{2}\right) \mathrm{d} x \\
& =\left[\frac{3 x^{2}}{2}-\frac{x^{3}}{3}\right]_{0}^{2} \\
& =\left[6-\frac{8}{3}\right]-[0] \\
& =3 \frac{1}{3},
\end{aligned}
$$

and the area under the straight line is

$$
\begin{aligned}
\int_{0}^{2} y \mathrm{~d} x & =\int_{0}^{2} x \mathrm{~d} x \\
& =\left[\frac{x^{2}}{2}\right]_{0}^{2} \\
& =[2]-[0] \\
& =2 .
\end{aligned}
$$

Thus the shaded area is $3 \frac{1}{3}-2=1 \frac{1}{3}$ units of area.

\section{Example}
Find the area of the segment cut off from the curve $y=\sin x, 0 \leq x \leq \pi$ by the line $y=1 / \sqrt{2}$.

\section{Solution}
We begin by sketching the curve $y=\sin x$ over the given range, and then the line $y=1 / \sqrt{2}$ on the same axes and for the same range.

\begin{center}
\includegraphics[max width=\textwidth]{2023_03_05_6da03f2c48825e8a78c9g-08}
\end{center}

We can see that we need to find the two points $P$ and $Q$ where the line intersects the curve. This will be when

$$
\sin x=\frac{1}{\sqrt{2}}
$$

in other words when $x=\pi / 4$ and $x=3 \pi / 4$. So we need to calculate the area under the curve between the $x$-axis and the ordinates $x=\pi / 4$ and $x=3 \pi / 4$, then the area under the straight line $y=1 / \sqrt{2}$ between the $x$-axis and the ordinates $x=\pi / 4$ and $x=3 \pi / 4$, and then subtract the latter from the former.

The area under the curve is

$$
\begin{aligned}
\int_{\pi / 4}^{3 \pi / 4} y \mathrm{~d} x & =\int_{\pi / 4}^{3 \pi / 4} \sin x \mathrm{~d} x \\
& =[-\cos x]_{\pi / 4}^{3 \pi / 4} \\
& =\left[-\cos \frac{3 \pi}{4}\right]-\left[-\cos \frac{\pi}{4}\right] \\
& =\left[-\left(-\frac{1}{\sqrt{2}}\right)\right]-\left[-\frac{1}{\sqrt{2}}\right] \\
& =\frac{2}{\sqrt{2}},
\end{aligned}
$$

and the area under the straight line is the area of a rectangle, which is its length times its height, giving

$$
\begin{aligned}
\left(\frac{3 \pi}{4}-\frac{\pi}{4}\right) \times \frac{1}{\sqrt{2}} & =\frac{\pi}{2} \times \frac{1}{\sqrt{2}} \\
& =\frac{\pi}{2 \sqrt{2}} .
\end{aligned}
$$

Thus the required area is $2 / \sqrt{2}-\pi / 2 \sqrt{2}=(4-\pi) / 2 \sqrt{2}$.

\section{Key Point}
The area between two curves may be calculated by finding the area between one curve and the $x$-axis, and subtracting the area between the other curve and the $x$-axis. It is important to draw a sketch, in order to find the points where the curves cross, so that the ordinates can be calculated. The sketch will also show which areas should be subtracted. It may be necessary to calculate the area in a number of different parts and add the results.

\section{Another way of finding the area between two curves}
There is a second way of calculating the area between two curves.

Let us return to the sketch of the first of our two examples, and call the upper curve $y_{1}=x(3-x)$ and the lower curve $y_{2}=x$.

\begin{center}
\includegraphics[max width=\textwidth]{2023_03_05_6da03f2c48825e8a78c9g-09}
\end{center}

We can see that the distance between the two curves at any value of $x$ is $y_{1}-y_{2}$. Thus a small piece of the area between the two curves and the ordinates $x$ and $x+\delta x$ is $\left(y_{1}-y_{2}\right) \delta x$. So we can add these small pieces of area together to get

$$
\sum\left(y_{1}-y_{2}\right) \delta x
$$

and in the limit we can obtain the area between the two curves by evaluating

$$
\int_{a}^{b}\left(y_{1}-y_{2}\right) \mathrm{d} x
$$

Let us see how this works our in our example. Here,

$$
y_{1}-y_{2}=x(3-x)-x=3 x-x^{2}-x=2 x-x^{2},
$$

so that the area between the two curves is given by

$$
\begin{aligned}
\int_{0}^{2}\left(y_{1}-y_{2}\right) \mathrm{d} x & =\int_{0}^{2}\left(2 x-x^{2}\right) \mathrm{d} x \\
& =\left[\frac{2 x^{2}}{2}-\frac{x^{3}}{3}\right]_{0}^{2} \\
& =\left[x^{2}-\frac{x^{3}}{3}\right]_{0}^{2} \\
& =\left[2^{2}-\frac{2^{3}}{3}\right]-0 \\
& =4-\frac{8}{3} \\
& =\frac{4}{3} .
\end{aligned}
$$

Thus the answer is the same as before. This second method is often an easier way to calculate the area between two curves.

\section{Key Point}
The area between the curves $y_{1}$ and $y_{2}$ and the ordinates $x=a$ and $x=b$ is given by

$$
A=\int_{a}^{b}\left(y_{2}-y_{1}\right) \mathrm{d} x
$$

where $y_{1}$ is the upper curve and $y_{2}$ the lower curve.

\section{Exercises}
\begin{enumerate}
  \setcounter{enumi}{4}
  \item Calculate the area contained between the curve $y=\cos x,-\pi<x<\pi$, and the line $y=\frac{1}{2}$.

  \item Find the area contained between the line $y=x$ and the curve $y=x^{2}$.

  \item Find the area contained between the two curves $y=3 x-x^{2}$ and $y=x+x^{2}$.

\end{enumerate}

\section{Answers}
\begin{enumerate}
  \item (a) 4 (b) $20 \frac{2}{3} \quad$ (c) $10 \frac{2}{3} \quad$ (d) $\frac{2}{3}$
\end{enumerate}

(note that the integrals for parts (c) and (d) are negative, but the areas are positive).

\begin{enumerate}
  \setcounter{enumi}{1}
  \item $\frac{1}{2}$

  \item 
  \begin{enumerate}
    \setcounter{enumii}{-1}
    \item The areas between $x=-1$ and $x=0$, and between $x=0$ and $x=1$, are equal in size. However, one area is above the axis and one is below the axis, so that the integrals have opposite sign. 4. 0. The curve crosses the $x$-axis at $x=0$ and $x=4$. The areas between $x=0$ and $x=4$, and between $x=4$ and $x=6$, are equal in size. However, one area is above the axis and one is below the axis, so that the integrals have opposite sign.
  \end{enumerate}
  \item $\sqrt{3}-\pi / 3$

  \item $\frac{1}{6}$

  \item $\frac{1}{3}$

\end{enumerate}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
