from numpy import log, sqrt, exp, linspace
import matplotlib.pyplot as plt


cosh = lambda x: 0.5*(exp(x) + exp(-x))

coshinv = lambda x: log(x + sqrt(x**2+1))

fc = 191
Qb = 117
Qt = 12
L = 91

A = fc/(Qb/Qt - 1)
C = coshinv(Qb/Qt)

arch = lambda x: A*(1 - cosh(C*x/L)) + 192

para = lambda x: -(x - 96)*(96 + x)/48 

X = linspace(-96, 96)
plt.plot(X, arch(X), label = "Originale")
plt.plot( X, para(X), label = "Parabole")
plt.ylim(0, 200)
plt.grid(True)
plt.legend()
plt.show()

