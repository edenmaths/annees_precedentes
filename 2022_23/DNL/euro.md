# Euro Maths Terminale 2022-2023


- Poly de cours de DNL Maths en terminale au format  [PDF](PolyDNLtale2022_23.pdf) 

- Les sources TEX:

	- [Quadratics](Quadratics.tex)
	- [Sequences](sequences.tex)

- Sujets de Bac: 
    
	- sur le [second degre](Sujets_DNL_quadratics.pdf).
	- sur les [suites](Sujets_Bac_DNL_suites.pdf).

- [Projectiles](projectile.md)


