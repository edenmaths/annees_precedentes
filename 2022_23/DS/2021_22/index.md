# Tests et devoirs

- Test de rentrée [PDF](./TestRentree.pdf) et [TEX](TestRentree.tex)
- DS1 [PDF](./DS1_21.pdf) et [TEX](DS1_21.tex)
- DS2 [PDF](./DS2_21.pdf) et [TEX](DS2_21.tex)
- DS3 [PDF](./DS3_21.pdf) et [TEX](DS3_21.tex)
- DS4 [PDF](./DS4_21.pdf) et [TEX](DS4_21.tex)
- DS5 [PDF](./DS5_21.pdf) et [TEX](DS5_21.tex)
- DS6 [PDF](./DS6_21.pdf) et [TEX](DS6_21.tex)
- DS7 [PDF](./DS7_21.pdf) et [TEX](DS7_21.tex)
- DS9 [PDF](./DS9_21.pdf) et [TEX](DS9_21.tex) et [Corrigé](./DS9_21_corr.pdf)