(TeX-add-style-hook
 "TestRentree"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("ucs" "mathletters") ("inputenc" "utf8x") ("babel" "french") ("examecs" "correction")))
   (add-to-list 'LaTeX-verbatim-environments-local "pythoncode*")
   (add-to-list 'LaTeX-verbatim-environments-local "pythoncode")
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb*")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "EscVerb")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "patch-tkz-graph"
    "article"
    "art12"
    "ifxetex"
    "fontenc"
    "ucs"
    "inputenc"
    "babel"
    "geometry"
    "xcolor"
    "colortbl"
    "dsfont"
    "amsmath"
    "amsfonts"
    "amssymb"
    "graphicx"
    "realboxes"
    "epsf"
    "examecs"
    "gfsartemisia-euler"
    "stmaryrd"
    "pstricks"
    "pst-plot"
    "pst-text"
    "pst-tree"
    "pstricks-add"
    "tikz"
    "tkz-graph"
    "tikz-uml"
    "tikz-qtree"
    "algo"
    "minted")
   (TeX-add-symbols
    '("oij" ["argument"] 0)
    '("DecalV" ["argument"] 1)
    '("illt" 4)
    '("Fonc" 5)
    '("ve" 1)
    '("vectt" 1)
    '("vect" 1)
    "np"
    "R"
    "N"
    "C"
    "Z"
    "Q"
    "PR"
    "nor"
    "nand"
    "I"
    "E"
    "ds")
   (LaTeX-add-environments
    '("pythoncode*" LaTeX-env-args (TeX-arg-key-val LaTeX-minted-key-val-options-local))
    '("pythoncode"))
   (LaTeX-add-array-newcolumntypes
    "a"))
 :latex)

