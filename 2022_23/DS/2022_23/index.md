---
summary: Mathématiques et Informatique en BCPST 1 à l'Externat - Nantes - Tests et devoirs - Sources TeX et PDF
---


# Tests et devoirs 2022/23

- Test de rentrée [PDF](TestRentree_2022.pdf) et [TEX](TestRentree_2022.tex)
- DS1 [PDF](./DS1_22.pdf) et [TEX](DS1_22.tex) et [Corrigé](./DS1_22_corrige.pdf)
- DS2 [PDF](./DS2_22.pdf) et [TEX](DS2_22.tex) et [Corrigé](./DS2_22_corrige.pdf)
- DS3 [PDF](./DS3_22.pdf) et [TEX](DS3_22.tex) et [Corrigé](./DS3_22_corrige.pdf)
- DS4 [PDF](./DS4_22.pdf) et [TEX](DS4_22.tex) et [Corrigé](./DS4_22_corrige.pdf)
- DS5 [PDF](./DS5_22.pdf) et [TEX](DS5_22.tex) et [Corrigé](./DS5_22_corrige.pdf)
- DS6 [PDF](./DS6_22.pdf) et [TEX](DS6_22.tex) et [Corrigé](./DS6_22_corrige.pdf)
- DS7 [PDF](./DS7_22.pdf) et [TEX](DS7_22.tex) 
- DS8 [PDF](./DS8_22.pdf) et [TEX](DS8_22.tex) 
- DS9 [PDF](./DS9_22.pdf)  
