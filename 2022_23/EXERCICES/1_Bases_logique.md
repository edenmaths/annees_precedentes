{{ chapitre(1, "Bases pour raisonner- Exercices", 8)}}

## Logique

!!! {{ exercice()}}

      On  considère les  propositions  atomiques  B, T,  V,  C représentant  les
      assertions suivantes : « Hélène est belle », « Hélène joue au
      Tennis », « Hélène vole sur un dragon», « Chri-Chri aime Hélène ».
	  
      1. Énoncez des phrases simples traduisant les propositions suivantes :
	  
	     -  $¬ B$

	     - $¬ B ∧ ¬ T$
		 
		 - $¬(B ∨ T)$
		 
		 - $(B ∧V) ⟹ C$
		 
		 - $C ⟹ (B ⟺ V)$
		 
		 - $((B ∨ T) ∧ ¬ V) ⟹ ¬ C$

	  1. Traduisez par une proposition simple les phrases :
	  
		  - Chri-Chri aime Hélène seulement si elle vole sur un dragon
		  
		  - Chri-Chri aime Hélène si elle vole sur un dragon
		  
		  - Chri-Chri aime Hélène si, seulement si elle vole sur un dragon
		  
		  - Si Chri-Chri aime Hélène alors elle vole sur un dragon
		  
		  - Il  est suffisant qu'Hélène  vole sur  un dragon pour  que Chri-Chri
		    l'aime
		
		  - Il  est nécessaire qu'Hélène  vole sur  un dragon pour  que Chri-Chri
		    l'aime

	
	
!!! {{ exercice()}}

     « Si Tyron Lannister mesure 1m90 alors l'hiver ne viendra pas »
	 
!!! {{ exercice()}}

    Réécrire les phrases suivantes pour  qu'elles soient équivalentes au sens de
    la logique des propositions en utilisant *suffisant* ou *nécessaire* :
	
	- Si ses poules ne se lavent pas les dents, Hodor ne vient pas en cours.
	
	- Si je range ma chambre, Hélène ne m'aimera pas.
	


!!! {{ exercice()}}

     Déterminez  les  contraposées,  les  réciproques  puis  les  négations  des
     propositions suivantes :
	 
	 1. Si Groucho n'étudie pas son cours de maths, les filles le fuient.
	 
	 1.  Bill embrassera  Céleste seulement  s'il  écrit de  bons programmes  en
	    Python
		
	 1. Héloïse marquera un but seulement si elle lit la vie de Karl Marx en latin. 

	 
!!! {{ exercice()}}

    === "Énoncé"

         Démontrez que le produit d'un irrationnel  par un rationnel non nul est
         toujours un irrationnel.  Avez-vous raisonné par l'absurde ou par contraposée?  

	=== "Indications"

         :)
		 
		 
!!! {{ exercice()}}

    === "Énoncé"

         $\sqrt{2}$ est-il rationnel ?

	=== "Indications"

         ![Q](./IMG/Q.jpeg)
		 


!!! {{ exercice()}}

    === "Énoncé"

          Formuler les  phrases suivantes  en utilisant uniquement  des symboles
          mathématiques, notamment ceux de la logique des prédicats : 
  
  		  - L'entier $n$ est le carré d'un entier.
  		  
		  - Le cercle unité possède un point à coordonnées rationnelles.
  		  
		  - Les ensembles A et Β ne sont pas disjoints.
  		  
		  - Les ensembles A et B sont distincts.
  		  
		  - Les éléments de A sont des éléments de B.
  		  
		  - Tout réel positif est un carré de réel.
  		  
		  - L'équation $f(x)=0$ ne possède pas de solution entière.
  		  
		  - La fonction $f$ s'annule sur tout intervalle de longueur 3.
  		  
		  - Tout intervalle ouvert de $ℝ$ contient un rationnel.
  		  
		  - La fonction $f$ ne s'annule pas sur $[a,b]$.
  		  
		  - Tout intervalle fermé de $ℝ$ d'amplitude 1 contient un nombre entier.

	=== "Indications"

         :0


         
		 
		 
!!! {{ exercice()}}

    === "Énoncé"

        
        Quelle peut-être la négation de $(∀ x)(P(x))$? De $(∃ x)(P(x))$?



        L'ensemble universel est $S=\bigl\{1,2,3,4,5\bigr\}$.  Pour chacune des
        propositions suivantes, donnez-en la négation puis la valeur de vérité. 

	    - $(\exists x)(x + 3 = 10)$
	
	    - $(\forall x)(x +3<10)$

	    - $(\exists x )(x+3<5)$
   
	    - $(\forall x)(x+3 \leqslant 7)$

	=== "Indications"

         


## Récurrence


!!! {{ exercice()}}

    === "Énoncé"

         Soit $(u_n)_{n\inℕ}$ la suite réelle  définie par $u_0=2,\ u_1=7$ et
         $u_{n+2}=7u_{n+1}-10u_n$.  Démontrer que: 
		 
	    $$
		 (∀n ∈ ℕ)(u_n=2^n+5^n)
	    $$

	=== "Indications"

         

!!! {{ exercice()}}

    === "Énoncé"

         Une suite $(u_n)$ est définie par
		 
	    $$
		 u_0=3,\ \ u_{n+1}=10 u_n-9
	    $$
		 
		 Observez, conjecturez, prouvez.

	=== "Indications"

        
!!! {{ exercice()}}

    === "Énoncé"

        Déterminer le terme général de la  suite $u$ de premiers termes $u_0=1$,
        $u_1=3$ et telle que :
  
	    $$
	     (∀n ∈ℕ)(u_{n+2}=2u_{n+1}+3u_n)
	    $$

	=== "Indications"



!!! {{ exercice()}}

    === "Énoncé"
	
		Inspiration ?

        ![cube](./IMG/cube.svg)

	=== "Indications"


!!! {{ exercice()}}

    === "Énoncé"

        Démontrer  que  $(∀ x  ∈  ℝ_+)(∀n  ∈ ℕ)((1+x)^n  \geqslant  1+nx)$
        (Inégalité de Bernoulli)

	=== "Indications"

        
		


!!! {{ exercice()}}

    === "Énoncé"

        Soit $f$ la fonction définie sur $ℝ$ par: $f(x)=(x-1){\rm e}^{-x}$. On
        définit la fonction $f^{(n)}$ comme étant la dérivée $n$-ième de $f$. 
		Démontrer que:

	    $$
	    (∀ x ∈ ℝ)(f^{(n)}(x)=(-1)^n(x-n-1){\rm e}^{-x}
	    $$

	=== "Indications"
	
		$f^{(1)} = f'$,  $f^{(2)}$ est la dérivée de  $f'$,..., $f^{(n+1)}$ est
		la dérivée de $f^{(n)}$.
		
		On va appliquer le principe de récurrence simple.



!!! {{ exercice()}}

    === "Énoncé"

	    On se propose de démontrer que tous les étudiants de BCPST-1
		ont le même âge et, pour cela, on note $P\left( n\right) $ l'affirmation

	    « *si  \ on  choisit $n$  étudiants $\left(  n\in \mathbb{N}
	    ^{\ast }\right) $, il est sûr qu'ils ont tous le même âge* » 
	



	    Il est clair que $P\left( 1\right) $ est vraie. 

	    Démontrons que $P\left(n\right) ⟹ P\left( n+1\right)$. Pour cela nous
		supposons  que  $P\left( n\right)  $  est  vraie (c'est  l'hypothèse  de
		récurrence)  et   nous  choisissons   un  groupe  quelconque   de  $n+1$
		étudiants  que  nous  ordonnons  par  ordre  alphabétique  (pourquoi
		pas).  D'après l'hypothèse  de récurrence,  les $n$  premiers de
		l'ordre  alphabétique ont  tous le  même âge  ainsi que  les $n$
		derniers.  Comme ces  deux  groupes de  $n$ étudiants  ont  au moins  un
		étudiant en commun, on en déduit qu'ils ont tous le même âge.
	
	    Nous venons de démontrer que $P\left( n\right) ⟹ P\left( n+1\right) $
		pour  tout   $n\geq  1$  et,   comme  $P\left(   1\right)  $  est   vrai,  $
		P\left( n\right) $ est toujours vrai pour tout $n\geqslant 1.$
		
		Y a-t-il une erreur dans le raisonnement ?

	=== "Indications"

	    Oui

!!! {{ exercice()}}

    === "Énoncé"

	    Démontrer que, pour un entier $n$ quelconque, il existe un unique couple
        $(a_n,b_n)$ d'entiers naturels tel que:
		
	    $$
		(1+\sqrt{2})^n=a_n+b_n\sqrt{2}
	    $$

	    Déterminer une fonction Python qui renvoie $a_n$ et
	    $b_n$.
	   
	=== "Indications"

        $(1+\sqrt{2})^{n+1}=(1+\sqrt{2})^{n}×(1+\sqrt{2}) = (...)×(...)$


!!! {{ exercice()}}

    === "Énoncé"

        Démontrer par récurrence que, pour tout $n \geqslant 2$
		
		$$
		\displaystyle\sum_{k=2}^n  \frac{1}{k²-1}= \frac{( n − 1 )( 3n + 2 )}{4n ( n + 1 )}
		$$

	=== "Indications"

	    Tout s'arrange en réduisant au même dénominateur :
		
        $$
		\dfrac{(n-1)(3n+2)}{4n(n+1)}+\dfrac{1}{(n+1)^2-1}
		$$









