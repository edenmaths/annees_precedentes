{{ chapitre(3, "Réels - Exercices", 5)}}



!!! {{ exercice()}}

     Soit $f:x\mapsto 3x^2-5x+2$. Donner  l'expression développée de $g:x\mapsto
     f(2x-1)$
	 

!!! {{ exercice()}}

	1.   On  définit  sur  $ℝ$   les  fonctions  $x:x\mapsto  x^2$  et  $g:x\mapsto
		2x+1$. Donner l'expression  développée, pour tout réel $x$  de $f\circ g(x)$
		et de $g\circ f(x)$.
	1. Même question avec $f:x\mapsto 1+x^2$ et $g:\sqrt{x-1}$. 
		On précisera les domaines de définition de $f$, $g$, $f\circ g$ et $g\circ f$.


!!! {{ exercice()}}

    Résoudre les équations :
	
	1.  $(E_1)\quad \lfloor 2x+1\rfloor=-5$
	
	1.  $(E_2)\quad \lfloor x^2\rfloor=1$
	

!!! {{ exercice()}}

    On pose $f:x\mapsto \ln\left( \sqrt{x^2+1}-x\right)$
	
	1. Déterminer l'ensemble de définition de la fonction $f$.
	
	1. Démontrer que $f$ est impaire.


!!! {{ exercice()}}

    Soit $f:x\mapsto |2x+6|-|x-2|$.

	Donner  une expression  de $f$  comme fonction  affine par  morceaux. Tracer
	l'allure de la courbe représentative dans un repère bien choisi. 

!!! {{ exercice()}}

    Résoudre  les (in)équations  suivantes  d'inconnue  $x$  sur  des  ensembles  à
    déterminer :
	
	1.   $(m^2-1)x=m+1\quad (m\inℝ)$
	1.   $\dfrac{2x+1}{1-3x}=\dfrac{2(2x-1)}{-6x}$
	1.   $\sqrt{x^2+7}=4$
	1.   $\sqrt{x+2}=-4$
	1.   $|x+1|=2$
	1.   $|x+1|+|2x-4|=3$
	1.   $\sqrt{x+2}=x-4$
	1.   $\sqrt{x-2}+\sqrt{x+1}=\sqrt{3}$
	1.   $\ln(2x+1) + \ln(x-2) = \ln(x)$
	1.  $9^x+3^x-12=0$ (une équation du 2nd degré est cachée dans cet exercice)
	1.   $|x-1|\leqslant 5$
	1.   $|x+2|\geqslant 3$
	1.   $|x+1|<|2x+1|$
	1.   $\dfrac{x+1}{x+2}\leqslant\dfrac{2x-1}{x-2} $
	1.   $\sqrt{x}\leqslant 2x-1$
	1.   $x^2-mx+1\geqslant 0 \quad (m\inℝ)$
	1.   $|x^2-1|\leqslant 3$

	
!!! {{ exercice()}}

     1. Voici un extrait du rapport d'une épreuve de concours : *Un nombre très
        important de candidats pense que si $a \geqslant 4$ et $b \geqslant 4$
		alors $\frac{a}{b}\geqslant \frac{4}{4}=1$* Qu'en pensez-vous?

	2. Sachant que  $x \geqslant 1$,  démontrez que $y=\frac{3x+1}{x+3}$  est aussi
		supérieur à 1. Déterminez le signe de $y-x$.
		
		*La  gestion  parfois défaillante  des  règles  de calculs  algébriques
          entraîne que seul 40% des candidats fournit une réponse correcte*
		  
	3.  Résoudre l'équation $L=\dfrac{3L+1}{L+3}$.
        
		*Il  est  surprenant  de  constater que  la  résolution  de  l'équation
          $L=\frac{3L+1}{L+3}$ pose problème à un nombre très important de candidats.*


!!! {{ exercice()}}

	Qui est plus grand: $\sqrt{6}+\sqrt{10}$ ou $\sqrt{5}+\sqrt{12}$?
	
!!! {{ exercice()}}

    Simplifiez $\sqrt{x^2} -x$
	
	

!!! {{ exercice()}}

    Mettre au même dénominateur, développer et réduire :
	
	1.  $ \displaystyle\left(\frac75-\frac12 \right)-\left( \frac75-1+\frac16\right)$
	1.  $ \displaystyle\left(\frac1{12}-\frac13\right)\left(1-\frac23\right)$
	1.  $ \displaystyle\frac1{n^2}-\frac{n-1}{n}$
	1.  $ \displaystyle\frac{\frac13-\frac19}{\frac{5}{12}+\frac{3}{8}}$
	1.  $ \displaystyle x-\frac{x}{x^2-1}+\frac{2}{x+1}$
	1.  $ \displaystyle \left({\rm e}^x - {\rm e}^{-x} \right)^3$
	

!!! {{ exercice()}}

    Comparer :

	1.   $\frac{11}{12}$ et $\frac{12}{13}$
	1.  $2\sqrt{6}$ et $3\sqrt{3}$
	1.  $\dfrac{n}{n+1}$ et $\dfrac{n-1}{n}$
	1.  $2\ln 3$ et $3\ln 2$	



!!! {{ exercice()}}

     On  définit,  pour tout  entier  $n\inℕ^\ast$  $n! =  n\times  (n-1)\times
     (n-2)\times \dots\times 3\times 2\times 1$ 

	 Soit $n\inℕ\setminus\{1,2 \}$. Calculer : 

	1.  $4!$
	1.  $5!$,
	1.   $\dfrac{100!}{98!}$,
	1.   $\dfrac{n!}{(n-2)!}$,
	1.   $\dfrac{1}{(n+1)!}-\dfrac1{n!}$	


!!! {{ exercice()}}

    Démontrer qu'il existe $(a,b,c)\inℝ^3$ tel que, pour tout réel $x$ distinct
    de $-1$ et $2$, on a:
	
	$$
	\dfrac{2x^2+1}{x^2-x-2} = a+\frac{b}{x-2}+\frac{c}{x+1}
	$$

!!! {{ exercice()}}

     Factoriser le plus possible :

	1.  $(2x-1)(-3x+1)-(2x-1)(x^2+1)$
	1.  $(x+2)(5x+1)-x-2$
	1.  $(x+2)(5x+1)-x^2+4$
	1.  $x^4+4$
	1.  $x^3-2x^2-5x+6$ (on cherchera une racine évidente)	


!!! {{ exercice()}}

    1. Démontrer que, pour tous réels $a$ et $b$, on a $ab\leqslant
       \dfrac{a^2+b^2}{2}$ avec égalité si, et seulement si, $a = b$
	
	2.    En   déduire   que   pour    tous   réels   positifs   $x$   et   $y$,
	    $\displaystyle{\sqrt{xy}\leq\frac{1}{2}(x+y)}$,   avec  égalité   si  et
	    seulement si $x=y$. 
		
	3.  On considère  un réel $p$ strictement positif. Quel  est le rectangle de
	    demi-périmètre $p$ ayant la plus grande aire possible? 


!!! {{ exercice("Et pour quelques (in)équations de plus")}}

    Résoudre  et  représenter  graphiquement  les  solutions  des  (in)équations
    suivantes (à inconnue réelle) : 

	1.  $\dfrac{x+3}{x-5}<1$
	1.  $\dfrac{x+3}{x-5}<0$
	1.  $(x-2)(x+3)<0$
	1.  $x<\dfrac{1}{x}$
	1.  $-x^3+2x^2 \geqslant 0$
	1.  $2x^2 - 4x - 6 \geqslant 0$
	1.  $x^3 -6x^2 + 12x - 6 < 2  $
	1.  $x^3 + x^2 - 5x - 5 \leqslant 0$
	1.  $\sqrt{x^2-5x+4}\geq x-1$
	1.  $x+\sqrt{x+1}<5$
	1.  $|x-5|<2$
	1.  $|x+1|>3$
	1.  $\dfrac{1+x}{1-x}<|x-1|$
	1.  $\sqrt{x^2-5x+4}\geq x-1$
	1.  $x+\sqrt{x+1}<5$
	1.  $|x-5|<2$
	1.  $|x+1|>3$
	1.  $\dfrac{1+x}{1-x}<|x-1|$
	
	
!!! {{ exercice("Inégalité avec parties entières")}}

	Démontrer que pour tout $(x,y) \in ℝ^2$, on a :

	$$
		\lfloor x  \rfloor + \lfloor  y \rfloor \leq  \lfloor x +y  \rfloor \leq
		\lfloor x \rfloor + \lfloor y \rfloor + 1
	$$

!!! {{ exercice("Égalité avec parties entières")}}

	Soit $x \in ℝ$ et $n  \in ℕ$. Les affirmations suivantes sont-elles vraies
	ou fausses ?
	
	1.  $\lfloor x + n \rfloor = \lfloor x \rfloor + n$
	1.  $\lfloor nx \rfloor = n \lfloor x \rfloor$ 	

!!! {{ exercice("Borne sup")}}

    Soient $A$ et $B$ deux parties de $ℝ$ vérifiant $A \subset B$.

	Démontrer que sup $A \leq $ sup $B$.
	
	
!!! {{ exercice("Incertitude en Physique")}}

    Deux résistances électriques de valeurs $R_1$ et $R_2$ (en ohms), montées en
    parallèle,  sont équivalentes  à  une  résistance de  valeur  $R$ (en  ohms)
    vérifiant $1/R=1/R_1+1/R_2$. Encadrer $R$ dans le cas où: 

	- $R_1$ est une résistance de $46$ ohms avec une précision de $5\%$;
	- $R_2$ est une résistance de $30$ ohms avec une précision de $5\%$.


!!! {{ exercice()}}

    1. Montrer que pour tout $(x,y) \in ℝ^2$, on a $(x+y)^2 \geq 4xy$.
	1.  En déduire que pour tout $(a,b,c) \in R^{+3}$, on a $(a+b)(b+c)(c+a) \geq 8abc$ 
	1. Quels sont les cas d'égalité dans l'inégalité précédente ? 


!!! {{ exercice("Approximation de Héron d'Alexandrie")}}

	On     pose    $u_0=2$     et    pour     tout    entier     naturel    $n$,
	$\displaystyle{u_{n+1}=\frac{u_n}{2}+\frac{1}{u_n}}$. 
	
	
	1.  Montrer que pour tout entier naturel $n$, $u_n\geq\sqrt{2}$.
	
	
	1.  Montrer que pour tout entier naturel $n$, $\displaystyle{u_{n+1}-\sqrt{2}\leq\frac{u_n-\sqrt{2}}{2}}$.
	
	1.  En déduire que pour tout entier naturel $n$, $\displaystyle{|u_n-\sqrt{2}|\leq 2^{-n}}$.
	
	
	1.  Déterminer un entier naturel $p$ tel que $u_p$ est une valeur approchée de $\sqrt{2}$ à $10^{-3}$ près.
	
	1.  Écrire une fonction en  Python permettant d'obtenir une valeur approchée
	    de $\sqrt{2}$  à $10^{-k}$ près,  où $k$ est  un entier naturel  non nul
	    donné en paramètre.



