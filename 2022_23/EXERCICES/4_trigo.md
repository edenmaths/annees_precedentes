{{ chapitre(4, "Trigonométrie - Exercices", 5)}}

!!! {{ exercice("Échauffement")}}
	
	1. Résoudre dans $ℝ$ l'équation $\sin x = -1/2$.
	1. Résoudre dans $ℝ$ l'équation $\cos x = \sqrt{3}/2$
	1. Soit  $x$ un  nombre réel  de l'intervalle  $[0;\pi]$ tel  que $\cos  x =
	   3/5$. Déterminer la valeur exacte de $\sin x$
	1.  Même question, mais $x$ est maintenant un nombre réel de l'intervalle $[-\pi ;0]$.
    1. Simplifier :
        1. $A=\cos(-\pi)+\cos(-\dfrac{3\pi}{4})+\cos(-\dfrac{\pi}{2})+\cos(-\dfrac{\pi}{4})$
		1. $B=\cos(0)+\cos(\dfrac{\pi}{4})+\cos(\dfrac{\pi}{2})+\cos(\dfrac{3\pi}{4})+\cos(\pi)$
		1. $C=\sin(\dfrac{\pi}{6})+\sin(\dfrac{\pi}{3})+\sin(\dfrac{\pi}{2})+\sin(\dfrac{\pi}{3})+\sin(\dfrac{5\pi}{6})+\sin(\pi)$ 
	1. Résoudre dans les ensembles proposés les inéquations suivantes :
		1. $\sin(x)\geq0$ pour $x\in[-2\pi;\pi]$
		1. $\cos(x)\leq0$ pour $x\in[\pi;2\pi]$
		1. $\sin(x)<0$ pour $x\in[-\pi;0]$
		1. $\sin(x)>0$ pour $x\in[0;3\pi]$
	1. Démontrer que pour tout réel $x$ on a $\big( \sin x + \cos x \big) ^2 = 1 + 2 \sin x \cos x$

!!! {{ exercice()}}

    Expliquez : 
	![Placeholder](./IMG/secc.jpg){ align=right width=550}

!!! {{ exercice("QCM")}}

	1.  $(1-\sin x)(1+\sin x)=$
        - [ ] $\cos x$
	    - [ ] $\sin x$
	    - [ ] $\tan x$
	    - [ ] $\cos^2 x$
	    - [ ] $\sin^2 x$
	
	2. $\dfrac{\tan x\cos x}{\sin x}=$
	    - [ ] $\frac{1}{\tan x}$
	    - [ ] $\frac{1}{\cos x}$
	    - [ ] $1$
	    - [ ] $\cos^2 x$
	    - [ ] $\tan x$

    3.  $\dfrac{1}{\cos x}-(\sin x)(\tan x)=$
		- [ ] $\cos x$
		- [ ] $\sin x$
		- [ ] $\tan x$
		- [ ] $\cos^2 x$
		- [ ] $\sin^2 x$

	4.  $\dfrac{\tan x-(\sin x)(\cos x)}{\tan x}=$
		- [ ] $1-\cos x$
		- [ ] $1-\sin x$
		- [ ] $\tan x+1$
		- [ ] $\cos^2 x$
		- [ ] $\sin^2 x$


	5. $\sec^2 x-1=$
	    - [ ] $\sin x\cos x$
		- [ ] $\sec^2 x$
		- [ ] $\tan^2 x$
		- [ ] $\cos^2 x$
		- [ ] $\sin^2 x$
	
	6. $\dfrac{1}{\sin x{\rm cotan} x}=$
		- [ ] $\cos x$
		- [ ] $\sin x$
		- [ ] $\tan x$
		- [ ] $\sec x$
		- [ ] ${\rm cosec} x$ 

	7. $\sin x + (\cos x)({\rm cotan} x)=$
		- [ ] ${\rm cosec} x$
		- [ ] $\sec x$
		- [ ] ${\rm cotan} x$
		- [ ] $\tan x$
		- [ ] $\sin x$ 


!!! {{ exercice("Périodicité")}}

    On considère la fonction $f$ définie sur $ℝ $ par $f (x) = \sin x\cos x$.
	Prouver que $f$ est impaire et périodique, de période $\pi $.



!!! {{ exercice("Coordonnée manquante")}}


	1. Sachant que $\displaystyle\sin {\pi \over 12} = {\sqrt 6 - \sqrt 2\over 4}$,
	   démontrer que
	   $
	    \cos {\pi \over 12} = {\sqrt 6 + \sqrt 2\over 4}.
	   $

	1. En déduire que
	   $
	   \tan {\pi \over 12} = 2 - \sqrt 3.
	   $

	1. En déduire les sinus et cosinus de
	   $
	   a = {11\pi \over 12}
		 \qquad {\rm et} \qquad
	   b = {23\pi \over 12}
	   $


!!! {{ exercice("Duplication")}}


	On donne $
	   \cos x = {\sqrt {2 + \sqrt 2}\over 2}
		  \qquad {\rm avec} \qquad
	   x \in I = \left [ 0; {\pi \over 4}\right] .
	$


	1.  Calculer $\sin x$.

	1.  En déduire $\sin 2x$.

	1.  Déterminer un encadrement de $2x$ lorsque $x$ est dans
		  l'intervalle $I$.

	1.  Déduire des questions précédentes que $\displaystyle x = {\pi \over 8}$.
	





!!! {{ exercice("Équations trigonométriques simples")}}

    Résoudre sur $ℝ$ les équations suivantes :

	1. $\sin x = -\dfrac{\sqrt{3}}{2}$
	1. $\cos \left( x+\dfrac{\pi}{6} \right) = \dfrac{\sqrt{2}}{2}$
	1. $\tan(2x)=1$
	1. $\sin\left(3x+\dfrac{\pi}{3}\right)=\cos(2x)$
	1. $\cos(x)+\cos(3x)=0$
	1. $\sin(x)+\sqrt{3}\cos(x)=\sqrt{2}$


!!! {{ exercice("Équations trigonométriques moins simples")}}

    Résoudre sur $ℝ$ les équations suivantes :
	
	1. $2\sin^2(x)+3\cos(x)-4=0$
	1. $\sin(x)+\sin(2x)+\sin(3x)=0$
	1. $\cos^4(x)-\sin^2(x)-1=0$



!!! {{ exercice("Quelques inéquations trigonométriques")}}
	
	Résoudre sur $ℝ$ les inéquations suivantes :
    
	1. $\tan(x)>\sqrt{3}$
	1. $\sin\left(2x+\dfrac{\pi}{4}\right) > \dfrac{\sqrt{2}}{2}$
	1. $\cos \left( x + \dfrac{\pi}{3} \right) + \sin \left( x+\dfrac{\pi}{3} \right) >0$


!!! {{ exercice("Un petit pot pourri")}}

    === "Énoncé"

		1. $2\cos\left(2x+\frac{\pi}{3}\right)=\sqrt{3}$.
		1. $\sin(x)\leq-\frac{1}{2}$.
		1. $\sin^2(x)+3\cos(x)-1=0$.
		1. $\cos(2x)-\sqrt{3}\sin(2x)=1$.
		1. $\sin^2\left(2x+\frac{\pi}{6}\right)=\cos^2\left(x+\frac{\pi}{3}\right)$. 
         

	=== "Indications"

         1. $\left\{x\inℝ\;\Big/\;x=-\dfrac{\pi}{12}\;[\pi]\quad\hbox{ou}\quad
            x=-\dfrac{\pi}{4}\;[\pi]\right\}$ 
		 2. $\displaystyle\bigcup_{k\inℤ}\left[\dfrac{7\pi}{6}+2k\pi,\dfrac{11\pi}{6}+2k\pi\right]$
		 3. $\left\{\dfrac{\pi}{2}+k\pi\Big/\;k\inℤ\right\}$
		 4. $\left\{-\dfrac{\pi}{3}+k\pi\Big/\;k\inℤ\right\}\cup\left\{k\pi\Big/\;k\inℤ\right\}$ 
		 5. $\left\{\dfrac{k\pi}{3}\Big/\;k\inℤ\right\}$







!!! {{ exercice("Formules avec la tangente")}}

	Soient $a$ et $b$ deux réels tels que $\tan(a)$, $\tan(b)$ et $\tan(a+b)$ soient définis.

	1. Donner une expression de $\tan(a+b)$ en fonction de $\tan(a)$ et $\tan(b)$.
	1. On se propose de calculer deux valeurs particulières de la fonction $\tan$.

	   1. Montrer que $\tan\left(\dfrac{\pi}{8}\right)$ est solution de l'équation $x^2+2x-1=0$.
	   1. En déduire que $x=\sqrt{2}-1$.
	   1.    Déterminer   avec    une   méthode    similaire   la    valeur   de
	      $\tan\left(\dfrac{\pi}{12}\right)$.
		  
!!! {{ exercice("Équation trigonométrique du second degré")}}

	Résoudre dans $ℝ$ l'équation $\displaystyle\sin^4x-{5\over2}\sin^2x+1=0$.



!!! {{ exercice("Suite trigonométrique")}}

	Pour $n \in ℕ$, on pose $x_n=\cos \left( \dfrac{\pi}{2^n} \right)$.
	
	1. Donner les valeurs de $x_0$, $x_1$ et $x_2$.
	1. Démontrer que $\forall n \in ℕ, \: x^2_{n+1}=\dfrac{x_n+1}{2}$.
	1. En déduire les valeurs de $\cos \left( \dfrac{\pi}{8} \right)$, $\cos \left( \dfrac{\pi}{16} \right)$ et $\cos \left( \dfrac{\pi}{32} \right)$.
	1. Démontrer que la suite $(x_n)$ est croissante	

    

!!! {{ exercice("Amplitude et phase en électricité")}}

	
	On peut démontrer en Physique que la tension aux bornes du condensateur dans
	un circuit LC  série (composé d'une bobine et d'un  condensateur branchés en
	série) vérifie l'équation : $q''(t)+\dfrac{1}{LC}q(t)=0$. 

	Une telle  équation, qui  porte sur  une fonction  inconnue et  ses dérivées
	s'appelle  une équation  différentielle  et apparaît  naturellement dans  de
	nombreux domaines de la Physique.  

	On suppose de plus que $q(0)=q_0>0$ et que $i(0)=q'(0)=i_0$. 

	On admet que les solutions de cette équation différentielle sont toujours de la forme :

	$q(t)=A \cos (\omega_0 t)+ B \sin (\omega_0 t)$.


	1. Démontrer que l'on a nécessairement $\omega_0=\dfrac{1}{\sqrt{LC}}$.

	1. Déterminer $A$ et $B$ en fonction de $q_0$ et $i_0$.
	1. Mettre $q(t)$ sous la forme $C\cos(\omega_0t+\varphi)$.    


!!! {{ exercice("Balistique")}}
	L'équation de la trajectoire d'un projectile lancé avec une vitesse initiale $v_0$
	suivant une direction faisant un angle $\alpha$ avec l'horizontale est donnée par

	$$
	y=-{g\over 2v_0^2}(1+\tan^2\alpha)x^2+(\tan\alpha)x,\quad x\in[0,X(\alpha)],
	$$

	où $g$ est l'accélération de la pesanteur et $X(\alpha)$ l'abscisse du point du
	sol où retombe le projectile. On suppose que $v_0$ est donnée  et que le lanceur peut
	choisir $\alpha$ dans $ ]0,\pi/2[$.

	1. Comment lancer le plus loin possible ?
	1. Démontrer que tous les points du sol accessibles, en dehors du point optimal,
	   peuvent être atteints de deux manières exactement: par un tir tendu et par un tir en cloche. 
	   

    

