## Nouveaux exos de probas

### Concours Agro-Véto xxx




Dans cet exercice, $n$ est un entier naturel non nul.

Une  urne contient  des  boules rouges  et des  boules  blanches, avec  une
proportion de boules rouges égale à  $p \in] 0,1[$. On effectue $n$ tirages
successifs d'une boule, avec remise. 

1. On note  $X$ la  variable aléatoire  égale au  nombre de  boules rouges
   obtenues à l'issue des $n$ tirages. 
   Déterminer la loi de $X$, son espérance et sa variance. 

2. Soit $r$ et $b$ deux entiers naturels tels que $r+b=n$.

	1. Quelle est la probabilité d'obtenir  $r$ boules rouges et $b$ boules
	   blanches  à  l'issue  des  $n$   tirages?  On  notera  $F(p)$  cette
	   probabilité. 
	   
	2. Déterminer  pour quelle(s)  valeur(s) de  $p$ cette  probabilité est
	   maximale. On pourra pour cela étudier la fonction $H=\ln \circ F$.

3. Un compteur  est chargé  de comptabiliser  le nombre  de boules  rouges
   obtenues  à  l'issue des  $n$  tirages  successifs. Hélas,  ce  compteur
   fonctionne mal : il affiche le bon total $X$ avec probabilité $1-\alpha$
   et il affiche  $X+1$ avec probabilité $\alpha$ (avec  $\alpha \in] 0,1[$
   ), indépendamment de la valeur de $X$. On note $Y$ la variable aléatoire
   égale au résultat affiché par le compteur. 

	1. Écrire  un programme informatique qui  simule l'expérience ci-dessus
	   et renvoie la valeur observée au compteur. 

	2.  Utiliser 10000  fois ce  programme pour  obtenir une  estimation de
	   $\mathbb E(Y)-n×p$.
	   Tester pour plusieurs valeurs de $\alpha, n$ et $p$.  Que peut-on conjecturer? 

	3. Déterminer la loi de probabilité de $Y$.

	4. Montrer que $\mathbb E(Y)=\mathbb E(X)+\alpha$.

	5. Calculer la variance de $Y$. Compétences attendues :



### Corrigé



1. $X$ suit la loi binomiale de paramètres $n$ et $p$ car le tirage est sans remise donc on effectue $n$ tirages indépendants suivant le même schéma de Bernoulli de paramètre $p$. Ainsi $\mathbb E(X) = n×p$ et $\mathbb V(X)=n×p×(1-p)$.

2.  
     1. $F(p)=\binom{n}{r}p^r(1-p)^b$

     2. $\forall p \in ]0,1[, H(p)=\ln\left(\binom{n}{r}\right)+r\ln(p)+b \ln(1-p)$  et
        $H'(p)=\frac{r}{p}-\frac{b}{1-p}=\frac{r-n p}{p(1-p)}$. 
		
		$H$ est  strictement croissante sur  $]0,r/n]$  et strictement
		décroissante  sur  $[r/n,1[$  donc  $F(p)$ est  maximale  pour
		$p=\frac{r}{n}$, qui est la proportion de boules rouges tirées. 

3. 
     1. On importe au choix `random` ou `numpy.random`:
	
	    ```python
	    import numpy.random as rd
	
	    def compteur(n: int, p: float, alpha: float) -> int:
	       X = 0
		   for k in range(n):
		       if rd.random() < p:
			       X = X + 1
		   return X + 1 if rd.random() < alpha else X
	    ```
     
	 2. On estime l'espérance à l'aide d'un calcul de moyenne.

        ```python
        def esperance(n: int, p: float, alpha: float) -> float:
           S = 0
	       N = 10000
	       for k in range(N):
	           S = S + compteur(n, p, alpha)
	       return S/N - n*p
        ```
        Les valeurs obtenues sont très proches de $\alpha$.

     3. $Y(\Omega)=\llbracket  0 ;  n+1 \rrbracket$. Notons  $A$ l'événement:
        «Le compteur fonctionne bien ». 
	  
	    On   applique   la  formule   des   probabilités   totales  avec   le
	    système complet d'événements $(A, \overline{A})$. 
	  
	    $\forall  k  \in Y(\Omega),  \mathbb P(Y=k)=P_{A}(Y=k)  \mathbb P(A)+P_{\overline{A}}(Y=k)
	    \mathbb P(\overline{A})=\mathbb P(X=k)(1-\alpha)+\mathbb P(X=k-1) \alpha$ 

	    - Si $k=0, \mathbb P(Y=0)=(1-p)^{n}(1-\alpha)$;
  
        - Si $k=n+1, \mathbb P(Y=n+1)=p^{n} \alpha$;

	    - Si $k \in \llbracket 1 ; n \rrbracket, \mathbb P(Y=k)=\left(\begin{array}{l}n
	    \\                                                k\end{array}\right)
	    p^{k}(1-p)^{n-k}(1-\alpha)+\left(\begin{array}{c}n                 \\
	    k-1\end{array}\right) p^{k-1}(1-p)^{n-k+1} \alpha$. 
	  
     4. On calcule l'espérance à partir de la définition :
    		$$
    		\begin{aligned}
    		\mathbb E(Y)  & =\displaystyle\sum_{k=0}^{n+1}  k  \mathbb P(Y=k)=(1-\alpha) \displaystyle\sum_{k=0}^{n+1}  k
    		\mathbb P(X=k)+\alpha \sum_{k=0}^{n+1} k \mathbb P(X=k-1) \\ 
            &  =(1-\alpha) \displaystyle\sum_{k=0}^{n}  k \mathbb P(X=k)+\alpha \displaystyle \sum_{k=0}^{n}(k+1)
            \mathbb P(X=k)  \\ 
			&  =(1-\alpha) \displaystyle\sum_{k=0}^{n}  k \mathbb P(X=k)+\alpha \displaystyle \sum_{k=0}^{n}k\mathbb P(X=k)+\alpha \displaystyle \sum_{k=0}^{n}\mathbb P(X=k)  \\ 
			&  = \displaystyle\sum_{k=0}^{n}  k \mathbb P(X=k)+\alpha×1  \\ 
			& =\mathbb E(X)+\alpha 
            \end{aligned}
            $$

     5. On démontre de même que $\mathbb E\left(Y^{2}\right)=(1-\alpha)
        \mathbb E\left(X^{2}\right)+\alpha
        \mathbb E\left((X+1)^{2}\right)=\mathbb E\left(X^{2}\right)+2 \alpha \mathbb E(X)+\alpha$ 
	  
	    De  plus,  $[\mathbb E(Y)]^{2}=[\mathbb E(X)]^{2}+2   \alpha  \mathbb E(X)+\alpha^{2}$,  d'où
	    $\mathbb V(Y)=\mathbb E\left(Y^{2}\right)-[\mathbb E(Y)]^{2}=\mathbb V(X)+\alpha(1-\alpha)$. 
