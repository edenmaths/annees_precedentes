## Fractions

## Prérequis

Règles de calcul sur les fractions.

Dès le début de la 1ère année.

## Calculs dans l'ensemble des rationnels

## Calcul 1.1 - Simplification de fractions.

Simplifier les fractions suivantes (la lettre $k$ désigne un entier naturel non nul).
a) $\frac{32}{40}$
c) $\frac{27^{-1} \times 4^{2}}{3^{-4} \times 2^{4}}$
b) $8^{3} \times \frac{1}{4^{2}}$
d) $\frac{(-2)^{2 k+1} \times 3^{2 k-1}}{4^{k} \times 3^{-k+1}}$

Calcul 1.2 - Sommes, produits, quotients, puissances.

Écrire les nombres suivants sous forme d'une fraction irréductible.
a) $\frac{2}{4}-\frac{1}{3}$
c) $\frac{36}{25} \times \frac{15}{12} \times 5$
b) $\frac{2}{3}-0,2$
d) $-\frac{2}{15} \div\left(-\frac{6}{5}\right)$

## Calcul 1.3

Écrire les nombres suivants sous forme d'une fraction irréductible.
a) $(2 \times 3 \times 5 \times 7)\left(\frac{1}{2}+\frac{1}{3}+\frac{1}{5}+\frac{1}{7}\right)$
b) $\left(\frac{136}{15}-\frac{28}{5}+\frac{62}{10}\right) \times \frac{21}{24}$
c) $\frac{5^{10} \times 7^{3}-25^{5} \times 49^{2}}{(125 \times 7)^{3}+5^{9} \times 14^{3}}$
d) $\frac{1978 \times 1979+1980 \times 21+1958}{1980 \times 1979-1978 \times 1979}$

Calcul 1.4 - Des nombres décimaux et des fractions.

Dans chaque cas, donner le résultat sous forme d'une fraction irréductible.
a) 0,2
c) 1,35
e) $\frac{1}{3}-0,3$
b) $0,36 \ldots \ldots \ldots \ldots$
d) $1,5+\frac{2}{3}$
f) $\frac{13,5}{18,2-3,2}$

Calcul 1.5 - Un petit calcul.

Écrire $\frac{0,5-\frac{3}{17}+\frac{3}{37}}{\frac{5}{6}-\frac{5}{17}+\frac{5}{37}}+\frac{0,5-\frac{1}{3}+\frac{1}{4}-0,2}{\frac{7}{5}-\frac{7}{4}+\frac{7}{3}-3,5}$ sous forme d'une fraction irréductible. Calcul 1.6 - Le calcul littéral à la rescousse.

En utilisant les identités remarquables et le calcul littéral, calculer les nombres suivants.
a) $\frac{2022}{(-2022)^{2}+(-2021)(2023)}$
c) $\frac{1235 \times 2469-1234}{1234 \times 2469+1235}$
b) $\frac{2021^{2}}{2020^{2}+2022^{2}-2}$
d) $\frac{4002}{1000 \times 1002-999 \times 1001}$

![](https://cdn.mathpix.com/cropped/2023_05_28_da8753aae6307299fee8g-010.jpg?height=120&width=300&top_left_y=423&top_left_x=1575)

Calcul 1.7 - Les fractions et le calcul littéral.

Mettre sous la forme d'une seule fraction, qu'on écrira sous la forme la plus simple possible.

a) $\frac{1}{(n+1)^{2}}+\frac{1}{n+1}-\frac{1}{n}$ pour $n \in \mathbb{N}^{*}$

b) $\frac{a^{3}-b^{3}}{(a-b)^{2}}-\frac{(a+b)^{2}}{a-b}$ pour $(a, b) \in \mathbb{Z}^{3}$, distincts deux à deux.

c) $\frac{\frac{6(n+1)}{n(n-1)(2 n-2)}}{\frac{2 n+2}{n^{2}(n-1)^{2}}}$ pour $n \in \mathbb{N}^{*} \backslash\{1,2\}$

Calcul 1.8 - Le quotient de deux sommes de Gauss.

Simplifier $\frac{\sum_{k=0}^{n^{2}} k}{\sum_{k=0}^{n} k}$ pour tout $n \in \mathbb{N}^{*}$, en utilisant la formule $1+2+\cdots+p=\frac{p(p+1)}{2} \ldots \ldots$

Calcul 1.9 - Décomposition en somme d'une partie entière et d'une partie décimale.

Soit $k \in \mathbb{R} \backslash\{1\}$ et $x \in \mathbb{R} \backslash\{2\}$. Écrire les fractions suivantes sous la forme $a+\frac{b}{c}$ avec $b<c$.
a) $\frac{29}{6}$
b) $\frac{k}{k-1}$
c) $\frac{3 x-1}{x-2}$

Calcul 1.10 - Un produit de fractions.

Soit $t \in \mathbb{R} \backslash\{-1\}$. On donne $A=\frac{1}{1+t^{2}}-\frac{1}{(1+t)^{2}}$ et $B=\left(1+t^{2}\right)(1+t)^{2}$.

Simplifier $A B$ autant que possible.

## Comparaison

Calcul 1.11 - Règles de comparaison.

Comparer les fractions suivantes avec le signe $«>», \ll<»$ ou $«=»$.
a) $\frac{3}{5} \ldots \frac{5}{9}$
b) $\frac{12}{11} \ldots \frac{10}{12}$
c) $\frac{125}{25} \ldots \frac{105}{21}$

Calcul 1.12 - Produit en croix.

![](https://cdn.mathpix.com/cropped/2023_05_28_da8753aae6307299fee8g-010.jpg?height=94&width=1684&top_left_y=2420&top_left_x=183)

(1) (1) (1)

Calcul 1.13 - Produit en croix.

On pose $A=\frac{100001}{1000001}$ et $B=\frac{1000001}{10000001}:$ a-t-on $A>B, A=B$ ou $A<B$ ?

(1) (1) (1) 1 Réponses mélangées

$$
\begin{aligned}
& 1000 \quad 2 t \quad \frac{13}{6} \quad 3 \quad \text { Non } \quad 2^{5} \quad \frac{-1}{n(n+1)^{2}} \quad \frac{3}{2} n \quad-\frac{a b}{a-b} \\
& \frac{12}{11}>\frac{10}{12} \quad \frac{n^{3}+n}{n+1} \quad \frac{1}{30} \quad 1+\frac{1}{k-1} \quad A>B \quad \frac{27}{20} \quad \frac{1}{9} \quad 2 \quad \frac{1}{5} \\
& \frac{9}{25} \quad \frac{203}{24} \quad \frac{-10}{3} \quad 1 \quad 247 \quad 4+\frac{5}{6} \quad \frac{1}{6} \quad \frac{3}{5}>\frac{5}{9} \quad \frac{9}{10} \quad \frac{7}{15} \\
& \frac{4}{5} \quad-2 \times 3^{3 k-2} \quad 2022 \quad \frac{16}{35} \quad 3+\frac{5}{x-2} \quad 9 \quad \frac{125}{25}=\frac{105}{21} \quad \frac{1}{2}
\end{aligned}
$$

- Réponses et corrigés page 83

