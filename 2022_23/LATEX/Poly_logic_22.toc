\babel@toc {french}{}\relax 
\contentsline {chapter}{Table des mati\`eres}{3}{chapterstar.2}%
\contentsline {chapter}{\numberline {2}Bases pour raisonner}{5}{chapter.3}%
\contentsline {section}{\numberline {2.1}Mise en \IeC {\'e}vidence des probl\IeC {\`e}mes}{6}{section.4}%
\contentsline {subsection}{\numberline {2.1.1}Rapports de jury}{6}{subsection.5}%
\contentsline {subsection}{\numberline {2.1.2}Les BCPST1 au Pays des Merveilles}{7}{subsection.6}%
\contentsline {section}{\numberline {2.2}Un brin de logique}{9}{section.7}%
\contentsline {subsection}{\numberline {2.2.1}Test pr\IeC {\'e}liminaire}{9}{subsection.8}%
\contentsline {subsection}{\numberline {2.2.2}Contexte}{9}{subsection.11}%
\contentsline {subsection}{\numberline {2.2.3}Implication}{10}{subsection.12}%
\contentsline {subsection}{\numberline {2.2.4}Contrapos\IeC {\'e}e}{11}{subsection.14}%
\contentsline {subsection}{\numberline {2.2.5}Contraire (n\IeC {\'e}gation) d'une implication}{11}{subsection.15}%
\contentsline {subsection}{\numberline {2.2.6}Quantificateurs}{12}{subsection.18}%
\contentsline {subsubsection}{\numberline {2.2.6.1}Un pour tous : $\forall $}{12}{subsubsection.19}%
\contentsline {subsubsection}{\numberline {2.2.6.2}Un pour l'existence : $\exists $}{12}{subsubsection.20}%
\contentsline {subsubsection}{\numberline {2.2.6.3}Quantificateurs et n\IeC {\'e}gation}{12}{subsubsection.21}%
\contentsline {subsection}{\numberline {2.2.7}R\IeC {\'e}ciproque / \IeC {\'E}quivalence}{12}{subsection.23}%
\contentsline {section}{\numberline {2.3}R\IeC {\'e}currence}{13}{section.26}%
\contentsline {subsection}{\numberline {2.3.1}R\IeC {\'e}cursion, r\IeC {\'e}currence, induction}{13}{subsection.27}%
\contentsline {subsection}{\numberline {2.3.2}Un jeu}{14}{subsection.29}%
\contentsline {subsection}{\numberline {2.3.3}Les th\IeC {\'e}or\IeC {\`e}mes}{15}{subsection.34}%
\contentsline {section}{\numberline {2.4}Les grands types de raisonnements: le r\IeC {\'e}sum\IeC {\'e} }{16}{section.40}%
\contentsline {subsection}{\numberline {2.4.1}Avec les quantificateurs}{16}{subsection.41}%
\contentsline {subsection}{\numberline {2.4.2}Pour d\IeC {\'e}montrer une implication}{16}{subsection.42}%
\contentsline {subsection}{\numberline {2.4.3}Un raisonnement par r\IeC {\'e}currence}{17}{subsection.46}%
\contentsline {subsection}{\numberline {2.4.4}\IeC {\'E}quivalence}{17}{subsection.47}%
\contentsline {section}{\numberline {2.5}Exer\IeC {\c c}ons-nous}{18}{section.49}%
\contentsfinish 
