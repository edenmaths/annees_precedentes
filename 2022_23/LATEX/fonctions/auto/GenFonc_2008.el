(TeX-add-style-hook
 "GenFonc_2008"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (TeX-add-symbols
    "f")
   (LaTeX-add-labels
    "lagrange"
    "histoire"
    "ex1"
    "pop"
    "chch"
    "riri")
   (LaTeX-add-environments
    '("demonstration" LaTeX-env-args ["argument"] 0)
    '("nliste" LaTeX-env-args ["argument"] 1)
    '("Sliste" LaTeX-env-args ["argument"] 0)
    '("DeuxCols" LaTeX-env-args ["argument"] 0)
    '("Zigoui" LaTeX-env-args ["argument"] 0)
    '("arrayl" LaTeX-env-args ["argument"] 0)
    '("rubric" LaTeX-env-args ["argument"] 1)
    '("Question" LaTeX-env-args ["argument"] 1)
    '("systeme" LaTeX-env-args ["argument"] 0)
    '("exercise" LaTeX-env-args ["argument"] 0)
    '("solution" LaTeX-env-args ["argument"] 0)
    '("exercice" LaTeX-env-args ["argument"] 0)
    '("algo" LaTeX-env-args ["argument"] 0)))
 :latex)

