(TeX-add-style-hook
 "fonctions21"
 (lambda ()
   (LaTeX-add-labels
    "lagrange"
    "histoire"
    "met3:inegalite"
    "met3:valabs"
    "met3:etude"
    "met3:inegder"
    "met3:bijprat"
    "ex1"
    "ex1bis"
    "ex2"
    "ex3"
    "ex4"
    "pop"
    "chch"
    "riri"
    "ch")
   (LaTeX-add-environments
    '("Test" LaTeX-env-args ["argument"] 1)
    '("nliste" LaTeX-env-args ["argument"] 1)
    '("Sliste" LaTeX-env-args ["argument"] 0)
    '("DeuxCols" LaTeX-env-args ["argument"] 0)
    '("Zigoui" LaTeX-env-args ["argument"] 0)
    '("arrayl" LaTeX-env-args ["argument"] 0)
    '("rubric" LaTeX-env-args ["argument"] 1)
    '("Question" LaTeX-env-args ["argument"] 1)
    '("systeme" LaTeX-env-args ["argument"] 0)
    '("demonstration" LaTeX-env-args ["argument"] 0))
   (LaTeX-add-index-entries
    "logarithme!népérien"
    "exponentielle!fonction"
    "puissance!fonction"
    "logarithme!décimal"
    "théorème!de croissance comparée"
    "inegalite@inégalité"
    "valeur absolue"
    "fonction!etude@étude"
    "bijection"
    "fonction!bijective"
    "equation@équation!trigonométrique"))
 :latex)

