(TeX-add-style-hook
 "der_app_21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "th::leibniz"
    "extreloc"
    "met3:inegder"
    "met3:bijprat"
    "met3:bijth"
    "met13:derivabilite"
    "met13:prolongement"
    "exo3:derivation"
    "exo3:bijective")
   (LaTeX-add-index-entries
    "inegalite@inégalité"
    "bijection"
    "fonction!bijective"
    "derivabilite@dérivabilité"
    "fonction!dérivable"
    "Rolle!théorème de"
    "théorème!de Rolle"
    "théorème!de prolongement $\\mathscr{C}^{1}$"))
 :latex)

