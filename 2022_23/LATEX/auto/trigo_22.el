(TeX-add-style-hook
 "trigo_22"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-environments
    '("demonstration" LaTeX-env-args ["argument"] 0)))
 :latex)

