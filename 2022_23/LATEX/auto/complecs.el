(TeX-add-style-hook
 "complecs"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "coeffrac"
    "croco"))
 :latex)

