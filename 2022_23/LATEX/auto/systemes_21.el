(TeX-add-style-hook
 "systemes_21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-run-style-hooks
    "systeme1"
    "systeme2"
    "systeme3"
    "systeme4"
    "systeme5"
    "systeme6"
    "systeme11"
    "systeme12"
    "systeme13"
    "systeme14"
    "systeme15"
    "systeme16")
   (LaTeX-add-labels
    "exo8:systeme"))
 :latex)

