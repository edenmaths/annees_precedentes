(TeX-add-style-hook
 "#Sets_22"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-add-symbols
    "B"
    "Cc"
    "Uni")
   (LaTeX-add-labels
    "dualite"
    "ce1")
   (LaTeX-add-xcolor-definecolors
    "circle edge"
    "circle area"))
 :latex)

