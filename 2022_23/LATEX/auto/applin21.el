(TeX-add-style-hook
 "applin21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "thm::vectim"
    "def::iso"
    "thm::morlib"
    "exo::im"))
 :latex)

