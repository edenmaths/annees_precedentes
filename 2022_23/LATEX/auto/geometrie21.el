(TeX-add-style-hook
 "geometrie21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "vecteurs-et-milieu2"
    "ps"
    "psnorme"
    "psproj"
    "pscos")
   (LaTeX-add-environments
    '("Test" LaTeX-env-args ["argument"] 1)
    '("demonstration" LaTeX-env-args ["argument"] 0))
   (LaTeX-add-index-entries
    "produit scalaire |( "
    "orthogonalité"
    "vecteur normal"))
 :latex)

