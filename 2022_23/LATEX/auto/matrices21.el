(TeX-add-style-hook
 "matrices21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "thm::trace_prod"
    "thm::distri"))
 :latex)

