(TeX-add-style-hook
 "lim_cont_21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "met12:continuite")
   (LaTeX-add-index-entries
    "fonction!limite"
    "limite(s)!définition de la"
    "limite(s)!unicité de la"
    "limite(s)!a gauche ou a droite@à gauche ou à droite"
    "limite(s)!opérations sur les"
    "théorème!des gendarmes"
    "théorème!d'encadrement"
    "fonction!continue"
    "continuité"
    "continuité!a gauche ou a droite@à gauche ou à droite"
    "continuité!prolongement par")
   (LaTeX-add-xcolor-definecolors
    "vert"))
 :latex)

