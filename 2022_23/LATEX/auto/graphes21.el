(TeX-add-style-hook
 "graphes21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "th::som_deg"))
 :latex)

