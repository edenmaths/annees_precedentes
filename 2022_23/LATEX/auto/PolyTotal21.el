(TeX-add-style-hook
 "PolyTotal21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("trmbookBCPST" "svgnames" "russe" "utf8x" "euler")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("ccfonts" "boldsans") ("mdframed" "framemethod=tikz") ("fontenc" "LGR" "T1") ("asymptote" "inline")))
   (TeX-run-style-hooks
    "latex2e"
    "patch-tkz-graph"
    "./fonctions/fonctions21"
    "Logic22"
    "sets_22"
    "Reels22"
    "trigo_22"
    "complecs"
    "denomb21"
    "suites1_21"
    "DerPrimInt_21"
    "integrales21"
    "equa_diffs_21"
    "matrices21"
    "systemes_21"
    "geometrie21"
    "stats_21"
    "poly_21"
    "EV_21"
    "suites_conv_21"
    "lim_cont_21"
    "der_app_21"
    "DL_21"
    "Int_2_21"
    "applin21"
    "probasIntro21"
    "VAR21"
    "fdxvars21"
    "trmbookBCPST"
    "trmbookBCPST10"
    "etex"
    "preambuleTrm"
    "yfonts"
    "alterqcm"
    "longtable"
    "stackengine"
    "lcd"
    "soul"
    "caption"
    "multido"
    "enumitem"
    "picins"
    "numprint"
    "tikz"
    "tkz-graph"
    "tikz-qtree"
    "tikz-cd"
    "tkz-tab"
    "tkz-linknodes"
    "cclicenses"
    "cclicence"
    "textcomp"
    ""
    "mathtools"
    "xlop"
    "algo"
    "bold-extra"
    "ccfonts"
    "eucal"
    "eulervm"
    "rotating"
    "mdframed"
    "environ"
    "hhline"
    "colortbl"
    "minitoc"
    "tocbibind"
    "fontenc"
    "textalpha"
    "asymptote"
    "frcursivefourier")
   (TeX-add-symbols
    '("tsxap" 1)
    '("ecsxap" 1)
    '("MATA" 1)
    '("textgreek" 1)
    '("indi" 1)
    '("abs" 1)
    '("arc" 1)
    '("GrosTrou" 1)
    '("Trou" 1)
    '("frbal" 3)
    "myunit"
    "triangleup"
    "nor"
    "nand"
    "cacheb"
    "cacheg"
    "soustitre"
    "Python"
    "Card"
    "func"
    "cacher"
    "dx"
    "emph"
    "btc")
   (LaTeX-add-environments
    '("Sol")
    '("Aide")
    "aide"
    "sol")
   (LaTeX-add-lengths
    "longarc")
   (LaTeX-add-array-newcolumntypes
    "A"))
 :latex)

