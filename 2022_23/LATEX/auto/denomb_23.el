(TeX-add-style-hook
 "denomb_23"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "cardpart"
    "cardinc"
    "crible"
    "cardprod"
    "fpascal"))
 :latex)

