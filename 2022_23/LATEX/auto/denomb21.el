(TeX-add-style-hook
 "denomb21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "cardpart"
    "cardinc"
    "crible"
    "cardprod"
    "fpascal"))
 :latex)

