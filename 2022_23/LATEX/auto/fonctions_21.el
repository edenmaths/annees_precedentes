(TeX-add-style-hook
 "fonctions_21"
 (lambda ()
   (LaTeX-add-labels
    "lagrange"
    "histoire"
    "met3:inegalite"
    "met3:valabs"
    "met3:etude"
    "met3:inegder"
    "met3:bijprat"
    "ex1"
    "pop"
    "chch"
    "riri"
    "ch")
   (LaTeX-add-index-entries
    "logarithme!népérien"
    "exponentielle!fonction"
    "puissance!fonction"
    "logarithme!décimal"
    "théorème!de croissance comparée"
    "inegalite@inégalité"
    "valeur absolue"
    "fonction!etude@étude"
    "bijection"
    "fonction!bijective"
    "equation@équation!trigonométrique"))
 :latex)

