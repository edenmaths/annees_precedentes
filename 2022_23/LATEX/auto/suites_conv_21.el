(TeX-add-style-hook
 "suites_conv_21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "ineg"
    "passage"
    "th::sim_def_eq"
    "exo3.2"
    "exo3.3"
    "exo3.4"
    "exo3.5"
    "exo3.9"
    "exo3.10"
    "exo3.12"
    "exo3.15"))
 :latex)

