(TeX-add-style-hook
 "suites1_21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "mamie"
    "thm::unicite_suite"
    "eq:1")
   (LaTeX-add-index-entries
    "algorithme"
    "suite"))
 :latex)

