(TeX-add-style-hook
 "EV_21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "sev1"
    "sev2"
    "th::ech"))
 :latex)

