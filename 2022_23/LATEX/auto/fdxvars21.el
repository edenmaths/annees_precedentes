(TeX-add-style-hook
 "fdxvars21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "experim"
    "pos1"
    "pos2"
    "pos4"
    "fig3"))
 :latex)

