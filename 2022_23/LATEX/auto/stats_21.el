(TeX-add-style-hook
 "stats_21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-run-style-hooks
    "stats2008")
   (TeX-add-symbols
    '("nombre" 1)
    "ie"
    "se")
   (LaTeX-add-labels
    "election"
    "disque"
    "harem"
    "garden"
    "vp"
    "banquise"
    "brevet"
    "tcc"
    "pcc"
    "mc"
    "haremcc"
    "onze"
    "comp"
    "tabqcq"))
 :latex)

