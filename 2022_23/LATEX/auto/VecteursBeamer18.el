(TeX-add-style-hook
 "VecteursBeamer18"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "french")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("preambuleTrm" "beamer" "utf8x") ("siunitx" "detect-all") ("daedale" "square")))
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "etex"
    "preambuleTrm"
    "iwona"
    "yfonts"
    "bookmark"
    "multido"
    "tikz"
    "tkz-graph"
    "circuitikz"
    "siunitx"
    "caption"
    "daedale"
    "algo"
    "casio"
    "ti83font"
    "scalefnt")
   (TeX-add-symbols
    '("MyBox" ["argument"] 1)
    "frt"
    "myunit"
    "triangleup"
    "TR")
   (LaTeX-add-environments
    "exercise")
   (LaTeX-add-counters
    "exb")
   (LaTeX-add-amsthm-newtheorems
    "exercice"))
 :latex)

