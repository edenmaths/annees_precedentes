(TeX-add-style-hook
 "PolyDerPrimInt21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("trmbookBCPST" "svgnames" "russe" "utf8x" "euler")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("ccfonts" "boldsans") ("mdframed" "framemethod=tikz") ("fontenc" "LGR" "T1")))
   (TeX-run-style-hooks
    "latex2e"
    "patch-tkz-graph"
    "DerPrimInt_21"
    "trmbookBCPST"
    "trmbookBCPST10"
    "etex"
    "preambuleTrm"
    "yfonts"
    "alterqcm"
    "longtable"
    "stackengine"
    "lcd"
    "soul"
    "caption"
    "multido"
    "enumitem"
    "picins"
    "numprint"
    "tikz"
    "tkz-graph"
    "tikz-qtree"
    "tikz-cd"
    "tkz-tab"
    "tkz-linknodes"
    "cclicenses"
    "cclicence"
    "textcomp"
    ""
    "mathtools"
    "xlop"
    "algo"
    "bold-extra"
    "ccfonts"
    "eucal"
    "eulervm"
    "rotating"
    "mdframed"
    "environ"
    "hhline"
    "colortbl"
    "minitoc"
    "tocbibind"
    "fontenc"
    "textalpha")
   (TeX-add-symbols
    '("abs" 1)
    '("arc" 1)
    '("GrosTrou" 1)
    '("Trou" 1)
    '("frbal" 3)
    "myunit"
    "triangleup"
    "nor"
    "nand"
    "cacheb"
    "cacheg"
    "soustitre"
    "Python"
    "Card"
    "func"
    "cacher"
    "dx"
    "emph"
    "btc")
   (LaTeX-add-environments
    "aide"
    "sol")
   (LaTeX-add-lengths
    "longarc"))
 :latex)

