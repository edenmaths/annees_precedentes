(TeX-add-style-hook
 "VAR21"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (LaTeX-add-labels
    "vapièces"
    "vadés"
    "th:fonc_rep"
    "vacarre"))
 :latex)

