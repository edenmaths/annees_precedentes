import sympy as sy

#define symbolic vars, function
x,y=sympy.symbols('x y')
fun=10*sy.sin(x**2/2 - y**2/4 + 3)*sy.cos(2*x+1-sy.exp(y))

#take the gradient symbolically
gradfun=[sympy.diff(fun,var) for var in (x,y)]

#turn into a bivariate lambda for numpy
numgradfun=sympy.lambdify([x,y],gradfun)

import numpy as np
import matplotlib.pyplot as plt

X,Y=np.meshgrid(np.linspace(-1.5,1.5,32),np.linspace(-3.5,1,32))
graddat=numgradfun(X,Y)

plt.figure()
plt.quiver(X,Y,graddat[0],graddat[1])
plt.savefig("/home/moi/PREPA/EDEN/bcpst1/docs/2022_23/LATEX/FIGURES/grad.pdf")

plt.show()
