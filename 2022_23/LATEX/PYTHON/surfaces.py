import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D # permet d'utiliser l'attribut projection="3d"

x = np.linspace(-1.5, 1.5, 100)
y = np.linspace(-3.5, 1, 100)
X, Y = np.meshgrid(x, y)

Z = 10*np.sin(X**2/2 - Y**2/4 + 3)*np.cos(2*X+1-np.exp(Y))

plt.figure()
ax = plt.axes(projection="3d")

ax.set_zlim3d(-10,10)

ax.contour3D(X, Y, Z, 16, cmap='viridis')#, zdir='z', offset=0)
ax.plot_surface(X, Y, Z, alpha=0.5, cmap='viridis')
ax.plot_wireframe(X, Y, Z, alpha=0.2, cmap='viridis')

plt.savefig("/home/moi/PREPA/EDEN/bcpst1/docs/2022_23/LATEX/FIGURES/grad_3d.pdf")
plt.show()
